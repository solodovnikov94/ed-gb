$(document).ready(function () {
	$(".mm__menu-group--title").on("click", function () {
		let title = $(this);
		let list = $(this).next();

		list.slideToggle(150);

		$(document).mouseup(function (e) {
			if (title.parent().has(e.target).length === 0) {
				list.slideUp(150);
			}
		});
	});

	$(".mm__close").on("click", function () {
		$(".mm").removeClass("active");
	});

	$(".mm__open").on("click", function () {
		$(".mm").toggleClass("active");
	});

	$(".cc__colors-item").on("click", function () {
		let key = $(this).data('key');
		let colorId = $(this).data('colorId');

		$(".cc--" + key).removeClass('active').fadeOut(1);
		$("#cc--" + key + "-" + colorId).fadeIn(150);

	});

	$("body").on('click', '[href*="#"]', function(e){
		$('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - 120 }, 500);
		e.preventDefault();
	});

	let seatFunctionsSlider = $('#seat-functions-slider').lightSlider({
		item: 3,
		adaptiveHeight: false,
		slideMargin: 30,
		pager: false,
		// prevHtml: '<img src="../images/icons/arrow_l_2.png">',
		// nextHtml: '<img src="../images/icons/arrow_r_2.png">',
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					item: 3,
					slideMargin: 15
				}
			},
			{
				breakpoint: 992,
				settings: {
					item: 2,
					slideMargin: 15
				}
			},
			{
				breakpoint: 768,
				settings: {
					item: 1,
					slideMargin: 15
				}
			}
		]
	});

	let sc = {
		item: 4,
		slideMargin: 15,
		pager: false,
		prevHtml: '<img src="../images/Zakaz_pop_up/arrow_l.png">',
		nextHtml: '<img src="../images/Zakaz_pop_up/arrow_r.png">',
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					item: 4,
					slideMargin: 15
				}
			},
			{
				breakpoint: 992,
				settings: {
					item: 3,
					slideMargin: 15
				}
			},
			{
				breakpoint: 768,
				settings: {
					item: 3,
					slideMargin: 15,
				}
			},
			{
				breakpoint: 700,
				settings: {
					item: 2,
					slideMargin: 15,
					adaptiveHeight: false,
				}
			},
			{
				breakpoint: 480,
				settings: {
					item: 1,
					slideMargin: 15,
					adaptiveHeight: true,
				}
			}
		]
	};

	let popupRelated = $('#popup-related').lightSlider(sc);

	$("#seat-functions--prev").on("click", function () {
		seatFunctionsSlider.goToPrevSlide();
	});

	$("#seat-functions--next").on("click", function () {
		seatFunctionsSlider.goToNextSlide();
	});

	$('#accessories-slider').lightSlider({
		item: 1,
		adaptiveHeight: true,
		slideMargin: 30,
		pager: false,
		prevHtml: '<img src="../images/icons/arrow_l_2.png">',
		nextHtml: '<img src="../images/icons/arrow_r_2.png">',
	});

	$('#recommended-items-slider').lightSlider({
		item: 1,
		adaptiveHeight: false,
		slideMargin: 30,
		pager: false,
		prevHtml: '<img src="../images/icons/arrow_l_2.png">',
		nextHtml: '<img src="../images/icons/arrow_r_2.png">',
	});

	$(".popup__close").on("click", function () {
		$(this).parent().parent().parent().fadeOut(300);
	});

	// order form
	$('#send-order-from').on('click', function (e) {
		e.preventDefault();
		var thisForm = $(this).closest('form');
		thisForm.addClass('loader');
		thisForm.find('div.order__error').html('');

		$.ajax({
			url: thisForm.attr('action'),
			type: thisForm.attr('method'),
			dataType: "html",
			data: thisForm.serialize(),
			success: function (response) {
				response = $.parseJSON(response);

				if(response.errors) {
					var key, label, errors = response.errors;

					for (key in errors) {
						thisForm.find('#error-'+key).html(errors[key]);
					}
				}

				if(response.success) {
					window.location.replace(response.url);
				}

				setTimeout(function () {
					thisForm.removeClass('loader');
				}, 500);
			},
			error: function (response) {
				thisForm.removeClass('loader');
			}
		});
	});

	$("input[name='delivery']").change(function(){
		if($(this).val() == 3) {
			$('#new-post-block').hide();
		} else {
			$('#new-post-block').show();
		}
	});

	if(window.location.pathname == '/order') {
		// load cities
		$.ajax({
			url : '/order/novaposhta',
			type : 'post',
			data : {
				'_token': $('meta[name="csrf-token"]').attr('content'),
			},
			success : function(data) {
				$('#cities').html(data.html);
			},
			error : function(request,error) {
				$('#cities').html('<option>-</option>');
			}
		});

		// get warehouses
		$('#cities').change(function(){
			$("#np_city_hidden").val($("#cities option:selected").text());
			$.ajax({
				url : '/order/novaposhta',
				type : 'POST',
				data : {
					'_token': $('meta[name="csrf-token"]').attr('content'),
					'warehouses' : $(this).val(),
				},
				success : function(data) {
					$('#warehouses').html(data.html);
				},
				error : function(request,error)
				{
					$('#warehouses').html('<option>-</option>');
				}
			});
		})
	}
	//end order form

	//basket
	$('.open-cart').on('click', function (e) {
		e.preventDefault();
		$('.popup-container').fadeIn(300);
		popupRelated.destroy();
		if (!popupRelated.lightSlider) {
			popupRelated = $('#popup-related').lightSlider(sc);
		}
	});

	$('body').on('click', '.crement', function (e) {
		e.preventDefault();
		var product = $(this).data('product-id');
		var crement = $(this).data('value');

		$.ajax({
			type: 'post',
			url: '/basket/crement',
			data: {
				'_token': $('meta[name="csrf-token"]').attr('content'),
				product: product,
				crement: crement
			},
			success: function (result) {
				contentCard(result.html);
				countProduct(result.count);
			}
		});
	});

	function contentCard(content) {
		$('#popup__content').html(content);
	}

	function countProduct(count) {
		$('#count-product-cart').html(count);
	}
	//end basket

	$('body').on('click', '.add-to-basket', function (e) {
		e.preventDefault();
		var product = $(this).data('product-id');

		$.ajax({
			type: 'post',
			url: '/basket/add-product',
			data: {
				'_token': $('meta[name="csrf-token"]').attr('content'),
				product: product
			},
			success: function (result) {
				contentCard(result.html);
				countProduct(result.count);
			}
		});
	});

	$('.cc__colors-item').on('click', function () {
		var productColor = $(this).attr('data-color');
		var app = $(this);
		$.ajax({
			type: 'get',
			url: '/color-product',
			data: {
				product_id: $('#color-text').attr('data-id'),
				product_color: productColor
			},
			success: function (result) {
				$('#color-text').html(result.html);
				var shopButton = app.closest('.container').find('.product-section__buttons a');
				var arr = shopButton.attr('href').split('/');
				arr[5] = productColor;
				var link = '';
				for(var i=0; i<arr.length; i++){
					link = link+arr[i];
					if(i != arr.length-1){
						link = link + '/';
					}
				}
				shopButton.attr('href', link);
				console.log(link);
			}
		});
	});

	$("#search-input").keyup(function () {
		var search = $.trim($("#search-input").val());

		if (search.length >= 1) {
			$.ajax({
				type: "GET",
				url: '/search?product_name=' + $('#search-input').val(),
				success: function (res) {
					$('#window-search').html(res.html);
				},
				error: function (res) {
					serverAnswer('.window-error');
				}
			});
			$("#window-search").css({'display': 'block'});
		} else {
			$("#window-search").css({'display': 'none'});
		}
	});

	$(".color-select").on('change', function() {
		var productColor = this.value;
		var productId = $(this).data('product-id');

		$.ajax({
			type: 'post',
			url: '/basket/color-product',
			data: {
				'_token': $('meta[name="csrf-token"]').attr('content'),
				product_id: productId,
				product_color: productColor
			},
			success: function (result) {
				alert('Вы изменили цвет в заказе')
				// contentCard(result.html);
				// countProduct(result.count);
			}
		});
	});

});