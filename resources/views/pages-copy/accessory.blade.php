@extends('layouts.layouts-main')

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="/">Главная</a> / <span>Модель аксессуара</span>
		</div>

		<div class="section__title m-tb">Люлька Cot to go</div>

		<div class="cc">
			<div class="cc__content">
				<div class="cc__images">
					<div class="pram-item__badge green">Limited edition</div>
					<div class="cc__image cc--idan" id="cc--idan-1" style="display: block;">
						<img src="{{ asset('images/kolaska_model/lulka.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-2">
						<img src="{{ asset('images/kolaska_model/lulka.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-3">
						<img src="{{ asset('images/kolaska_model/lulka.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-4">
						<img src="{{ asset('images/kolaska_model/lulka.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-5">
						<img src="{{ asset('images/kolaska_model/lulka.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
				</div>
				<div class="cc__body m-t">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, ipsa?</p>
					<p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, iure?</strong></p>
				</div>
				<div class="pram-item__price">4599 грн</div>
			</div>
			<div class="cc__colors">
				<div class="cc__colors-title">Цвета +</div>
				<div class="cc__colors-list">
					<a class="cc__colors-item green" data-key="idan" data-color-id="1"></a>
					<a class="cc__colors-item blue" data-key="idan" data-color-id="2"></a>
					<a class="cc__colors-item gray" data-key="idan" data-color-id="3"></a>
					<a class="cc__colors-item red" data-key="idan" data-color-id="4"></a>
					<a class="cc__colors-item black" data-key="idan" data-color-id="5"></a>
				</div>
			</div>
			<div class="cc__content">
				<div class="product-section__buttons m-b">
					<a href="#">Купить</a>
				</div>
			</div>
		</div>
	</div>
@stop