@extends('layouts.layouts-main')

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="/">Главная</a> / <span>Оформление заказа</span>
		</div>

		<div class="order">
			<div class="order__section">
				<div class="order__section-title">1. Данные пользователя</div>

				<label class="order__input">
					<input type="text" placeholder="Имя и фамилия">
				</label>

				<label class="order__input">
					<input type="text" placeholder="Контактный номер телефона">
				</label>

				<label class="order__input">
					<input type="text" placeholder="Контактный e-mail">
				</label>

				<label class="order__input">
					<input type="text" placeholder="Город доставки товара">
				</label>

				<div class="order__error">* Поле Город обязательно для заполнения</div>

			</div>

			<div class="order__section">
				<div class="order__section-title">2. Способ оплаты</div>

				<label class="order__checkbox">
					<input type="radio" name="test">
					<span class="order__checkbox-content">
						<span></span>
						<span>Наложенным платежом</span>
					</span>
				</label>

				<label class="order__checkbox">
					<input type="radio" name="test">
					<span class="order__checkbox-content">
						<span></span>
						<span>Оплата по банковским реквизитам</span>
					</span>
				</label>

				<label class="order__checkbox">
					<input type="radio" name="test">
					<span class="order__checkbox-content">
						<span></span>
						<span>Наличными курьеру (только при доставке курьером по Киеву)</span>
					</span>
				</label>

			</div>

			<div class="order__section m-tb p-b">
				<div class="order__section-title">3. Способ доставки</div>

				<label class="order__checkbox">
					<input type="radio" name="test2" id="new-post">
					<span class="order__checkbox-content">
						<span></span>
						<span>Новой почтой</span>
					</span>
				</label>

				<div id="new-post-block">
					<label for="new-post" class="order__select">
						<select name="" id="">
							<option value="">Авангард</option>
							<option value="">Киев</option>
							<option value="">Каменец-Подольский</option>
						</select>
					</label>
					<label for="new-post" class="order__select">
						<select name="" id="">
							<option value="">Отделение номер один</option>
							<option value="">Отделение №2</option>
							<option value="">Крч 3</option>
						</select>
					</label>
				</div>

				<label class="order__checkbox">
					<input type="radio" name="test2">
					<span class="order__checkbox-content">
						<span></span>
						<span>Курьером Новой почты по Киеву (при условии 100% оплаченного заказа)</span>
					</span>
				</label>

				<label class="order__checkbox">
					<input type="radio" name="test2">
					<span class="order__checkbox-content">
						<span></span>
						<span>Самовывоз из <a href="#">магазина</a> в Киеве</span>
					</span>
				</label>

				<label class="order__textarea">
					<span>Комментарий к заказу</span>
					<textarea></textarea>
				</label>

				<div class="product-section__buttons block tac">
					<button type="submit">Оформить заказ</button>
				</div>

			</div>

		</div>

	</div>
@stop