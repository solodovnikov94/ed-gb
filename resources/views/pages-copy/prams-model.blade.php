@extends('layouts.layouts-main')

@section('content')
	<div class="gb"></div>

	<div class="container">
		<div class="cc m-t">
			<div class="cc__content">
				<div class="cc__images bigger">
					<div class="cc__plane">
						<img src="{{ asset('images/kolaska_model/avia.png') }}" alt="Допускается на борт самолета">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-1" style="display: block;">
						<img src="{{ asset('images/kolaska_model/Pockit+_red.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-2">
						<img src="{{ asset('images/kolaska_model/Pockit+_red.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-3">
						<img src="{{ asset('images/kolaska_model/Pockit+_red.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-4">
						<img src="{{ asset('images/kolaska_model/Pockit+_red.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-5">
						<img src="{{ asset('images/kolaska_model/Pockit+_red.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
				</div>
			</div>
			<div class="cc__colors m-b p-b">
				<div class="cc__colors-title">Цвета +</div>
				<div class="cc__colors-list">
					<a class="cc__colors-item green" data-key="idan" data-color-id="1"></a>
					<a class="cc__colors-item blue" data-key="idan" data-color-id="2"></a>
					<a class="cc__colors-item gray" data-key="idan" data-color-id="3"></a>
					<a class="cc__colors-item red" data-key="idan" data-color-id="4"></a>
					<a class="cc__colors-item black" data-key="idan" data-color-id="5"></a>
				</div>
			</div>
		</div>

		<div class="videos-container">
			<div class="videos-container__videos">
				<div class="videos-container__videos-item">
					<iframe src="https://www.youtube.com/embed/qtHuVD1luQM?controls=1"
						frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>
				</div>
				<div class="videos-container__videos-item">
					<iframe src="https://www.youtube.com/embed/qtHuVD1luQM?controls=1"
						frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>
				</div>
				<div class="videos-container__videos-item">
					<iframe src="https://www.youtube.com/embed/qtHuVD1luQM?controls=1"
						frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>
				</div>
			</div>
			<div class="videos-container__content">

				<div class="pockit">
					<div class="pockit__image">
						<img src="{{ asset('images/kolaska_model/Pockit+.png') }}" alt="Pockit+">
						<div class="pockit__text" id="pockit-text-1">Вес 5,6 кг</div>
						<div class="pockit__text" id="pockit-text-2">Кнопки для<br> складывания<br> коляски</div>
						<div class="pockit__text" id="pockit-text-3">Большой капюшон<br> с УФ защитой</div>
						<div class="pockit__text" id="pockit-text-4">Ткань комфорт</div>
						<div class="pockit__text" id="pockit-text-5">Мягкие подушечки<br> на ремнях<br> безопасности</div>
						<div class="pockit__text" id="pockit-text-6">Угол наклона<br> спинки 30&deg;</div>
						<div class="pockit__text" id="pockit-text-7">Передние<br> манёвренные<br> колёса</div>
						<div class="pockit__text" id="pockit-text-8">Вместительная корзина</div>
					</div>
					<div class="pockit__bottom-text">Тип складывания - книжка<br>Складывается одной рукой<br>Стоит в сложенном виде</div>
					<div class="product-section__buttons">
						<a href="#" class="active">Купить</a>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="section gray-bg p-tb">
		<div class="container">
			<div class="advantages">
				<div class="advantage">
					<div class="advantage__image">
						<img src="{{ asset('images/kolaska_model/a_1.png') }}" alt="Компактность в сложенном виде">
					</div>
					<div class="advantage__title">Компактность в сложенном виде</div>
					<div class="advantage__description">
						20 x 32 x 38<br>
						Допускается на борт самолёта
					</div>
				</div>
				<div class="advantage">
					<div class="advantage__image">
						<img src="{{ asset('images/kolaska_model/a_2.png') }}" alt="Возможность использования с автокреслом">
					</div>
					<div class="advantage__title">Возможность использования с автокреслом</div>
					<div class="advantage__arrow">
						<img src="{{ asset('images/kolaska_model/arrow_down.png') }}" alt="">
					</div>
				</div>
				<div class="advantage">
					<div class="advantage__image">
						<img src="{{ asset('images/kolaska_model/a_3.png') }}" alt="Возможность использования с люлькой GB COT">
					</div>
					<div class="advantage__title">Возможность использования с люлькой GB COT</div>
					<div class="advantage__arrow">
						<img src="{{ asset('images/kolaska_model/arrow_down.png') }}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="accessories">
			<div class="section__subtitle m-tb">Аксессуары</div>
			<div id="accessories-slider">
				<div>
					<div class="accessory">
						<div class="accessory__title">Cot to go</div>
						<div class="accessory__image">
							<img src="{{ asset('images/kolaska_model/lulka.png') }}" alt="Cot to go">
						</div>
						<div class="pram-item__price m-tb">4599 грн</div>
						<table class="accessory__params">
							<tr>
								<td>Размер корзины:</td>
								<td>210х68х70</td>
							</tr>
							<tr>
								<td>Вес:</td>
								<td>22 кг</td>
							</tr>
							<tr>
								<td>Материал внешний:</td>
								<td>Кожа дракона</td>
							</tr>
							<tr>
								<td>Материал внутренний:</td>
								<td>Шкура единорога</td>
							</tr>
						</table>
						<div class="product-section__buttons">
							<a href="#">Купить</a>
						</div>
					</div>
				</div>
				<div>
					<div class="accessory">
						<div class="accessory__title">Cot to go</div>
						<div class="accessory__image">
							<img src="{{ asset('images/kolaska_model/lulka.png') }}" alt="Cot to go">
						</div>
						<div class="pram-item__price m-tb">4599 грн</div>
						<table class="accessory__params">
							<tr>
								<td>Размер корзины:</td>
								<td>210х68х70</td>
							</tr>
							<tr>
								<td>Вес:</td>
								<td>22 кг</td>
							</tr>
							<tr>
								<td>Материал внешний:</td>
								<td>Кожа дракона</td>
							</tr>
							<tr>
								<td>Материал внутренний:</td>
								<td>Шкура единорога</td>
							</tr>
						</table>
						<div class="product-section__buttons">
							<a href="#">Купить</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section gray-bg p-tb">
		<div class="section__subtitle p-t">Сопутствующий товар</div>
		<div class="pram-item">
			<div class="pram-item__title">
				<a href="#">Автокресло Artio</a>
			</div>
			<div class="pram-item__image m-t p-lr">
				<img src="{{ asset('images/kolaska_model/Idan.png') }}" alt="Автокресло Artio">
			</div>
			<div class="pram-item__price m-t">4599 грн</div>
			<div class="product-section__buttons">
				<a href="#">Купить</a>
				<a href="#">Узнать больше</a>
			</div>
		</div>
	</div>
@stop