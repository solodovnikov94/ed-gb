@extends('layouts.layouts-main')

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="/">Главная</a> / <span>Автокресло Idan</span>
		</div>

		<div class="section__title m-tb">Автокресло Idan</div>

		<div class="cc">
			<div class="cc__content">
				<div class="cc__images">
					<div class="cc__image cc--idan" id="cc--idan-1" style="display: block;">
						<img src="{{ asset('images/avtoklreslo/gb_PL_Idan_viertel_Capri_Blue copy.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-2">
						<img src="{{ asset('images/avtoklreslo/Idan_Plus_Headrest.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-3">
						<img src="{{ asset('images/avtoklreslo/Idan_Travelsystem_Maris.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-4">
						<img src="{{ asset('images/avtoklreslo/Idan_sun_canopy.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
					<div class="cc__image cc--idan" id="cc--idan-5">
						<img src="{{ asset('images/avtoklreslo/arrow_r_2.png') }}" alt="Автокресло Idan (зелёное)">
					</div>
				</div>
				<div class="cc__body m-t">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, ipsa?</p>
				</div>
				<div class="pram-item__price">4599 грн</div>
			</div>
			<div class="cc__colors">
				<div class="cc__colors-title">Цвета +</div>
				<div class="cc__colors-list">
					<a class="cc__colors-item green" data-key="idan" data-color-id="1"></a>
					<a class="cc__colors-item blue" data-key="idan" data-color-id="2"></a>
					<a class="cc__colors-item gray" data-key="idan" data-color-id="3"></a>
					<a class="cc__colors-item red" data-key="idan" data-color-id="4"></a>
					<a class="cc__colors-item black" data-key="idan" data-color-id="5"></a>
				</div>
			</div>
			<div class="cc__content">
				<div class="product-section__buttons m-b">
					<a href="#">Купить</a>
				</div>
			</div>
		</div>

	</div>

	<div class="section__video">
		<div class="container">
			<div class="section__video-content">
				<iframe src="https://www.youtube.com/embed/L6MKVCRdaXg"
								frameborder="0"
								allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen></iframe>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="section__title m-tb">Функции автокресла</div>

		<div class="seat-functions m-b p-b">

			<div id="seat-functions-slider">
				<div>
					<div class="seat-function">
						<div class="seat-function__image">
							<img src="{{ asset('images/avtoklreslo/Idan_Plus_Headrest.png') }}" alt="Название картинки">
						</div>
						<div class="seat-function__title">Защита, которая растёт вместе с ребёнком</div>
						<div class="seat-function__description">Регулированный по высоте, consectetur adipisicing elit. Aut beatae blanditiis consectetur cumque dolor dolorum ducimus et fuga fugit id incidunt ipsam labore modi
							nesciunt
							placeat praesentium quas, quasi quisquam ratione rem repellendus sapiente soluta ullam unde vitae, voluptatem voluptatum!</div>
					</div>
				</div>
				<div>
					<div class="seat-function">
						<div class="seat-function__image">
							<img src="{{ asset('images/avtoklreslo/Idan_Travelsystem_Maris.png') }}" alt="Название картинки">
						</div>
						<div class="seat-function__title">Система для путешествий</div>
						<div class="seat-function__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
					</div>
				</div>
				<div>
					<div class="seat-function">
						<div class="seat-function__image">
							<img src="{{ asset('images/avtoklreslo/Idan_sun_canopy.png') }}" alt="Название картинки">
						</div>
						<div class="seat-function__title">Регулируемый капюшон</div>
						<div class="seat-function__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi, blanditiis commodi consequuntur, exercitationem, incidunt nisi pariatur quis recusandae rem rerum sunt veritatis vitae!</div>
					</div>
				</div>
				<div>
					<div class="seat-function">
						<div class="seat-function__image">
							<img src="{{ asset('images/avtoklreslo/Idan_Plus_Headrest.png') }}" alt="Название картинки">
						</div>
						<div class="seat-function__title">Защита, которая растёт вместе с ребёнком</div>
						<div class="seat-function__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto asperiores cupiditate dolor dolores et iste nulla omnis quasi quidem, rem sapiente veniam veritatis voluptatibus. Amet aut, commodi corporis dignissimos excepturi itaque iure laudantium magnam neque odit optio pariatur, quam, qui.</div>
					</div>
				</div>
				<div>
					<div class="seat-function">
						<div class="seat-function__image">
							<img src="{{ asset('images/avtoklreslo/Idan_Travelsystem_Maris.png') }}" alt="Название картинки">
						</div>
						<div class="seat-function__title">Защита, которая растёт вместе с ребёнком</div>
						<div class="seat-function__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam consequatur consequuntur corporis cumque cupiditate deserunt dignissimos dolor eligendi eum facere fugiat ipsa, iste modi, odit, officiis optio perferendis perspiciatis quaerat quam quia recusandae reiciendis similique sint tempore ullam vel voluptatem!</div>
					</div>
				</div>
			</div>

			<div class="seat-functions__controls">
				<a class="seat-functions__controls-left" id="seat-functions--prev">
					<img src="{{ asset('images/icons/arrow_l_2.png') }}" alt="Предыдущий слайд">
				</a>
				<a class="seat-functions__controls-right" id="seat-functions--next">
					<img src="{{ asset('images/icons/arrow_r_2.png') }}" alt="Следующий слайд">
				</a>
			</div>

			<div class="product-section__buttons center">
				<a href="#">Купить</a>
			</div>
		</div>
	</div>
@stop