@extends('layouts.layouts-main')

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="/">Главная</a> / <span>Коляски</span>
		</div>

		<div class="tab-links">
			<a href="#" class="active">Все коляски</a>
			<a href="#">Gold</a>
			<a href="#">Platinum</a>
		</div>

		<div class="pram-item">
			<div class="pram-item__title">
				<a href="#">Очень длинное название классной модели колясок</a>
			</div>
			<div class="pram-item__image">
				<img src="{{ asset('images/prams/pockit+/gray.png') }}" alt="Очень длинное название классной модели колясок">
				<div class="pram-item__badge">Новинка</div>
			</div>
			<div class="pram-item__price">4599 грн</div>
			<div class="product-section__buttons">
				<a href="#">Купить</a>
				<a href="#">Узнать больше</a>
				<a href="#">Аксессуары</a>
			</div>
		</div>

		<div class="pram-item">
			<div class="pram-item__title">
				<a href="#">Название, модель</a>
			</div>
			<div class="pram-item__image">
				<img src="{{ asset('images/prams/pockit+/blue.png') }}" alt="Очень длинное название классной модели колясок">
				<div class="pram-item__badge green">Limited edition</div>
			</div>
			<div class="pram-item__price">4599 грн</div>
			<div class="product-section__buttons">
				<a href="#">Купить</a>
				<a href="#">Узнать больше</a>
				<a href="#">Аксессуары</a>
			</div>
		</div>

		<div class="pram-item">
			<div class="pram-item__title">
				<a href="#">Очень длинное название классной модели колясок</a>
			</div>
			<div class="pram-item__image">
				<img src="{{ asset('images/prams/pockit+/black.png') }}" alt="Очень длинное название классной модели колясок">
				<div class="pram-item__badge gray">Предзаказ</div>
			</div>
			<div class="pram-item__price">4599 грн</div>
			<div class="product-section__buttons">
				<a href="#">Купить</a>
				<a href="#">Узнать больше</a>
				<a href="#">Аксессуары</a>
			</div>
		</div>

		<div class="pram-item">
			<div class="pram-item__title">
				<a href="#">Очень длинное название классной модели колясок</a>
			</div>
			<div class="pram-item__image">
				<img src="{{ asset('images/prams/pockit+/green.png') }}" alt="Очень длинное название классной модели колясок">
			</div>
			<div class="pram-item__price">4599 грн</div>
			<div class="product-section__buttons">
				<a href="#">Купить</a>
				<a href="#">Узнать больше</a>
				<a href="#">Аксессуары</a>
			</div>
		</div>

		<ul class="pagination m-b p-b">
			<li><span>&lt;</span></li>
			<li class="active"><span>1</span></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li class="disabled"><span>...</span></li>
			<li><a href="#">104</a></li>
			<li><a href="#">&gt;</a></li>
		</ul>
	</div>
@stop