@extends('layouts.layouts-main')

@section('content')
	<div class="video-section">
		<iframe src="https://www.youtube.com/embed/qtHuVD1luQM?controls=1"
						frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>
		<div class="product-section__buttons">
			<a href="#" class="active">Купить</a>
		</div>
	</div>

	<div class="section">
		<div class="section__title m-b">Maris - прогулочний блок</div>

		<div class="container">
			<div class="videos-container p-t">
				<div class="videos-container__videos small">
					<div class="videos-container__videos-item">
						<iframe src="https://www.youtube.com/embed/qtHuVD1luQM?controls=1"
										frameborder="0"
										allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
										allowfullscreen></iframe>
					</div>
					<div class="videos-container__videos-item">
						<iframe src="https://www.youtube.com/embed/qtHuVD1luQM?controls=1"
										frameborder="0"
										allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
										allowfullscreen></iframe>
					</div>
					<div class="videos-container__videos-item">
						<iframe src="https://www.youtube.com/embed/qtHuVD1luQM?controls=1"
										frameborder="0"
										allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
										allowfullscreen></iframe>
					</div>
				</div>
				<div class="videos-container__content">

					<div class="pockit dd">
						<div class="pockit__image">
							<img src="{{ asset('images/daydream/daydream.png') }}" alt="Maris - прогулочний блок">
							<div class="pockit__text" id="dd-text-1">
								<span>Вес 12,2 кг</span>
							</div>
							<div class="pockit__text" id="dd-text-2">
								<span>Большой капюшон</span>
								<span class="pockit__subtext">С защитой от UF лучей</span>
							</div>
							<div class="pockit__text" id="dd-text-3">
								<span>Регулируемая спинка<br>с положением лёжа</span>
								<span class="pockit__subtext">Настройка горизонтального<br>положения одной рукой</span>
							</div>
							<div class="pockit__text" id="dd-text-4">
								<span>Кнопки "Memory"</span>
								<span class="pockit__subtext">Комфортно менять<br>направление прогулочного<br>блока</span>
							</div>
							<div class="pockit__text" id="dd-text-5">
								<span>Тормоз</span>
								<span class="pockit__subtext">Удобный и тихий</span>
							</div>
							<div class="pockit__text" id="dd-text-6">
								<span>Амортизация<br>на 4-х колесах</span>
								<span class="pockit__subtext">Плавная и спокойная езда</span>
							</div>
							<div class="pockit__text" id="dd-text-7">
								<span>Ширина шасси 54 см</span>
								<span class="pockit__text-arrows"></span>
							</div>
							<div class="pockit__text" id="dd-text-8">
								<span>Вместительная<br>корзина для покупок</span>
								<span class="pockit__subtext">С нагрузкой до 5 кг</span>
							</div>
							<div class="pockit__text" id="dd-text-9">
								<span>Резиновые колёса</span>
								<span class="pockit__subtext">Проколостойкие с пенным<br>наполнителем</span>
							</div>
							<div class="pockit__text" id="dd-text-10">
								<span>Подножка</span>
								<span class="pockit__subtext">С функцией настройки<br>в 3-х положениях</span>
							</div>
							<div class="pockit__text" id="dd-text-11">
								<span>Съемный, поворотный<br>бампер</span>
							</div>
							<div class="pockit__text" id="dd-text-12">
								<span>Мягкие ремни<br>безопасности</span>
								<span class="pockit__subtext">Комфорт для малыша</span>
							</div>
						</div>
						<div class="pockit__bottom-text">Тип складывания - книжка<br>Складывается одной рукой<br>Стоит в сложенном виде</div>
						<div class="product-section__buttons">
							<a href="#" class="active">Купить</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="section">
		<div class="pram-item p-lr">
			<div class="pram-item__title">
				<a href="#">Maris - люлька для новорожденного</a>
			</div>
			<div class="pram-item__image m-t p-lr">
				<div class="pram-item__badge red">Новинка</div>
				<img src="{{ asset('images/daydream/lulka.png') }}" alt="Maris - люлька для новорожденного">
			</div>
			<div class="pram-item__price m-t">4599 грн</div>
			<div class="pram-item__details">
				<div class="pram-item__details-left">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur deleniti eaque incidunt laudantium mollitia repellendus!
				</div>
				<div class="pram-item__details-center">
					<table class="accessory__params m-t">
						<tr>
							<td>Размер корзины:</td>
							<td>210х68х70</td>
						</tr>
						<tr>
							<td>Вес:</td>
							<td>22 кг</td>
						</tr>
						<tr>
							<td>Материал внешний:</td>
							<td>Кожа дракона</td>
						</tr>
						<tr>
							<td>Материал внутренний:</td>
							<td>Шкура единорога</td>
						</tr>
					</table>
				</div>
				<div class="pram-item__details-right">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, magni!
				</div>
			</div>
			<div class="product-section__buttons">
				<a href="#">Купить</a>
			</div>
		</div>
	</div>

	<div class="section gray-bg p-tb">
		<div class="section__subtitle p-t">Сопутствующий товар</div>
		<div class="pram-item">
			<div class="pram-item__title">
				<a href="#">Автокресло Idan 0+</a>
			</div>
			<div class="pram-item__image m-t p-lr">
				<img src="{{ asset('images/daydream/idan.png') }}" alt="Автокресло Idan 0+">
			</div>
			<div class="pram-item__price m-t">4599 грн</div>
			<div class="product-section__buttons">
				<a href="#">Купить</a>
				<a href="#">Узнать больше</a>
			</div>
		</div>
	</div>
@stop