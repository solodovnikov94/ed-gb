@extends('layouts.layouts-main')

@section('content')
	<div class="product-section pt" style="background-image: url('{{ asset('images/bg1.jpg') }}')">
		<div class="product-section__title">Maris day dream — Система 3 в 1</div>
		<div class="product-section__buttons">
			<a href="#">Купить</a>
			<a href="#">Узнать больше</a>
			<a href="#">Аксессуары</a>
		</div>
		<div class="product-section__image">
			<img src="{{ asset('images/home/Maris.png') }}" alt="Maris day dream — Система 3 в 1">
		</div>
	</div>

	<div class="product-section" style="background-image: url('{{ asset('images/bg2.jpg') }}')">
		<div class="product-section__title">Pockit+</div>
		<div class="product-section__buttons">
			<a href="#">Купить</a>
			<a href="#">Узнать больше</a>
			<a href="#">Аксессуары</a>
		</div>
		<div class="product-section__image">
			<img src="{{ asset('images/home/Pockit+.png') }}" alt="Pockit+">
		</div>
	</div>

	<div class="product-section" style="background-image: url('{{ asset('images/bg3.jpg') }}')">
		<div class="product-section__title">Автокресло Idan 0+</div>
		<div class="product-section__buttons">
			<a href="#">Купить</a>
			<a href="#">Узнать больше</a>
		</div>
		<div class="product-section__image">
			<img src="{{ asset('images/home/gb_PL_Idan_viertel_Capri_Blue.png') }}" alt="Автокресло Idan 0+">
		</div>
	</div>

	<div class="product-section" style="background-image: url('{{ asset('images/bg4.jpg') }}')">
		<div class="product-section__title">Vaya I-size Гр. 0+/1</div>
		<div class="product-section__buttons">
			<a href="#">Купить</a>
			<a href="#">Узнать больше</a>
		</div>
		<div class="product-section__image">
			<img src="{{ asset('images/home/Vaya_i_Size.png') }}" alt="Vaya I-size Гр. 0+/1">
		</div>
	</div>
@stop