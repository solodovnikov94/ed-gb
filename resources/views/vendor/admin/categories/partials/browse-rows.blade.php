@foreach($dataTypeContent->sortBy('order') as $data)
    @if(!$data->parent)
    <tr @isset($data->id) data-id="{{ $data->id }}" @endisset class="parent">
        @if(!isset($dataTypeOptions->datatable->rowReorder))
            <td>
                <input type="checkbox" name="row_id" id="checkbox_{{ $data->id }}" value="{{ $data->id }}">
            </td>
        @endif
        @foreach($dataType->browseRows as $row)
            <td>
                <?php $options = json_decode($row->details); ?>
                @if($row->type == 'image')
                    <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Admin::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif"
                         style="width:100px">
                @elseif($row->type == 'relationship')
                    @include('admin::formfields.relationship', ['view' => 'browse'])
                @elseif($row->type == 'select_multiple')
                    @if(property_exists($options, 'relationship'))
                        @foreach($data->{$row->field} as $item)
                            @if($item->{$row->field . '_page_slug'})
                                <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last)
                                    , @endif
                            @else
                                {{ $item->{$row->field} }}
                            @endif
                        @endforeach

                        {{-- $data->{$row->field}->implode($options->relationship->label, ', ') --}}
                    @elseif(property_exists($options, 'options') && !empty($data->{$row->field}))
                        @foreach(json_decode($data->{$row->field}) as $item)
                            {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                        @endforeach
                    @endif

                @elseif($row->type == 'select_dropdown' && property_exists($options, 'options'))

                    @if($data->{$row->field . '_page_slug'})
                        <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $options->options->{$data->{$row->field}} !!}</a>
                    @else
                        {!! $options->options->{$data->{$row->field}} !!}
                    @endif


                @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                    <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                @elseif($row->type == 'date')
                    {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}
                @elseif($row->type == 'checkbox')
                    @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                        @if($data->{$row->field})
                            <span class="label label-info">{{ $options->on }}</span>
                        @else
                            <span class="label label-primary">{{ $options->off }}</span>
                        @endif
                    @else
                        {{ $data->{$row->field} }}
                    @endif
                @elseif($row->type == 'color')
                    <span class="badge badge-lg"
                          style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                @elseif($row->type == 'text')
                    @include('admin::multilingual.input-hidden-bread-browse')
                    <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                @elseif($row->type == 'text_area')
                    @include('admin::multilingual.input-hidden-bread-browse')
                    <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                    @include('admin::multilingual.input-hidden-bread-browse')
                    @if(json_decode($data->{$row->field}))
                        @foreach(json_decode($data->{$row->field}) as $file)
                            <a href="{{ Storage::disk(config('admin.storage.disk'))->url($file->download_link) ?: '' }}"
                               target="_blank">
                                {{ $file->original_name ?: '' }}
                            </a>
                            <br/>
                        @endforeach
                    @else
                        <a href="{{ Storage::disk(config('admin.storage.disk'))->url($data->{$row->field}) }}"
                           target="_blank">
                            Download
                        </a>
                    @endif
                @elseif($row->type == 'rich_text_box')
                    @include('admin::multilingual.input-hidden-bread-browse')
                    <div class="readmore">{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                @elseif($row->type == 'coordinates')
                    @include('admin::partials.coordinates-static-image')
                @else
                    @include('admin::multilingual.input-hidden-bread-browse')
                    <span>{{ $data->{$row->field} }}</span>
                @endif
            </td>
        @endforeach
        <td class="no-sort no-click" id="crud-actions">
            @can('delete', $data)
                @unless($customServiceButtons && is_object($customServiceButtons) && property_exists($customServiceButtons, 'delete'))
                    <a href="javascript:;" title="{{ __('admin.generic.delete') }}"
                       class="btn btn-sm pull-right btn-danger delete" data-id="{{ $data->{$data->getKeyName()} }}"
                       id="delete-{{ $data->{$data->getKeyName()} }}">
                        <i class="admin-trash"></i> <span
                                class="hidden-xs hidden-sm">{{ $serviceButtons['delete']['title'] }}</span>
                    </a>
                @endunless
            @endcan
            @can('edit', $data)
                @unless($customServiceButtons && is_object($customServiceButtons) && property_exists($customServiceButtons, 'delete'))
                    <a href="{{ route('admin.'.$dataType->slug.'.edit', $data->{$data->getKeyName() }) }}?{{ $requestQuery }}"
                       title="{{ __('admin.generic.edit') }}" class="btn btn-sm pull-right btn-primary edit">
                        <i class="admin-edit"></i> <span
                                class="hidden-xs hidden-sm">{{ $serviceButtons['edit']['title'] }}</span>
                    </a>
                @endunless
            @endcan
            @can('read', $data)
                @unless($customServiceButtons && is_object($customServiceButtons) && property_exists($customServiceButtons, 'read'))
                    <a href="{{ route('admin.'.$dataType->slug.'.show', $data->{$data->getKeyName()}) }}?{{ $requestQuery }}"
                       title="{{ __('admin.generic.view') }}" class="btn btn-sm btn-warning pull-right">
                        <i class="admin-eye"></i>
                        <span class="hidden-xs hidden-sm">{{ $serviceButtons['read']['title'] }}</span>
                    </a>
                @endunless
            @endcan
            @if(isset($dataTypeOptions->browse->service_buttons))
                @foreach($dataTypeOptions->browse->service_buttons as $serviceButton)
                    @can(isset($serviceButton->apply_permission) ? $serviceButton->apply_permission : 'browse', $data)
                        @if(isset($serviceButton->attribute) && isset($data->{$serviceButton->attribute}) && $data->{$serviceButton->attribute})
                            <a data-id="{{ $data->{$data->getKeyName()} }}"
                               data-title="{{ isset($serviceButton->title) ? $serviceButton->title : '' }}"
                               title="{{ isset($serviceButton->title) ? $serviceButton->title : '' }}"
                               class="btn btn-sm pull-right {{ isset($serviceButton->class) ? $serviceButton->class : '' }}"
                               href="{{ isset($serviceButton->attribute) && isset($data->{$serviceButton->attribute}) ? $data->{$serviceButton->attribute} : '#' }}"
                               target="{{ isset($serviceButton->target) ? $serviceButton->target : '_self' }}">
                                @if(isset($serviceButton->icon))
                                    <i class="{{ $serviceButton->icon }}"></i>
                                @endif
                                @if(config('admin.views.browse.display_text_on_service_buttons'))
                                    <span class="hidden-xs hidden-sm">{{ isset($serviceButton->title) ? $serviceButton->title : '' }}</span>
                                @endif
                            </a>
                        @endif
                    @endcan
                @endforeach
            @endif
        </td>
    </tr>
    @endif
    @if($data->children)
        @foreach($data->children->sortBy('order') as $data)

            <tr @isset($data->id) data-id="{{ $data->id }}" @endisset class="children">
            @if(!isset($dataTypeOptions->datatable->rowReorder))
                <td>
                    <input type="checkbox" name="row_id" id="checkbox_{{ $data->id }}" value="{{ $data->id }}">
                </td>
            @endif
            @foreach($dataType->browseRows as $row)
                <td>
                    <?php $options = json_decode($row->details); ?>
                    @if($row->type == 'image')
                        <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Admin::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif"
                             style="width:100px">
                    @elseif($row->type == 'relationship')
                        @include('admin::formfields.relationship', ['view' => 'browse'])
                    @elseif($row->type == 'select_multiple')
                        @if(property_exists($options, 'relationship'))

                            @foreach($data->{$row->field} as $item)
                                @if($item->{$row->field . '_page_slug'})
                                    <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last)
                                        , @endif
                                @else
                                    {{ $item->{$row->field} }}
                                @endif
                            @endforeach

                            {{-- $data->{$row->field}->implode($options->relationship->label, ', ') --}}
                        @elseif(property_exists($options, 'options') && !empty($data->{$row->field}))
                            @foreach(json_decode($data->{$row->field}) as $item)
                                {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                            @endforeach
                        @endif

                    @elseif($row->type == 'select_dropdown' && property_exists($options, 'options'))

                        @if($data->{$row->field . '_page_slug'})
                            <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $options->options->{$data->{$row->field}} !!}</a>
                        @else
                            {!! $options->options->{$data->{$row->field}} !!}
                        @endif


                    @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                        <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                    @elseif($row->type == 'date')
                        {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}
                    @elseif($row->type == 'checkbox')
                        @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                            @if($data->{$row->field})
                                <span class="label label-info">{{ $options->on }}</span>
                            @else
                                <span class="label label-primary">{{ $options->off }}</span>
                            @endif
                        @else
                            {{ $data->{$row->field} }}
                        @endif
                    @elseif($row->type == 'color')
                        <span class="badge badge-lg"
                              style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                    @elseif($row->type == 'text')
                        @include('admin::multilingual.input-hidden-bread-browse')
                        <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                    @elseif($row->type == 'text_area')
                        @include('admin::multilingual.input-hidden-bread-browse')
                        <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                    @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                        @include('admin::multilingual.input-hidden-bread-browse')
                        @if(json_decode($data->{$row->field}))
                            @foreach(json_decode($data->{$row->field}) as $file)
                                <a href="{{ Storage::disk(config('admin.storage.disk'))->url($file->download_link) ?: '' }}"
                                   target="_blank">
                                    {{ $file->original_name ?: '' }}
                                </a>
                                <br/>
                            @endforeach
                        @else
                            <a href="{{ Storage::disk(config('admin.storage.disk'))->url($data->{$row->field}) }}"
                               target="_blank">
                                Download
                            </a>
                        @endif
                    @elseif($row->type == 'rich_text_box')
                        @include('admin::multilingual.input-hidden-bread-browse')
                        <div class="readmore">{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                    @elseif($row->type == 'coordinates')
                        @include('admin::partials.coordinates-static-image')
                    @else
                        @include('admin::multilingual.input-hidden-bread-browse')
                        <span>{{ $data->{$row->field} }}</span>
                    @endif
                </td>
            @endforeach
            <td class="no-sort no-click" id="crud-actions">
                @can('delete', $data)
                    @unless($customServiceButtons && is_object($customServiceButtons) && property_exists($customServiceButtons, 'delete'))
                        <a href="javascript:;" title="{{ __('admin.generic.delete') }}"
                           class="btn btn-sm pull-right btn-danger delete" data-id="{{ $data->{$data->getKeyName()} }}"
                           id="delete-{{ $data->{$data->getKeyName()} }}">
                            <i class="admin-trash"></i> <span
                                    class="hidden-xs hidden-sm">{{ $serviceButtons['delete']['title'] }}</span>
                        </a>
                    @endunless
                @endcan
                @can('edit', $data)
                    @unless($customServiceButtons && is_object($customServiceButtons) && property_exists($customServiceButtons, 'delete'))
                        <a href="{{ route('admin.'.$dataType->slug.'.edit', $data->{$data->getKeyName() }) }}?{{ $requestQuery }}"
                           title="{{ __('admin.generic.edit') }}" class="btn btn-sm pull-right btn-primary edit">
                            <i class="admin-edit"></i> <span
                                    class="hidden-xs hidden-sm">{{ $serviceButtons['edit']['title'] }}</span>
                        </a>
                    @endunless
                @endcan
                @can('read', $data)
                    @unless($customServiceButtons && is_object($customServiceButtons) && property_exists($customServiceButtons, 'read'))
                        <a href="{{ route('admin.'.$dataType->slug.'.show', $data->{$data->getKeyName()}) }}?{{ $requestQuery }}"
                           title="{{ __('admin.generic.view') }}" class="btn btn-sm btn-warning pull-right">
                            <i class="admin-eye"></i>
                            <span class="hidden-xs hidden-sm">{{ $serviceButtons['read']['title'] }}</span>
                        </a>
                    @endunless
                @endcan
                @if(isset($dataTypeOptions->browse->service_buttons))
                    @foreach($dataTypeOptions->browse->service_buttons as $serviceButton)
                        @can(isset($serviceButton->apply_permission) ? $serviceButton->apply_permission : 'browse', $data)
                            @if(isset($serviceButton->attribute) && isset($data->{$serviceButton->attribute}) && $data->{$serviceButton->attribute})
                                <a data-id="{{ $data->{$data->getKeyName()} }}"
                                   data-title="{{ isset($serviceButton->title) ? $serviceButton->title : '' }}"
                                   title="{{ isset($serviceButton->title) ? $serviceButton->title : '' }}"
                                   class="btn btn-sm pull-right {{ isset($serviceButton->class) ? $serviceButton->class : '' }}"
                                   href="{{ isset($serviceButton->attribute) && isset($data->{$serviceButton->attribute}) ? $data->{$serviceButton->attribute} : '#' }}"
                                   target="{{ isset($serviceButton->target) ? $serviceButton->target : '_self' }}">
                                    @if(isset($serviceButton->icon))
                                        <i class="{{ $serviceButton->icon }}"></i>
                                    @endif
                                    @if(config('admin.views.browse.display_text_on_service_buttons'))
                                        <span class="hidden-xs hidden-sm">{{ isset($serviceButton->title) ? $serviceButton->title : '' }}</span>
                                    @endif
                                </a>
                            @endif
                        @endcan
                    @endforeach
                @endif
            </td>
        </tr>
        @endforeach
    @endif
@endforeach