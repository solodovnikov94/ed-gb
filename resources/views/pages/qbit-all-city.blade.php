@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $product->meta_title }}</title>
	<meta name="description" content="{{ $product->meta_description }}">
	<meta name="keywords" content="{{ $product->meta_keywords }}">

	<meta property="og:title" content="{{ $product->meta_title }}">
	<meta property="og:description"
				content="{{ $product->meta_description }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ url(Request::path()) }}">
	<meta property="og:image" content="{{ $product->ogImage }}">
	@if(!$product->active)
		<meta name="robots" content="noindex, nofollow">
	@endif
	@include('includes.open-graph')
@stop

@section('content')
	<div class="qbit-section m-b">
		<div class="qbit-section__image" style="background-image: url('{{ asset('images/new/001.png') }}')"></div>
		<div class="qbit-section__content">
			<div class="qbit-section__top">
				<div class="container">
					<div class="breadcrumbs">
						<a href="{{ url('/')  }}">Главная</a> / <span>{{ $product->title }}</span>
					</div>

					<div class="section__title m-tb">{{ $product->title }}</div>
				</div>
			</div>
			<div class="qbit-section__bottom">
				<div class="container">
					<div class="pram-item__price m-t">{{ $product->price }} грн</div>
					<div class="product-section__buttons">
						{{--<a href="#">Все модели Qbit</a>--}}
						<a href="{{ $product->addToCart }}">Купить</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		@include('includes.product-image-block', [
			'product' => $product,
			'show_price' => true,
			'show_body' => false,
			'fly_badge' => false,
			'custom_class' => false,
			'big_image' => false
		])
	</div>

	<div class="section m-t">
		<div class="container">
			<div class="videos-container p-t">
				<div class="videos-container__videos">
					<div class="subsection-title">&nbsp;</div>
					@if($product->video)
						<div class="videos-container__videos-item">
							<iframe src="{{ $product->video }}?controls=1"
											frameborder="0"
											allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
											allowfullscreen></iframe>
						</div>
					@endif
					@if(count($product->productVideos))
						@foreach($product->productVideos as $video)
							<div class="videos-container__videos-item">
								<iframe src="{{ $video->video }}?controls=1"
												frameborder="0"
												allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
												allowfullscreen></iframe>
							</div>
						@endforeach
					@endif
				</div>
				<div class="videos-container__content">
					<div class="subsection-title">GB QBIT+</div>

					<div class="pockit qbit-all-terrain">
						<div class="pockit__image">
							<img src="{{ asset('images/new/Qbit+city.png') }}" alt="GB QBIT+">
							<div class="pockit__text" id="qbit-all-city-text-1">Кнопка для<br>складывания<br>коляски</div>
							<div class="pockit__text" id="qbit-all-city-text-2">2 смотровых<br>окошка</div>
							<div class="pockit__text" id="qbit-all-city-text-3">Комфортная<br>ткань</div>
							<div class="pockit__text" id="qbit-all-city-text-4">Поворотный<br>съемный<br>бампер</div>
							<div class="pockit__text" id="qbit-all-city-text-5">5-точечные ремни<br>безопасности</div>
							<div class="pockit__text" id="qbit-all-city-text-6">Горизонтальное<br>положение<br>спинки</div>
							<div class="pockit__text" id="qbit-all-city-text-7">Поворотные<br>передние<br>колеса</div>
							<div class="pockit__text" id="qbit-all-city-text-8">Вместительная<br>корзина<br>(до 5 кг)</div>
							<div class="pockit__text" id="qbit-all-city-text-9">Регулируемая<br>подножка</div>
							<div class="pockit__text" id="qbit-all-city-text-10">Большой<br>капюшон с<br>защитой UF 50+</div>
							<div class="pockit__text" id="qbit-all-city-text-11">Вес: 7,5 кг</div>
						</div>
						<div class="pockit__bottom-text">Механизм сложения Qbit+ All-City<br>позволяет за считанные секунды сложить<br>коляску до компактного размера (43х26х47см)</div>
						<div class="product-section__buttons">
							<a href="{{ $product->addToCart }}" class="active">Купить</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	@include('includes.product-functions', ['product' => $product])

	@include('includes.product-accessories', ['product' => $product])
	<br>
	@include('includes.accompanying-goods', ['product' => $product])
@stop