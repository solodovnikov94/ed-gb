@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $product->meta_title }}</title>
	<meta name="description" content="{{ $product->meta_description }}">
	<meta name="keywords" content="{{ $product->meta_keywords }}">

	<meta property="og:title" content="{{ $product->meta_title }}">
	<meta property="og:description"
				content="{{ $product->meta_description }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ url(Request::path()) }}">
	<meta property="og:image" content="{{ $product->ogImage }}">
	@if(!$product->active)
		<meta name="robots" content="noindex, nofollow">
	@endif
	@include('includes.open-graph')
@stop

@section('content')
	<div class="video-section">
		<iframe src="{{ $product->video }}?controls=1"
						frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>
		<div class="product-section__buttons">
			<a href="{{ $product->addToCart }}" class="active">Купить</a>
		</div>
	</div>

	<div class="section">
		<div class="section__title m-b">{{ $product->title }} - прогулочний блок</div>

		<div class="container">
			<div class="videos-container p-t">
				<div class="videos-container__videos small">
					@if(count($product->productVideos))
						@foreach($product->productVideos as $video)
							<div class="videos-container__videos-item">
								<iframe src="{{ $video->video }}?controls=1"
												frameborder="0"
												allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
												allowfullscreen></iframe>
							</div>
						@endforeach
					@endif
				</div>
				<div class="videos-container__content">

					<div class="pockit dd">
						<div class="pockit__image">
							<img src="{{ asset('images/daydream/daydream.png') }}" alt="Maris - прогулочний блок">
							<div class="pockit__text" id="dd-text-1">
								<span>Вес 12,2 кг</span>
							</div>
							<div class="pockit__text" id="dd-text-2">
								<span>Большой капюшон</span>
								<span class="pockit__subtext">С защитой от UF лучей</span>
							</div>
							<div class="pockit__text" id="dd-text-3">
								<span>Регулируемая спинка<br>с положением лёжа</span>
								<span class="pockit__subtext">Настройка горизонтального<br>положения одной рукой</span>
							</div>
							<div class="pockit__text" id="dd-text-4">
								<span>Кнопки "Memory"</span>
								<span class="pockit__subtext">Комфортно менять<br>направление прогулочного<br>блока</span>
							</div>
							<div class="pockit__text" id="dd-text-5">
								<span>Тормоз</span>
								<span class="pockit__subtext">Удобный и тихий</span>
							</div>
							<div class="pockit__text" id="dd-text-6">
								<span>Амортизация<br>на 4-х колесах</span>
								<span class="pockit__subtext">Плавная и спокойная езда</span>
							</div>
							<div class="pockit__text" id="dd-text-7">
								<span>Ширина шасси 54 см</span>
								<span class="pockit__text-arrows"></span>
							</div>
							<div class="pockit__text" id="dd-text-8">
								<span>Вместительная<br>корзина для покупок</span>
								<span class="pockit__subtext">С нагрузкой до 5 кг</span>
							</div>
							<div class="pockit__text" id="dd-text-9">
								<span>Резиновые колёса</span>
								<span class="pockit__subtext">Проколостойкие с пенным<br>наполнителем</span>
							</div>
							<div class="pockit__text" id="dd-text-10">
								<span>Подножка</span>
								<span class="pockit__subtext">С функцией настройки<br>в 3-х положениях</span>
							</div>
							<div class="pockit__text" id="dd-text-11">
								<span>Съемный, поворотный<br>бампер</span>
							</div>
							<div class="pockit__text" id="dd-text-12">
								<span>Мягкие ремни<br>безопасности</span>
								<span class="pockit__subtext">Комфорт для малыша</span>
							</div>
						</div>
						<div class="pockit__bottom-text">Тип складывания - книжка<br>Складывается одной рукой<br>Стоит в сложенном виде</div>
						<div class="pram-item__price m-t">{{ $product->price }} грн</div>
						<div class="product-section__buttons">
							<a href="{{ $product->addToCart }}" class="active">Купить</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	@if(count($product->accessoriesRelationList))
		<div id="accessories-anchor"></div>
		<div class="container">
			<div class="accessories">
				<div class="section__subtitle m-tb">Аксессуары</div>
				<div id="accessories-slider">
					@foreach($product->accessoriesRelationList as $item)
						<div>
							<div class="accessory">
								<a href="{{ $item->url }}" class="accessory__title">{{ $item->title }}</a>
								<div class="accessory__image">
									<img src="/storage/{{ $item->image }}" alt="{{ $item->title }}">
								</div>
								<div class="pram-item__price m-tb">{{ $item->price }} грн</div>
								<div class="accessory__description">
									{!! $item->body !!}
								</div>
								<div class="product-section__buttons">
									<a href="{{ $item->addToCart }}">Купить</a>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	@endif

	@include('includes.accompanying-goods', ['product' => $product])

@stop