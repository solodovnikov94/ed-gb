@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $page->meta_title }}</title>
	<meta name="description" content="{{ $page->meta_description }}">
	<meta name="keywords" content="{{ $page->meta_keywords }}">
@stop

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="{{ url('/') }}">Главная</a> / <span>Оформление заказа</span>
		</div>

		<div class="thanks">
			<div class="thanks__slogan">Future perfect</div>
			<div class="thanks__title">Спасибо за заказ!</div>
			<div class="thanks__subtitle">Наш консультант свяжется с Вами<br> для подтверждения заказа.</div>

			<div class="product-section__buttons">
				<a href="{{ url('/') }}" class="width-auto">Продолжить покупки</a>
			</div>
		</div>

	</div>
@stop