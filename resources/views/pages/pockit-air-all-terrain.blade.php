@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $product->meta_title }}</title>
	<meta name="description" content="{{ $product->meta_description }}">
	<meta name="keywords" content="{{ $product->meta_keywords }}">

	<meta property="og:title" content="{{ $product->meta_title }}">
	<meta property="og:description"
				content="{{ $product->meta_description }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ url(Request::path()) }}">
	<meta property="og:image" content="{{ $product->ogImage }}">
	@if(!$product->active)
		<meta name="robots" content="noindex, nofollow">
	@endif
	@include('includes.open-graph')

@stop

@section('content')
	<div class="qbit-section m-b">
		<div class="qbit-section__image" style="background-image: url('{{ asset('images/preloader.jpg') }}')"></div>
		<div class="qbit-section__content">
			<div class="qbit-section__top">
				<div class="container">
					<div class="breadcrumbs">
						<a href="{{ url('/')  }}">Главная</a> / <span>{{ $product->title }}</span>
					</div>

					<div class="section__title m-tb">{{ $product->title }}</div>
				</div>
			</div>
			<div class="qbit-section__bottom">
				<div class="container">
					<div class="pram-item__price m-t">{{ $product->price }} грн</div>
					<div class="product-section__buttons">
						<a href="{{ $product->addToCart }}">Купить</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		@include('includes.product-image-block', [
			'product' => $product,
			'show_price' => true,
			'show_body' => false,
			'fly_badge' => true,
			'custom_class' => 'm-t',
			'big_image' => true
		])

		<div class="videos-container">
			<div class="videos-container__videos">
				<div class="subsection-title">Легкая, как пёрышко!</div>
				@if($product->video)
				<div class="videos-container__videos-item">
					<iframe src="{{ $product->video }}?controls=1"
						frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>
				</div>
				@endif
				@if(count($product->productVideos))
					@foreach($product->productVideos as $video)
						<div class="videos-container__videos-item">
							<iframe src="{{ $video->video }}?controls=1"
											frameborder="0"
											allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
											allowfullscreen></iframe>
						</div>
					@endforeach
				@endif
			</div>
			<div class="videos-container__content">
				<div class="subsection-title">{{ $product->title }}</div>

				<div class="pockit pockit-air">
					<div class="pockit__image">
						<img src="{{ asset('images/new/GB_19_y045_EU_NIBL_PockitAir_All-Terrain_screen_HD.jpg') }}" alt="Pockit Air All-Terrain">
						<div class="pockit__text" id="pockit-text-1">Вес 4,6 кг</div>
						<div class="pockit__text" id="pockit-text-2">Кнопки для<br> складывания<br> коляски</div>
						<div class="pockit__text" id="pockit-text-3">Навес от солнца</div>
						<div class="pockit__text" id="pockit-text-4">Автоматический<br>замок в<br>сложенном виде</div>
						<div class="pockit__text" id="pockit-text-5">5-точечные<br> ремни<br> безопасности</div>
						<div class="pockit__text" id="pockit-text-6">Вместительная<br>корзина (до 5кг)</div>
						<div class="pockit__text" id="pockit-text-7">Двойные<br> колёса</div>
						<div class="pockit__text" id="pockit-text-8">Поворотные<br>передние колеса</div>
					</div>
					<div class="pockit__bottom-text">Механизм сложения Pockit+ All-Terrain<br> позволяет за считанные секунды сложить <br>коляску до компактного размера (18х30х35см)</div>
					<div class="product-section__buttons">
						<a href="{{ $product->addToCart }}" class="active">Купить</a>
					</div>
				</div>

			</div>
		</div>
	</div>

	@include('includes.product-functions', ['product' => $product])

	@include('includes.product-accessories', ['product' => $product])
	<br>
	@include('includes.accompanying-goods', ['product' => $product])

@stop