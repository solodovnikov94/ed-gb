@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $page->meta_title }}</title>
	<meta name="description" content="{{ $page->meta_description }}">
	<meta name="keywords" content="{{ $page->meta_keywords }}">
@stop

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="{{ url('/') }}">Главная</a> / <span>Оформление заказа</span>
		</div>

		<div class="order">
			<form action="{{ url('save-order') }}" method="post">
			@csrf
			<div class="order__section">
				<div class="order__section-title">1. Данные пользователя</div>

				<label class="order__input">
					<input type="text" name="fio" placeholder="ФИО">
				</label>
				<div class="order__error" id="error-fio"></div>

				<label class="order__input">
					<input type="text" name="phone" placeholder="Контактный номер телефона">
				</label>
				<div class="order__error" id="error-phone"></div>

				<label class="order__input">
					<input type="text" name="email" placeholder="Контактный e-mail">
				</label>
				<div class="order__error" id="error-email"></div>

				<label class="order__input">
					<input type="text" name="city" placeholder="Город доставки товара">
				</label>
				<div class="order__error" id="error-city"></div>

			</div>

			<div class="order__section">
				<div class="order__section-title">2. Способ оплаты</div>

				<label class="order__checkbox">
					<input type="radio" name="payment" value="1">
					<span class="order__checkbox-content">
						<span></span>
						<span>Наложенным платежом</span>
					</span>
				</label>

				<label class="order__checkbox">
					<input type="radio" name="payment" value="2">
					<span class="order__checkbox-content">
						<span></span>
						<span>Оплата по банковским реквизитам</span>
					</span>
				</label>

				<label class="order__checkbox">
					<input type="radio" name="payment" value="3">
					<span class="order__checkbox-content">
						<span></span>
						<span>Наличными курьеру (только при доставке курьером по Киеву)</span>
					</span>
				</label>
				<div class="order__error" id="error-payment"></div>

			</div>

			<div class="order__section m-tb p-b">
				<div class="order__section-title">3. Способ доставки</div>

				<label class="order__checkbox">
					<input type="radio" name="delivery" id="new-post" value="1">
					<span class="order__checkbox-content">
						<span></span>
						<span>Новой почтой</span>
					</span>
				</label>

				<div id="new-post-block" style="display: none">
					<label for="new-post" class="order__select">
						<select name="city_np" id="cities">
						</select>
					</label>
					<input type="hidden" id="np_city_hidden" name="np_city_hidden">
					<label for="new-post" class="order__select">
						<select name="warehouses_np" id="warehouses">
						</select>
					</label>
				</div>

				<label class="order__checkbox">
					<input type="radio" name="delivery" value="2">
					<span class="order__checkbox-content">
						<span></span>
						<span>Курьером Новой почты по Киеву (при условии 100% оплаченного заказа)</span>
					</span>
				</label>

				<label class="order__checkbox">
					<input type="radio" name="delivery" value="3">
					<span class="order__checkbox-content">
						<span></span>
						<span>Самовывоз из магазина в Киеве</span>
					</span>
				</label>
				<div class="order__error" id="error-delivery"></div>

				<label class="order__textarea">
					<span>Комментарий к заказу</span>
					<textarea name="description"></textarea>
				</label>

				<div class="product-section__buttons block tac">
					<button type="submit" id="send-order-from">Оформить заказ</button>
				</div>
			</div>
			</form>
		</div>

	</div>
@stop