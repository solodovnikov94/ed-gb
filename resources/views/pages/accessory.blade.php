@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $product->meta_title }}</title>
	<meta name="description" content="{{ $product->meta_description }}">
	<meta name="keywords" content="{{ $product->meta_keywords }}">

	<meta property="og:title" content="{{ $product->meta_title }}">
	<meta property="og:description"
				content="{{ $product->meta_description }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ url(Request::path()) }}">
	<meta property="og:image" content="{{ $product->ogImage }}">
	@if(!$product->active)
		<meta name="robots" content="noindex, nofollow">
	@endif
	@include('includes.open-graph')
@stop

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="{{ url('/') }}">Главная</a> / <span>{{ $product->title }}</span>
		</div>

		<div class="section__title m-tb">{{ $product->title }}</div>

		@include('includes.product-image-block', [
			'product' => $product,
			'show_price' => true,
			'show_body' => true,
			'fly_badge' => false,
			'custom_class' => false,
			'big_image' => false
		])

	</div>
@stop