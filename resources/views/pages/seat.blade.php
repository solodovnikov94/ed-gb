@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $product->meta_title }}</title>
	<meta name="description" content="{{ $product->meta_description }}">
	<meta name="keywords" content="{{ $product->meta_keywords }}">
	<meta property="og:title" content="{{ $product->meta_title }}">
	<meta property="og:description"
				content="{{ $product->meta_description }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ url(Request::path()) }}">
	<meta property="og:image" content="{{ $product->ogImage }}">
	@if(!$product->active)
		<meta name="robots" content="noindex, nofollow">
	@endif
	@include('includes.open-graph')
@stop

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="{{ url('/') }}">Главная</a> / <span>{{ $product->title }}</span>
		</div>

		<div class="section__title m-tb">{{ $product->title }}</div>

		@include('includes.product-image-block', [
			'product' => $product,
			'show_price' => true,
			'show_body' => true,
			'fly_badge' => false,
			'custom_class' => false,
			'big_image' => false
		])

	</div>

	@if($product->video)
	<div class="section__video">
		<div class="container">
			<div class="section__video-content">
				<iframe src="{{ $product->video }}"
								frameborder="0"
								allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen></iframe>
			</div>
		</div>
	</div>
	@endif

	<div class="container">
		@if(count($product->productFunctions))
		<div class="section__title m-tb">Функции автокресла</div>

		<div class="seat-functions m-b p-b">

			<div id="seat-functions-slider">
				@foreach($product->productFunctions as $item)
					<div>
						<div class="seat-function">
							<div class="seat-function__image">
								<img src="/storage/{{ $item->image }}" alt="{{ $item->title }}">
							</div>
							<div class="seat-function__title">{{ $item->title }}</div>
							<div class="seat-function__description">{{ $item->body }}</div>
						</div>
					</div>
				@endforeach
			</div>

			@if(count($product->productFunctions) > 2)
			<div class="seat-functions__controls">
				<a class="seat-functions__controls-left" id="seat-functions--prev">
					<img src="{{ asset('images/icons/arrow_l_2.png') }}" alt="Предыдущий слайд">
				</a>
				<a class="seat-functions__controls-right" id="seat-functions--next">
					<img src="{{ asset('images/icons/arrow_r_2.png') }}" alt="Следующий слайд">
				</a>
			</div>
			@endif

			<div class="product-section__buttons center">
				<a href="{{ $product->addToCart }}">Купить</a>
			</div>
		</div>
		@else
			<br>
		@endif
	</div>
@stop