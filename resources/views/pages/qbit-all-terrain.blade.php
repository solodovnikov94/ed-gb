@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $product->meta_title }}</title>
	<meta name="description" content="{{ $product->meta_description }}">
	<meta name="keywords" content="{{ $product->meta_keywords }}">

	<meta property="og:title" content="{{ $product->meta_title }}">
	<meta property="og:description"
				content="{{ $product->meta_description }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ url(Request::path()) }}">
	<meta property="og:image" content="{{ $product->ogImage }}">
	@if(!$product->active)
		<meta name="robots" content="noindex, nofollow">
	@endif
	@include('includes.open-graph')
@stop

@section('content')
	<div class="qbit-section m-b">
		<div class="qbit-section__image" style="background-image: url('{{ asset('images/new/GB_18_EU_GL_Qbit+_CHRE_02_DERV_HQ.jpg') }}')"></div>
		<div class="qbit-section__content">
			<div class="qbit-section__top">
				<div class="container">
					<div class="breadcrumbs">
						<a href="{{ url('/')  }}">Главная</a> / <span>{{ $product->title }}</span>
					</div>

					<div class="section__title m-tb">{{ $product->title }}</div>
				</div>
			</div>
			<div class="qbit-section__bottom">
				<div class="container">
					<div class="pram-item__price m-t">{{ $product->price }} грн</div>
					<div class="product-section__buttons">
						{{--<a href="#">Все модели Qbit</a>--}}
						<a href="{{ $product->addToCart }}">Купить</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		@include('includes.product-image-block', [
			'product' => $product,
			'show_price' => true,
			'show_body' => false,
			'fly_badge' => false,
			'custom_class' => false,
			'big_image' => false
		])
	</div>

	<div class="section m-t">
		<div class="container">
			<div class="videos-container p-t">
				<div class="videos-container__videos">
					<div class="subsection-title">&nbsp;</div>
					@if($product->video)
						<div class="videos-container__videos-item">
							<iframe src="{{ $product->video }}?controls=1"
											frameborder="0"
											allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
											allowfullscreen></iframe>
						</div>
					@endif
					@if(count($product->productVideos))
						@foreach($product->productVideos as $video)
							<div class="videos-container__videos-item">
								<iframe src="{{ $video->video }}?controls=1"
												frameborder="0"
												allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
												allowfullscreen></iframe>
							</div>
						@endforeach
					@endif
				</div>
				<div class="videos-container__content">
					<div class="subsection-title">GB QBIT+</div>

					<div class="pockit qbit-all-terrain">
						<div class="pockit__image">
							<img src="{{ asset('images/new/GB_19_y045_EU_LABL_Qbit+_All-Terrain_screen_HD.jpg') }}" alt="GB QBIT+">
							<div class="pockit__text" id="qbit-all-terrain-text-1">Кнопка для<br>складывания<br>коляски</div>
							<div class="pockit__text" id="qbit-all-terrain-text-2">2 смотровых<br>окошка</div>
							<div class="pockit__text" id="qbit-all-terrain-text-3">Комфортная<br>ткань</div>
							<div class="pockit__text" id="qbit-all-terrain-text-4">Съемный<br>бампер</div>
							<div class="pockit__text" id="qbit-all-terrain-text-5">5-точечные ремни<br>безопасности</div>
							<div class="pockit__text" id="qbit-all-terrain-text-6">Автоматический<br>замок в<br>сложенном виде</div>
							<div class="pockit__text" id="qbit-all-terrain-text-7">Двойные колеса<br>с амортизацией</div>
							<div class="pockit__text" id="qbit-all-terrain-text-8">Поворотные<br>передние<br>колеса</div>
							<div class="pockit__text" id="qbit-all-terrain-text-9">Вместительная<br>корзина<br>(до 5 кг)</div>
							<div class="pockit__text" id="qbit-all-terrain-text-10">Регулируемая<br>подножка</div>
							<div class="pockit__text" id="qbit-all-terrain-text-11">Большой капюшон<br>с защитой UF 50+</div>
							<div class="pockit__text" id="qbit-all-terrain-text-12">Вес: 7,6 кг</div>
						</div>
						<div class="pockit__bottom-text">Механизм сложения Qbit+ All-Terrain<br>позволяет за считанные секунды сложить<br>коляску до компактного размера (43х33х53см)</div>
						<div class="product-section__buttons">
							<a href="{{ $product->addToCart }}" class="active">Купить</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	@include('includes.product-functions', ['product' => $product])

	@include('includes.product-accessories', ['product' => $product])
	<br>
	@include('includes.accompanying-goods', ['product' => $product])
@stop