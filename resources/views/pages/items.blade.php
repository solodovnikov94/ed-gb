@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $category->meta_title }}</title>
	<meta name="description" content="{{ $category->meta_description }}">
	<meta name="keywords" content="{{ $category->meta_keywords }}">
@stop

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="{{ url('/') }}">Главная</a> / <span>{{ $category->title }}</span>
		</div>

		@if(count($category->children))
		<div class="tab-links">
			<a href="{{ $category->url }}" @if($subcategory_slug == '') class="active" @endif>Все </a>
			@foreach($category->children as $subcategory)
				<a href="{{ $subcategory->url }}" @if($subcategory->slug == $subcategory_slug) class="active" @endif>{{ $subcategory->title }}</a>
			@endforeach
		</div>
		@endif

		@if(count($productList))
			@foreach($productList as $product)
				<div class="pram-item">
					<div class="pram-item__title">
						<a href="{{ $product->url }}">{{ $product->title }}</a>
					</div>
					<div class="pram-item__image">
						<img src="/storage/{{ $product->image }}" alt="Очень длинное название классной модели колясок">
						@if($product->badge)
							<div class="pram-item__badge {{ $product->badge }}">{{ $product->text_badge }}</div>
						@endif
					</div>
					<div class="pram-item__price">{{ $product->price }} грн</div>
					<div class="product-section__buttons">
						<a href="{{ $product->addToCart }}">Купить</a>
						<a href="{{ $product->url }}">Узнать больше</a>
						@if($product->getAccessoryButton())
							<a href="{{ $product->url }}#accessories">Аксессуары</a>
						@endif
					</div>
				</div>
			@endforeach
		@endif

		{!! $productList->links() !!}
		<br>
	</div>
@stop