@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $page->meta_title }}</title>
	<meta name="description" content="{{ $page->meta_description }}">
	<meta name="keywords" content="{{ $page->meta_keywords }}">
@stop

@section('content')
	<div class="container">
		<div class="breadcrumbs">
			<a href="{{ url('/') }}">Главная</a> / <span>{{ $page->title }}</span>
		</div>

		<div class="thanks">
			<div class="thanks__title">{{ $page->title }}</div>
			<div class="thanks__subtitle">{!! $page->body !!}</div>
		</div>

	</div>
@stop