@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $page->meta_title }}</title>
	<meta name="description" content="{{ $page->meta_description }}">
	<meta name="keywords" content="{{ $page->meta_keywords }}">

	<meta property="og:title" content="{{ $page->meta_title }}">
	<meta property="og:description"
				content="{{ $page->meta_description }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ url(Request::path()) }}">
@stop

@section('preloader')
	<script>
		$(window).on('load', function () {
			let loader = $('#preloader');
			let content = $('#preloader-content');

			loader.addClass('loaded');
			content.addClass('displayed');

			setInterval(function () {
				content.addClass('loaded');
				// $('#body').addClass('loaded');
			}, 1);
		});
	</script>

	<div class="preloader" id="preloader">
		<div class="preloader__content">
			<div class="preloader__logo">
				<img src="{{ asset('images/gb_logo_big.png') }}" alt="GB logotype">
			</div>
			<div class="preloader__title">Эксклюзивный<br> представитель<br> GB в Украине</div>
			<div class="preloader__slogan">Future Perfect</div>
		</div>
	</div>
@stop

@section('content')
	@php $stroller = $products->where('slug', 'maris-day-dream')->first() @endphp
	@if($stroller)
	<div class="product-section pt" style="background-image: url('{{ asset('images/bg1.jpg') }}')">
		<div class="product-section__title">{{ $stroller->title }}</div>
		<div class="product-section__buttons">
			<a href="{{ $stroller->addToCart }}">Купить</a>
			<a href="{{ $stroller->url }}">Узнать больше</a>
			@if($stroller->getAccessoryButton())
				<a href="{{ $stroller->url }}#accessories-anchor">Аксессуары</a>
			@endif
		</div>
		<div class="product-section__image">
			<img src="/storage/{{ $stroller->image }}" alt="{{ $stroller->title }}">
		</div>
	</div>
	@endif

	@php $stroller = $products->where('slug', 'qbit-all-city')->first() @endphp
	@if($stroller)
	<div class="product-section" style="background-image: url('{{ asset('images/bg2.jpg') }}')">
		<div class="product-section__title">{{ $stroller->title }}</div>
		<div class="product-section__buttons">
			<a href="{{ $stroller->addToCart }}">Купить</a>
			<a href="{{ $stroller->url }}">Узнать больше</a>
			@if($stroller->getAccessoryButton())
				<a href="{{ $stroller->url }}#accessories-anchor">Аксессуары</a>
			@endif
		</div>
		<div class="product-section__image">
			<img src="/storage/{{ $stroller->productImages->where('color', 'green')->first()->image }}" alt="{{ $stroller->title }}">
		</div>
	</div>
	@endif

	@php $seat = $seats->where('slug', 'avtokreslo-idan')->first() @endphp
	@if($seat)
	<div class="product-section" style="background-image: url('{{ asset('images/bg3.jpg') }}')">
		<div class="product-section__title">{{ $seat->title }}</div>
		<div class="product-section__buttons">
			<a href="{{ $seat->addToCart }}">Купить</a>
			<a href="{{ $seat->url }}">Узнать больше</a>
		</div>
		<div class="product-section__image">
			<img src="/storage/{{ $seat->image }}" alt="{{ $seat->title }}">
		</div>
	</div>
	@endif

	@php $seat = $seats->where('slug', 'vaya-i-size')->first() @endphp
	@if($seat)
	<div class="product-section" style="background-image: url('{{ asset('images/bg4.jpg') }}')">
		<div class="product-section__title">{{ $seat->title }}</div>
		<div class="product-section__buttons">
			<a href="{{ $seat->addToCart }}">Купить</a>
			<a href="{{ $seat->url }}">Узнать больше</a>
		</div>
		<div class="product-section__image">
			<img src="/storage/{{ $seat->image }}" alt="{{ $seat->title }}">
		</div>
	</div>
	@endif
@stop