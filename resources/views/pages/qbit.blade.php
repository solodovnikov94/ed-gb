@extends('layouts.layouts-main')

@section('meta')
	<title>{{ $product->meta_title }}</title>
	<meta name="description" content="{{ $product->meta_description }}">
	<meta name="keywords" content="{{ $product->meta_keywords }}">

	<meta property="og:title" content="{{ $product->meta_title }}">
	<meta property="og:description"
				content="{{ $product->meta_description }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ url(Request::path()) }}">
	<meta property="og:image" content="{{ $product->ogImage }}">
	@if(!$product->active)
		<meta name="robots" content="noindex, nofollow">
	@endif
	@include('includes.open-graph')
@stop

@section('content')
	<div class="qbit-section m-b">
		<div class="qbit-section__image"></div>
		<div class="qbit-section__content">
			<div class="qbit-section__top">
				<div class="container">
					<div class="breadcrumbs">
						<a href="{{ url('/')  }}">Главная</a> / <span>{{ $product->title }}</span>
					</div>

					<div class="section__title m-tb">{{ $product->title }}</div>
				</div>
			</div>
			<div class="qbit-section__bottom">
				<div class="container">
					<div class="pram-item__price m-t">{{ $product->price }} грн</div>
					<div class="product-section__buttons">
						{{--<a href="#">Все модели Qbit</a>--}}
						<a href="{{ $product->addToCart }}">Купить</a>
						<a href="#accessories-anchor">Аксессуары к Qbit</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		@include('includes.product-image-block', [
			'product' => $product,
			'show_price' => false,
			'show_body' => false,
			'fly_badge' => false,
			'custom_class' => false,
			'big_image' => false
		])
	</div>

	<div class="section m-t">
		<div class="container">
			<div class="videos-container p-t">
				<div class="videos-container__videos">
					<div class="subsection-title">&nbsp;</div>
					@if($product->video)
						<div class="videos-container__videos-item">
							<iframe src="{{ $product->video }}?controls=1"
											frameborder="0"
											allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
											allowfullscreen></iframe>
						</div>
					@endif
					@if(count($product->productVideos))
						@foreach($product->productVideos as $video)
							<div class="videos-container__videos-item">
								<iframe src="{{ $video->video }}?controls=1"
												frameborder="0"
												allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
												allowfullscreen></iframe>
							</div>
						@endforeach
					@endif
				</div>
				<div class="videos-container__content">
					<div class="subsection-title">GB QBIT+</div>

					<div class="pockit qbit">
						<div class="pockit__image">
							<img src="{{ asset('images/qbit/GB_18_y045_EU_LABL_QbitPlus_bumperbar_DERV_HQ.png') }}" alt="GB QBIT+">
							<div class="pockit__text" id="qb-text-1">2 смотровых<br>окошка</div>
							<div class="pockit__text" id="qb-text-2">Ткань комфорт</div>
							<div class="pockit__text" id="qb-text-3">Съемный бампер</div>
							<div class="pockit__text" id="qb-text-4">5-точечные ремни<br>безопасности</div>
							<div class="pockit__text" id="qb-text-5">Горизонтальное<br>положение спинки</div>
							<div class="pockit__text" id="qb-text-6">Тормоз</div>
							<div class="pockit__text" id="qb-text-7">Вместительная<br>корзина<br>(max нагрузка 5 кг)</div>
							<div class="pockit__text" id="qb-text-8">Ширина шасси 49 см<span class="pockit__text-arrows"></span></div>
							<div class="pockit__text" id="qb-text-9">Закрытая<br>амортизация<br>всех колес</div>
							<div class="pockit__text" id="qb-text-10">Поворотные<br>передние колеса<br>(диаметр 16 см)</div>
							<div class="pockit__text" id="qb-text-11">Регулируемая<br>подножка</div>
							<div class="pockit__text" id="qb-text-12">Мягкие подушечки<br>на ремнях<br>безопасности</div>
							<div class="pockit__text" id="qb-text-13">Большой капюшон<br>с UF 50+</div>
							<div class="pockit__text" id="qb-text-14">Вес: 7,6 кг</div>
						</div>
						<div class="pockit__bottom-text">Прогулочная коляска от рождения до 17 кг<br>Максимальный комфорт и удобство для мамы и ребёнка<br>Стильная и легкая коляска на каждый день.</div>
						<div class="product-section__buttons">
							<a href="{{ $product->addToCart }}" class="active">Купить</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="section gray-bg p-tb">
		<div class="container">
			<div class="advantages">
				<div class="advantage">
					<div class="advantage__image">
						<img src="{{ asset('images/qbit/1.png') }}" alt="Компактность в сложенном виде">
					</div>
					<div class="advantage__title">Компактность и устойчивость в сложенном виде</div>
				</div>
				<div class="advantage">
					<div class="advantage__image">
						<img src="{{ asset('images/qbit/2.png') }}" alt="Возможность использования с автокреслом">
					</div>
					<div class="advantage__title">Возможность использования с автокреслом Гр 0+</div>
					<a href="#recommended-anchor" class="advantage__arrow">
						<img src="{{ asset('images/kolaska_model/arrow_down.png') }}" alt="">
					</a>
				</div>
				<div class="advantage">
					<div class="advantage__image">
						<img src="{{ asset('images/qbit/3.png') }}" alt="Возможность использования с люлькой GB COT">
					</div>
					<div class="advantage__title">Возможность использования с люлькой GB COT TO GO</div>
					<a href="#accessories-anchor" class="advantage__arrow">
						<img src="{{ asset('images/kolaska_model/arrow_down.png') }}" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>

	@include('includes.product-accessories', ['product' => $product])
	<br>
	@include('includes.accompanying-goods', ['product' => $product])
@stop