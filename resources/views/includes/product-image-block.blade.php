<div class="cc @if($custom_class) {{ $custom_class }} @endif">
	<div class="cc__content">
		@isset($product->title)
      <div class="cc__title">{{ $product->title }}
				<br>
				<span id="color-text" data-id="{{ $product->id }}">
					@if(count($product->productImages))
						{{ $product->productImages->first()->title }}
					@endif
				</span>
			</div>
		@endisset

		<div class="cc__images @if($big_image) bigger @endif">
			@if($product->badge)
				<div class="pram-item__badge {{ $product->badge }}">{{ $product->text_badge }}</div>
			@endif
			@if($fly_badge)
				<div class="cc__plane">
					<img src="{{ asset('images/kolaska_model/avia.png') }}" alt="Допускается на борт самолета">
				</div>
			@endif
			@if(count($product->productImages))
				@foreach($product->productImages->where('active', 1) as $colors)
					<div class="cc__image cc--idan" id="cc--idan-{{ $loop->iteration }}" @if ($loop->first) style="display: block;" @endif>
						{{--<span class="cc__title">--}}
							{{--@isset($product->title) {{ $product->title }} @endisset {{ $colors->title }}--}}
						{{--</span>--}}
						<img src="/storage/{{ $colors->image }}" alt="{{ $product->title }} {{ $colors->color }}">
					</div>
				@endforeach
			@else
				<div class="cc__image cc--idan" id="cc--idan-1" style="display: block;">
					<img src="/storage/{{ $product->image }}" alt="{{ $product->title }}">
				</div>
			@endif
		</div>
		@if($show_body)
			<div class="cc__body m-t">
				{!! $product->body !!}
			</div>
		@endif
		@if($show_price)
			<div class="pram-item__price">{{ $product->price }} грн</div>
		@endif
	</div>
	@if(count($product->productImages))
		<div class="cc__colors">
			<div class="cc__colors-title">Цвета +</div>
			<div class="cc__colors-list">
				@foreach($product->productImages->where('active', 1) as $colors)
					<a class="cc__colors-item {{ $colors->color }}" data-color="{{ $colors->color }}" data-key="idan" data-color-id="{{ $loop->iteration }}"></a>
				@endforeach
			</div>
		</div>
	@endif
	<div class="cc__content">
		<div class="product-section__buttons m-b">
			<a href="{{ $product->addToCart }}">Купить</a>
		</div>
	</div>
</div>