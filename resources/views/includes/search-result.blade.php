@if(isset($products) && count($products))
	@foreach($products as $product)
		<a href="{{ $product->url }}">{{ $product->title }}</a>
	@endforeach
@else
	Нет результатов по даному поиску
@endif