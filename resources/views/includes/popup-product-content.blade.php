@php $basketProducts = basket_products() @endphp
@if(count($basketProducts))
	<div class="popup-cart">
		@foreach($basketProducts as $item)
			<div class="popup-cart__item">
				<div class="popup-cart__item-title">{{ $item->title }}</div>
				<div class="popup-cart__item-content">
					<div class="popup-cart__item-image">
						@if(isset($item->productImages) && count($item->productImages) && product_color($item->id))
							<img src="/storage/{{ $item->productImages->where('color', product_color($item->id))->first()->image }}" alt="{{ $item->title }}">
						@else
							<img src="/storage/{{ $item->image }}" alt="{{ $item->title }}">
						@endif
					</div>
					<div class="popup-cart__item-controls">
						<div class="popup-cart__item-controls--top">
							<div class="popup-cart__item-counter">
								<button class="crement" data-product-id="{{ $item->id }}" data-value="1">+</button>
								<span>{{ session_basket()[$item->id] }}</span>
								<button class="crement" data-product-id="{{ $item->id }}" data-value="-1">-</button>
							</div>
							<div class="popup-cart__item-price">{{ session_basket()[$item->id] * $item->price }} грн</div>
						</div>
						@if(isset($item->productImages) && count($item->productImages))
							<div class="popup-cart__item-controls--bottom">
								<span>Цвет:</span>
								<select name="color" class="color-select" data-product-id="{{ $item->id }}">
									@foreach($item->productImages as $itemImage)
										<option value="{{ $itemImage->color }}"
														@if(color_products($item->id, $itemImage->color)) selected @endif>{{ $itemImage->title }}</option>
									@endforeach
								</select>
							</div>
						@endif
					</div>
				</div>
			</div>
		@endforeach
	</div>

	<div class="product-section__buttons tac block">
		<a href="{{ url('order') }}?pupup=0" class="active width-auto">Оформить заказ</a>
	</div>
@endif