@if(count($product->accompanyingGoods))
    <div class="section gray-bg p-tb">
        <div class="section__subtitle p-t" id="recommended-anchor">Сопутствующий товар</div>
        <div class="recommended-items">
            <div id="recommended-items-slider">
                @foreach($product->accompanyingGoods as $item)
                    <div>
                        <div class="pram-item">
                            <div class="pram-item__title">
                                <a href="{{ $item->url }}">{{ $item->title }}</a>
                            </div>
                            <div class="pram-item__image m-t p-lr">
                                <img src="/storage/{{ $item->image }}" alt="{{ $item->title }}">
                                @if($item->badge)
                                    <div class="pram-item__badge {{ $item->badge }}">{{ $item->text_badge }}</div>
                                @endif
                            </div>
                            <div class="pram-item__price m-t">{{ $item->price }} грн</div>
                            <div class="product-section__buttons">
                                <a href="{{ $item->addToCart }}">Купить</a>
                                <a href="{{ $item->url }}">Узнать больше</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif