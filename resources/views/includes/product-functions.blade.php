@if(count($product->productFunctions))
	<div class="section gray-bg p-tb">
		<div class="container">
			<div class="advantages">
				@foreach($product->productFunctions as $item)
					<div class="advantage">
						<div class="advantage__image">
							<img src="/storage/{{ $item->image }}" alt="{{ $item->title }}">
						</div>
						<div class="advantage__title">{{ $item->title }}</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endif