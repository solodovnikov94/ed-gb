<div class="popup">
	<div class="popup__close">
		<img src="{{ asset('images/Zakaz_pop_up/close_pop_up.png') }}" alt="Закрыть">
	</div>
	<div class="popup__title">Ваш заказ</div>
	<div class="popup__content">
		<div id="popup__content">
			@include('includes.popup-product-content')
		</div>
		@php $basketProducts = basket_products() @endphp
		@if(count($basketProducts))

		@foreach($basketProducts as $item)
			@if(count($item->accompanyingGoods))
				<div class="popup-cart__item-title m-tb p-t">Сопутствующие товары</div>
				@break
			@endif
		@endforeach

		<div class="popup-related">
			<div id="popup-related">
				@foreach($basketProducts as $item)
					@if(count($item->accompanyingGoods))
						@foreach($item->accompanyingGoods as $good)
							<div>
								<div class="popup-related__item">
									<div class="popup-related__item-title">{{ $good->title }}</div>
									<div class="popup-related__item-content">
										<div class="popup-related__item-image">
											<img src="/storage/{{ $good->image }}" alt="{{ $good->title }}">
										</div>
										<div class="popup-related__item-price m-t">{{ $good->price }} грн</div>
										<div class="product-section__buttons center">
											<a href="" class="active add-to-basket" data-product-id="{{ $good->id }}">Добавить</a>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@endif
				@endforeach
			</div>
		</div>
		@endif

		<div class="product-section__buttons tac block">
			<a href="{{ url('/') }}" class="width-auto">Продолжить покупки</a>
		</div>
	</div>
</div>