@if(count($product->accessoriesRelationList))
    <div class="container">
        <div class="accessories">
            <div class="section__subtitle m-tb" id="accessories-anchor">Аксессуары</div>
            <div id="accessories-slider">
                @foreach($product->accessoriesRelationList as $item)
                    <div>
                        <div class="accessory">
                            <div class="accessory__title">{{ $item->title }}</div>
                            <div class="accessory__image">
                                <img src="/storage/{{ $item->image }}" alt="{{ $item->price }}">
                                @if($item->badge)
                                    <div class="pram-item__badge {{ $item->badge }}">{{ $item->text_badge }}</div>
                                @endif
                            </div>
                            <div class="pram-item__price m-tb">{{ $item->price }} грн</div>
                            {!! $item->body !!}
                            <div class="product-section__buttons">
                                <a href="{{ $item->addToCart }}">Купить</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif