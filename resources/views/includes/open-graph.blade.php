<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Product",
		"description": "{{ $product->meta_description }}",
		"name": "{{ $product->title }}",
		"image": "{{ asset('/storage/'.$product->image) }}",
		"offers": {
			"@type": "Offer",
			"availability": "http://schema.org/InStock",
			"price": "{{ $product->price }}",
			"priceCurrency": "UAH"
		}
	}
</script>