<!doctype html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<meta name="viewport"
				content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	@yield('meta')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="google-site-verification" content="onJxIT6p5UnCk_ajTJ1KNZ2zQIN2by0Mw1ugRl-Fuyw"/>

	{{--<style>--}}
		{{--body {--}}
			{{--overflow: hidden;--}}
		{{--}--}}
	{{--</style>--}}
	<link rel="icon" type="image/png" href="{{ asset('images/gb_logo_menu.png') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}?{{ time() }}">
	<link rel="stylesheet" href="{{ asset('css/common.css') }}?{{ time() }}">

	<script src="{{ asset('libs/jquery/jquery-3.3.1.min.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script src="{{ mix('js/common.js') }}?{{ time() }}"></script>
	@yield('scripts')
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-79473800-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-79473800-1');
	</script>
</head>

<body id="body">
<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
	<meta itemprop="addressCountry" content="Украина">
	<meta itemprop="addressLocality" content="Киев">
	<meta itemprop="streetAddress" content="улица Эспланадная 20">
</div>
<meta itemprop="name" content="Эксклюзивный представитель GB в Украине">
<meta itemprop="openingHours" content="Mo-Su 00:00-24:00">
<meta itemprop="priceRange" content="400 - 15000 UAH">
<meta itemprop="image" content="https://gbaby.com.ua/images/preloader.jpg">
<meta itemprop="telephone" content="38 (067) 555-94-83">
<meta itemprop="url" content="https://gbaby.com.ua">

@yield('preloader')

<div class="wrapper {{ Request::path() == '/' ? '' : 'loaded' }}" id="preloader-content">
	<div class="wrapper__content">
		<div class="header__wrapper">
			<div class="mm">
				<a class="mm__close">
					<img src="{{ asset('images/icons/times.png') }}" alt="Закрыть меню">
				</a>
				<div class="mm__menu">
					<a href="{{ url('/') }}" class="mm__menu-section">Главная</a>
					<a href="{{ url('about') }}" class="mm__menu-section">О бренде</a>
					@foreach($categories as $category)
						<a href="{{ $category->url }}" class="mm__menu-section">{{ $category->title }}</a>
						@foreach($category->children->sortBy('order') as $subcategory)
							<div class="mm__menu-group">
								@if($category->slug == 'seats')
									<a class="mm__menu-group--title" href="{{ $subcategory->url }}">{{ $subcategory->title }}</a>
								@else
									<a class="mm__menu-group--title">{{ $subcategory->title }}</a>
									@if(count($subcategory->products))
										<div class="mm__menu-group--wrapper">
											<div class="mm__menu-group--list">
												@foreach($subcategory->products->where('active', 1)->sortBy('order_') as $product)
													<a href="{{ $product->url }}">{{ $product->title }}</a>
												@endforeach
											</div>
										</div>
									@endif
								@endif
							</div>
						@endforeach
					@endforeach
					<a href="{{ url('where-can-buy') }}" class="mm__menu-section">Где купить</a>
				</div>
			</div>
			<header class="header">
				<div class="header__top">
					<div class="container">
						<div class="header__top-content">
							<a href="{{ url('/') }}" class="header__logo">
								<img src="{{ asset('images/gb_logo_menu_with_text.png') }}" alt="GB logotype">
							</a>
							<div class="header__menu">
								@foreach($products as $stroller)
									<a href="{{ $stroller->url }}">{{ $stroller->title }}</a>
								@endforeach
								<a href="{{ url('strollers/3-qbit') }}">Qbit+</a>
								<a href="{{ url('seats') }}">Автокресла</a>
								<a href="{{ url('accessories') }}">Аксессуары</a>
								<a href="{{ url('where-can-buy') }}">Где купить</a>
							</div>
							<div class="header__buttons">
								<a href="#" class="header__cart open-cart">
									<img src="{{ asset('images/icons/cart.png') }}" alt="Ваша корзина">
									<span id="count-product-cart">{{ basket_counter() }}</span>
								</a>
								<a class="mm__open">
									<img src="{{ asset('images/icons/menu_icon.png') }}" alt="Открыть меню">
									<div>menu</div>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="header__bottom">
					<div class="container">
						<div class="header__bottom-content">
							<form class="header__search">
								<input type="text" placeholder="Найти..." id="search-input" name="search">
								<button type="submit"><svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="search" class="svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="#000000" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z"></path></svg>
								</button>
								<div class="header__search-results" id="window-search" style="display: none">
								</div>
							</form>
							<div class="header__socials">
								<a href="https://www.instagram.com/gb.ukraine/" target="_blank" rel="nofollow">
									<img src="{{ asset('images/icons/instagram_icon.png') }}" alt="GB в Instagram">
								</a>
								<a href="https://www.facebook.com/GBaby.com.ua" target="_blank" rel="nofollow">
									<img src="{{ asset('images/icons/facebook_icon.png') }}" alt="GB в Facebook">
								</a>
								<a href="https://www.youtube.com/channel/UCuxLkK9eitJDDK93Rejbpzg" target="_blank" rel="nofollow">
									<img src="{{ asset('images/icons/youtube_icon.png') }}" alt="GB на Youtube">
								</a>
							</div>
						</div>
					</div>
				</div>
			</header>
		</div>

		<main>
			@yield('content')
		</main>
	</div>
	<div class="wrapper__footer">
		<div class="container">
			<footer class="footer">
				<div class="footer__left">
					<a href="/" class="footer__logo">
						<img src="{{ asset('images/gb_logo_menu.png') }}" alt="GB">
					</a>
					<div class="footer__socials">
						<a href="https://www.instagram.com/gbaby_ukraine" target="_blank" rel="nofollow">
							<img src="{{ asset('images/icons/instagram_w.png') }}" alt="GB в Instagram">
						</a>
						<a href="https://www.facebook.com/GBaby.com.ua" target="_blank" rel="nofollow">
							<img src="{{ asset('images/icons/facebook_w.png') }}" alt="GB в Facebook">
						</a>
						<a href="https://www.youtube.com/channel/UCuxLkK9eitJDDK93Rejbpzg" target="_blank" rel="nofollow">
							<img src="{{ asset('images/icons/youtube_w.png') }}" alt="GB на Youtube">
						</a>
					</div>
				</div>
				<div class="footer__right">
					<div class="footer__contacts">
						<div class="footer__contacts-title">Увидеть новинки и получить консультацию:</div>
						<div class="footer__contacts-content">
							<div class="footer__contacts-item">067-555-94-83</div>
							<div class="footer__contacts-item">067-555-94-81</div>
							{{--<div class="footer__contacts-item">067-134-20-04</div>--}}
							<div class="footer__contacts-item">ул. Джона Маккейна, 7, Киев, 02000</div>
						</div>
					</div>
					<a href="https://www.google.com/maps/place/%D1%83%D0%BB.+%D0%94%D0%B6%D0%BE%D0%BD%D0%B0+%D0%9C%D0%B0%D0%BA%D0%BA%D0%B5%D0%B9%D0%BD%D0%B0,+7,+%D0%9A%D0%B8%D0%B5%D0%B2,+02000/@50.4146489,30.5311973,16z/data=!4m5!3m4!1s0x40d4cf3fadf44a6b:0xd91bf84f1564fd6a!8m2!3d50.4149353!4d30.530897?shorturl=1"
						 target="_blank"
						 rel="nofollow"
						 class="footer__map-marker">
						<img src="{{ asset('images/icons/map-marker_w.png') }}" alt="GB на карте">
					</a>
				</div>
			</footer>
		</div>
	</div>
</div>

<div class="popup-container" @if(Request::path() == 'order' && request()->pupup == null) style="display: block" @endif>
	<div class="popup-wrapper">
		@include('includes.basket-popup')
	</div>
</div>

<script>
	(function(w,d,u){
		var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
		var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
	})(window,document,'https://cdn.bitrix24.ua/b9790115/crm/site_button/loader_1_mn4t22.js');
</script>

</body>
</html>
