<?php

Route::group(['prefix' => 'admin'], function () {
	Admin::routes();
});

Route::get('/', 'PagesController@index');

Route::get('/strollers/{id}-{slug}', 'ProductsController@stroller')->where(['id' => '[0-9]+']);
Route::get('/strollers/{subcategory?}', 'ProductsController@strollers');

Route::get('/seats/{id}-{slug}', 'ProductsController@seat')->where(['id' => '[0-9]+']);
Route::get('/seats/{subcategory?}', 'ProductsController@seats');

Route::get('/search', 'ProductsController@search');

Route::get('/accessories', 'ProductsController@accessories');
Route::get('/accessories/{id}-{slug}', 'ProductsController@accessory')->where(['id' => '[0-9]+']);

Route::get('/add-to-cart/{id}/{color?}', 'OrdersController@addToCart')->where(['id' => '[0-9]+']);
Route::get('/where-can-buy', 'PagesController@whereCanBuy');
Route::get('/about', 'PagesController@about');
Route::get('/thanks', 'PagesController@thanks');
Route::get('/order', 'OrdersController@order');
Route::post('/save-order', 'OrdersController@saveOrder');
Route::post('/order/novaposhta', 'OrdersController@novaPoshta');

Route::post('/basket/crement', 'OrdersController@crement');
Route::post('/basket/add-product', 'OrdersController@addAjaxToCart');
Route::post('/basket/color-product', 'OrdersController@productColors');

Route::get('/color-product', 'PagesController@colorProduct');

Route::get('sitemap-create', function (){
	Artisan::call('sitemap:create');
	return 'success';
});

//TODO don't remove pls
//Route::get('/frontend/dd', function () {return view('pages-copy.dd');});
//Route::get('/frontend/accessory', function () {return view('pages-copy.accessory');});
//Route::get('/frontend/order', function () {return view('pages-copy.order');});
//Route::get('/frontend/prams', function () {return view('pages-copy.prams');});
//Route::get('/frontend/prams-model', function () {return view('pages-copy.prams-model');});
//Route::get('/frontend/seat', function () {return view('pages-copy.seat');});
//Route::get('/new/1', function () {
//	$product = \App\Product::where([
//		['id', '=', 2],
//	])->active()->firstOrFail();
//
//	return view('pages.pockit-air-all-terrain', compact('product'));
//});
//Route::get('/new/2', function () {
//	$product = \App\Product::where([
//		['id', '=', 2],
//	])->active()->firstOrFail();
//
//	return view('pages.pockit-all-terrain', compact('product'));
//});
//Route::get('/new/3', function () {
//	$product = \App\Product::where([
//		['id', '=', 2],
//	])->active()->firstOrFail();
//
//	return view('pages.qbit-all-terrain', compact('product'));
//});
//Route::get('/new/4', function () {
//	$product = \App\Product::where([
//		['id', '=', 2],
//	])->active()->firstOrFail();
//
//	return view('pages.pockit-all-city', compact('product'));
//});
//Route::get('/new/5', function () {
//	$product = \App\Product::where([
//		['id', '=', 2],
//	])->active()->firstOrFail();
//
//	return view('pages.qbit-all-city', compact('product'));
//});
//Route::get('/new/6', function () {
//	$product = \App\Product::where([
//		['id', '=', 2],
//	])->active()->firstOrFail();
//
//	return view('pages.qbit-all-city-fashion', compact('product'));
//});