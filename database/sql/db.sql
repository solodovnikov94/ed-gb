-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Май 08 2019 г., 15:52
-- Версия сервера: 10.1.38-MariaDB-1~jessie
-- Версия PHP: 7.1.27-1+0~20190307202251.14+jessie~1.gbp7163d5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `gbabyN`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `title`, `slug`, `created_at`, `updated_at`, `meta_title`, `meta_description`) VALUES
(1, NULL, 1, 'Коляски', 'strollers', '2019-02-06 14:47:43', '2019-02-06 14:47:43', 'Коляски', 'Коляски'),
(2, NULL, 2, 'Автокресла', 'seats', '2019-02-06 14:48:27', '2019-02-06 14:52:03', 'Автокресла', 'Автокресла'),
(3, NULL, 3, 'Аксессуары', 'accessories', '2019-02-06 14:49:03', '2019-02-06 14:52:14', 'accessories', 'accessories'),
(4, 1, 1, 'Gold', 'gold', '2019-02-06 14:55:44', '2019-02-06 14:55:44', 'Gold', 'Gold'),
(5, 1, 2, 'Platinum', 'platinum', '2019-02-06 14:56:03', '2019-02-06 14:56:15', 'Platinum', 'Platinum'),
(6, 2, 1, 'Группа 0+', 'gruppa-0', '2019-02-06 14:57:06', '2019-03-06 10:07:13', 'Гр 0+', 'Гр 0+'),
(7, 2, 2, 'Группа 0+/1', 'gruppa-0-1', '2019-02-06 14:57:41', '2019-03-06 10:07:25', 'группа 0+/1', 'группа 0+/1'),
(8, 2, 3, 'Группа 1/2/3', 'gruppa-1-2-3', '2019-02-06 14:59:45', '2019-02-27 10:02:56', 'группа 1/2/3', 'группа 1/2/3'),
(9, 2, 4, 'Группа 2/3', 'gruppa-2-3', '2019-02-06 15:00:11', '2019-02-27 10:05:53', 'группа 2/3', 'группа 2/3');

-- --------------------------------------------------------

--
-- Структура таблицы `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 1, '', 2),
(3, 1, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(4, 1, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 4),
(5, 1, 'category_id', 'select_dropdown', 'Category', 1, 0, 1, 1, 1, 0, '{\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 5),
(6, 1, 'title', 'text', '', 1, 1, 1, 1, 1, 1, '', 6),
(7, 1, 'excerpt', 'text_area', '', 1, 0, 1, 1, 1, 1, '', 7),
(8, 1, 'body', 'rich_text_box', '', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 8),
(9, 1, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"cropper\":[{\"name\":\"avatar\",\"size\":{\"name\":\"max\",\"width\":\"300\",\"height\":\"200\"},\"resize\":[{\"name\":\"norm\",\"width\":\"200\",\"height\":\"null\"},{\"name\":\"min\",\"width\":\"100\",\"height\":\"null\"}]}]}', 9),
(10, 1, 'meta_description', 'text_area', 'meta_description', 1, 0, 1, 1, 1, 1, '', 10),
(11, 1, 'meta_keywords', 'text_area', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 11),
(12, 1, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 12),
(13, 1, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 13),
(14, 2, 'id', 'number', 'ID', 1, 1, 0, 0, 0, 0, NULL, 1),
(16, 2, 'title', 'text', 'Заголовок', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"}}', 2),
(17, 2, 'excerpt', 'text_area', 'excerpt', 0, 0, 0, 0, 0, 0, NULL, 4),
(18, 2, 'body', 'rich_text_box', 'Контент', 0, 0, 1, 1, 1, 1, NULL, 5),
(19, 2, 'slug', 'text', 'Псевдоним', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"}}', 3),
(20, 2, 'meta_description', 'text_area', 'Описание (meta)', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"}}', 7),
(21, 2, 'meta_keywords', 'text', 'Ключевые слова (meta)', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"}}', 8),
(22, 2, 'status', 'checkbox', 'status', 0, 0, 0, 0, 0, 0, '{\"on\":\"Да\",\"off\":\"Нет\"}', 9),
(23, 2, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, NULL, 10),
(24, 2, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, NULL, 11),
(25, 2, 'image', 'image', 'image', 0, 0, 0, 0, 0, 0, NULL, 12),
(26, 3, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(27, 3, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
(28, 3, 'email', 'text', 'email', 1, 1, 1, 1, 1, 1, '', 3),
(29, 3, 'password', 'password', 'password', 0, 0, 0, 1, 1, 0, '', 4),
(30, 3, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"LaravelAdminPanel\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(31, 3, 'remember_token', 'text', 'remember_token', 0, 0, 0, 0, 0, 0, '', 5),
(32, 3, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 6),
(33, 3, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 7),
(34, 3, 'avatar', 'image', 'avatar', 0, 1, 1, 1, 1, 1, '', 8),
(35, 5, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(36, 5, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
(37, 5, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(38, 5, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(39, 4, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, NULL, 2),
(40, 4, 'parent_id', 'select_dropdown', 'Категория', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"title\"}}', 3),
(41, 4, 'order', 'text', 'Порядок', 1, 1, 0, 1, 1, 0, '{\"default\":1}', 1),
(43, 4, 'slug', 'text', 'Псевдоним', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"}}', 5),
(44, 4, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, NULL, 6),
(45, 4, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, NULL, 7),
(46, 6, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(47, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(48, 6, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(49, 6, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(50, 6, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(51, 1, 'seo_title', 'text', 'seo_title', 0, 1, 1, 1, 1, 1, '', 14),
(52, 1, 'featured', 'checkbox', 'featured', 1, 1, 1, 1, 1, 1, '', 15),
(53, 3, 'role_id', 'text', 'role_id', 1, 1, 1, 1, 1, 1, '', 9),
(54, 7, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(55, 7, 'data_type_id', 'select_dropdown', 'Data Type', 1, 1, 1, 1, 1, 1, '{\"relationship\":{\"key\":\"id\",\"label\":\"display_name_singular\"}}', 2),
(56, 7, 'options', 'code_editor', 'Options', 1, 0, 1, 1, 1, 1, '{\"formfields_custom\":\"json_editor\"}', 3),
(57, 2, 'meta_title', 'text', 'Seo название', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"}}', 6),
(58, 8, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, NULL, 1),
(59, 8, 'title', 'text', 'Заголовок', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"}}', 4),
(60, 8, 'price', 'number', 'Цена', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"}}', 6),
(61, 8, 'image', 'image', 'Изображение', 0, 1, 1, 1, 1, 0, '{}', 7),
(62, 8, 'meta_title', 'text', 'Seo название', 0, 0, 1, 1, 1, 0, '{\"validation\":{\"rule\":\"required|max:255\"}}', 9),
(63, 8, 'meta_description', 'text_area', 'Описание (meta)', 0, 0, 1, 1, 1, 0, '{\"validation\":{\"rule\":\"required|max:255\"}}', 10),
(64, 8, 'meta_keywords', 'text', 'Ключевые слова (meta)', 0, 0, 1, 1, 1, 0, NULL, 11),
(65, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 12),
(66, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 15),
(67, 8, 'slug', 'text', 'Псевдоним', 0, 0, 0, 1, 1, 0, '{\"slugify\":{\"origin\":\"title\"}}', 5),
(68, 8, 'active', 'checkbox', 'Активность', 0, 1, 1, 1, 1, 1, '{\"on\":\"Да\",\"off\":\"Нет\"}', 8),
(69, 8, 'product_id', 'checkbox', NULL, 0, 0, 0, 0, 0, 0, NULL, 13),
(71, 8, 'type', 'select_dropdown', 'Тип товара', 0, 0, 0, 0, 0, 0, '{\"default\":\"accessories\",\"options\":{\"accessories\":\"Аксессуар\",\"seats\":\"Авто кресло\",\"strollers\":\"Коляска\"}}', 16),
(72, 8, 'body', 'rich_text_box', 'Описание', 0, 0, 0, 1, 1, 0, NULL, 14),
(73, 9, 'id', 'number', 'Id', 1, 1, 0, 0, 0, 0, NULL, 1),
(74, 9, 'image', 'image', 'Изображение', 0, 1, 1, 1, 1, 0, NULL, 2),
(75, 9, 'title', 'text', 'Заголовок', 0, 1, 1, 1, 1, 0, NULL, 4),
(76, 9, 'body', 'text_area', 'Описание', 0, 0, 1, 1, 1, 1, NULL, 5),
(77, 9, 'product_id', 'select_dropdown', 'Товар', 0, 0, 1, 1, 1, 0, '{\"relationship\":{\"key\":\"id\",\"label\":\"title\"}}', 3),
(78, 8, 'product_belongsto_product_function_relationship', 'relationship', 'Функции Товара', 0, 0, 1, 1, 0, 0, '{\"model\":\"App\\\\ProductFunction\",\"table\":\"product_functions\",\"type\":\"hasMany\",\"column\":\"product_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"details\":\"{\\\"list\\\":\\\"datatable\\\"}\"}', 22),
(79, 10, 'id', 'number', 'Id', 1, 1, 0, 0, 0, 0, NULL, 1),
(80, 10, 'color', 'select_dropdown', 'Цвет', 0, 1, 1, 1, 1, 1, '{\"default\":\"green\",\"options\":{\"green\":\"Зеленый\",\"blue\":\"Голубой\",\"gray\":\"Серый\",\"red\":\"Красный\",\"black\":\"Черный\",\"brown\":\"Коричневый\",\"khaki\":\"Хаки\",\"pink\":\"Розовый\"}}', 2),
(81, 10, 'image', 'image', 'Изображение', 0, 1, 1, 1, 1, 1, NULL, 3),
(82, 8, 'product_hasmany_product_image_relationship', 'relationship', 'Изображения товаров', 0, 0, 1, 1, 0, 0, '{\"model\":\"App\\\\ProductImage\",\"table\":\"product_images\",\"type\":\"hasMany\",\"column\":\"product_id\",\"key\":\"color\",\"label\":\"id\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"details\":\"{\\\"list\\\":\\\"datatable\\\"}\"}', 21),
(83, 10, 'product_id', 'select_dropdown', 'Товар', 0, 0, 0, 1, 1, 0, '{\"relationship\":{\"key\":\"id\",\"label\":\"title\"}}', 4),
(84, 8, 'accessories', 'select_multiple', 'Аксессуары', 0, 0, 0, 1, 1, 0, '{\"relationship\":{\"key\":\"id\",\"label\":\"title\"}}', 17),
(85, 8, 'accompanying_goods', 'select_multiple', 'Сопутсвующий товар', 0, 0, 0, 1, 1, 0, '{\"relationship\":{\"key\":\"id\",\"label\":\"title\"}}', 18),
(86, 8, 'badge', 'select_dropdown', 'Бейджик', 0, 0, 0, 1, 1, 0, '{\"default\":\"\",\"options\":{\"\":\"------------\",\"red\":\"Красный\",\"green\":\"Зеленый\",\"gray\":\"Серый\"}}', 19),
(87, 8, 'text_badge', 'text', 'Текст бейджика', 0, 0, 0, 1, 1, 0, NULL, 20),
(88, 4, 'title', 'text', 'Заголовок', 1, 1, 1, 1, 1, 1, NULL, 4),
(89, 4, 'meta_title', 'text', 'Seo название', 0, 0, 1, 1, 1, 0, NULL, 8),
(90, 4, 'meta_description', 'text_area', 'Описание (meta)', 0, 0, 1, 1, 1, 0, NULL, 9),
(91, 8, 'category_id', 'select_dropdown', 'Категория', 0, 0, 0, 1, 1, 0, '{\"formfields_custom\":\"subcategories\",\"relationship\":{\"key\":\"id\",\"label\":\"title\"}}', 2),
(92, 8, 'product_belongsto_category_relationship', 'relationship', 'Категория', 0, 1, 1, 0, 0, 1, '{\"model\":\"App\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"details\":null}', 3),
(93, 11, 'id', 'number', 'Id', 1, 1, 0, 0, 0, 0, NULL, 1),
(94, 11, 'fio', 'text', 'ФИО', 0, 1, 1, 0, 0, 1, NULL, 2),
(95, 11, 'phone', 'text', 'Телефон', 0, 1, 1, 0, 0, 1, NULL, 3),
(96, 11, 'email', 'text', 'Емайл', 0, 0, 1, 0, 0, 1, NULL, 4),
(97, 11, 'city', 'text', 'Город', 0, 0, 1, 1, 1, 0, NULL, 5),
(98, 11, 'payment', 'select_dropdown', 'Платеж', 0, 0, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"1\":\"Наложенным платежом\",\"2\":\"Оплата по банковским реквизитам\",\"3\":\"Наличными курьеру (только при доставке курьером по Киеву)\"}}', 6),
(99, 11, 'delivery', 'select_dropdown', 'Способ доставки', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"options\":{\"1\":\"Новой почтой\",\"2\":\"Курьером Новой почты по Киеву (при условии 100% оплаченного заказа)\",\"3\":\"Самовывоз из магазина в Киеве\",\"\":\"----------------\"}}', 7),
(100, 11, 'status_id', 'select_dropdown', 'Статус заказ', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"1\":\"Новый заказ\",\"2\":\"В обработке\",\"3\":\"Успешный заказ\"}}', 8),
(101, 11, 'np_city', 'text', 'Город (Новая Почта)', 0, 0, 1, 1, 1, 0, NULL, 9),
(102, 11, 'np_warehouse', 'text', 'Отделение (Новая Почта)', 0, 0, 1, 1, 1, 0, NULL, 10),
(103, 11, 'sum', 'text', 'Сума заказа', 0, 1, 1, 0, 0, 1, NULL, 11),
(104, 11, 'created_at', 'timestamp', 'Дата заказа', 0, 1, 1, 0, 0, 1, NULL, 13),
(105, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 14),
(106, 12, 'id', 'number', 'Id', 1, 1, 0, 0, 0, 0, NULL, 0),
(107, 12, 'product_id', 'checkbox', 'Товар', 0, 0, 0, 0, 0, 0, NULL, 2),
(108, 12, 'count', 'text', 'Количество', 0, 1, 1, 1, 1, 1, NULL, 4),
(109, 12, 'sum', 'text', 'Сума', 0, 1, 1, 1, 1, 1, NULL, 5),
(110, 11, 'order_hasmany_order_product_relationship', 'relationship', 'Товары в заказе', 0, 0, 1, 1, 0, 0, '{\"model\":\"App\\\\OrderProduct\",\"table\":\"order_products\",\"type\":\"hasMany\",\"column\":\"order_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"details\":\"{\\\"list\\\":\\\"datatable\\\"}\"}', 15),
(111, 12, 'order_product_belongsto_product_relationship', 'relationship', 'Товар', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"product_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"details\":null}', 3),
(112, 12, 'order_id', 'checkbox', 'Заказ', 0, 0, 0, 0, 0, 0, NULL, 6),
(113, 11, 'description', 'text_area', 'Комментарий к заказу', 0, 0, 1, 0, 0, 0, NULL, 12),
(114, 8, 'video', 'text', 'Видео с ютюб', 0, 0, 0, 1, 1, 0, NULL, 20),
(115, 13, 'id', 'number', 'Id', 1, 1, 0, 0, 0, 0, NULL, 1),
(116, 13, 'name', 'text', 'Название', 0, 1, 1, 1, 1, 0, NULL, 3),
(117, 13, 'video', 'text', 'Видео с ютюб', 0, 0, 1, 1, 1, 0, NULL, 4),
(118, 13, 'product_id', 'select_dropdown', 'Товар', 0, 0, 0, 1, 1, 0, '{\"relationship\":{\"key\":\"id\",\"label\":\"title\"}}', 2),
(119, 8, 'product_hasmany_product_video_relationship', 'relationship', 'Видео товара', 0, 0, 0, 1, 0, 0, '{\"model\":\"App\\\\ProductVideo\",\"table\":\"product_videos\",\"type\":\"hasMany\",\"column\":\"product_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"details\":\"{\\\"list\\\":\\\"datatable\\\"}\"}', 23),
(120, 8, 'order_', 'number', 'Порядок в меню', 0, 0, 0, 1, 1, 0, NULL, 21);

-- --------------------------------------------------------

--
-- Структура таблицы `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `pagination` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'js',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `details`, `generate_permissions`, `pagination`, `server_side`, `created_at`, `updated_at`) VALUES
(1, 'posts', 'posts', 'Post', 'Posts', 'admin-news', 'LaravelAdminPanel\\Models\\Post', 'LaravelAdminPanel\\Policies\\PostPolicy', '', '', NULL, 1, 'js', 0, '2019-01-24 17:10:19', '2019-01-24 17:10:19'),
(2, 'pages', 'pages', 'Page', 'Pages', 'admin-file-text', 'App\\Page', NULL, NULL, NULL, NULL, 1, 'js', 0, '2019-01-24 17:10:19', '2019-01-28 18:04:08'),
(3, 'users', 'users', 'User', 'Users', 'admin-person', 'LaravelAdminPanel\\Models\\User', 'LaravelAdminPanel\\Policies\\UserPolicy', '', '', NULL, 1, 'js', 0, '2019-01-24 17:10:19', '2019-01-24 17:10:19'),
(4, 'categories', 'categories', 'Категория', 'Категории', 'admin-categories', 'App\\Category', NULL, NULL, NULL, NULL, 1, 'js', 0, '2019-01-24 17:10:19', '2019-02-06 14:31:19'),
(5, 'menus', 'menus', 'Menu', 'Menus', 'admin-list', 'LaravelAdminPanel\\Models\\Menu', NULL, '', '', NULL, 1, 'js', 0, '2019-01-24 17:10:19', '2019-01-24 17:10:19'),
(6, 'roles', 'roles', 'Role', 'Roles', 'admin-lock', 'LaravelAdminPanel\\Models\\Role', NULL, '', '', NULL, 1, 'js', 0, '2019-01-24 17:10:19', '2019-01-24 17:10:19'),
(7, 'form_designer', 'form-designer', 'Form Designer', 'Forms Designer', 'designer-list', 'LaravelAdminPanel\\Models\\FormDesigner', NULL, '', '', NULL, 1, 'js', 0, '2019-01-24 17:10:19', '2019-01-24 17:10:19'),
(8, 'products', 'products', 'Товар', 'Товары', 'admin-lighthouse', 'App\\Product', NULL, NULL, NULL, '{\"browse\":{\"service_buttons\":{\"read\":{\"permission\":\"read\",\"title\":\"Смотреть на сайте\",\"class\":\"btn-warning\",\"icon\":\"admin-eye\",\"attribute\":\"url\",\"target\":\"blank\"}}},\"after_save\":\"redirect_to_edit\"}', 1, 'js', 0, '2019-02-03 16:04:04', '2019-02-06 10:06:55'),
(9, 'product_functions', 'product-functions', 'Функиция Товара', 'Функции товаров', NULL, 'App\\ProductFunction', NULL, NULL, NULL, NULL, 1, 'js', 0, '2019-02-05 14:37:33', '2019-02-05 14:37:33'),
(10, 'product_images', 'product-images', 'Картинки товара', 'Картинки товаров', NULL, 'App\\ProductImage', NULL, NULL, NULL, NULL, 1, 'js', 0, '2019-02-05 20:46:57', '2019-02-05 20:46:57'),
(11, 'orders', 'orders', 'Заказ', 'Заказы', 'admin-window-list', 'App\\Order', NULL, NULL, NULL, NULL, 1, 'js', 0, '2019-02-06 21:29:26', '2019-02-06 21:29:26'),
(12, 'order_products', 'order-products', 'Товар в заказе', 'Товары в заказе', NULL, 'App\\OrderProduct', NULL, NULL, NULL, NULL, 1, 'js', 0, '2019-02-06 21:45:51', '2019-02-06 21:45:51'),
(13, 'product_videos', 'product-videos', 'Видео к товару', 'Видео к товару', 'admin-video', 'App\\ProductVideo', NULL, NULL, NULL, NULL, 1, 'js', 0, '2019-02-20 17:26:59', '2019-02-20 17:26:59');

-- --------------------------------------------------------

--
-- Структура таблицы `form_designer`
--

CREATE TABLE `form_designer` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `form_designer`
--

INSERT INTO `form_designer` (`id`, `data_type_id`, `options`) VALUES
(1, 8, '[{\"class\":\"col-md-7\",\"panels\":[{\"class\":\"panel\",\"title\":\"Информация\",\"fields\":[\"category_id\",\"title\",\"slug\",\"body\",\"active\",\"price\",\"accessories\",\"type\",\"badge\",\"text_badge\",\"accompanying_goods\",\"order_\"]}]},{\"class\":\"col-md-5\",\"panels\":[{\"class\":\"panel panel-bordered panel-primary\",\"title\":\"Блок изображения\",\"fields\":[\"photo_alt\",\"image\",\"video\"]},{\"class\":\"panel panel-bordered panel-info\",\"title\":\"admin.post.seo_content\",\"fields\":[\"meta_title\",\"meta_keywords\",\"meta_description\"]}]}]');

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-01-24 17:10:19', '2019-01-24 17:10:19');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`, `details`) VALUES
(1, 1, 'Dashboard', '', '_self', 'admin-boat', NULL, NULL, 1, '2019-01-24 17:10:19', '2019-01-24 17:10:19', 'admin.dashboard', NULL, ''),
(2, 1, 'Media', '', '_self', 'admin-images', NULL, NULL, 5, '2019-01-24 17:10:19', '2019-01-24 17:10:19', 'admin.media.index', NULL, ''),
(3, 1, 'Posts', '', '_self', 'admin-news', NULL, NULL, 6, '2019-01-24 17:10:19', '2019-01-24 17:10:19', 'admin.posts.index', NULL, ''),
(4, 1, 'Users', '', '_self', 'admin-person', NULL, NULL, 3, '2019-01-24 17:10:19', '2019-01-24 17:10:19', 'admin.users.index', NULL, ''),
(5, 1, 'Категории', '', '_self', 'admin-categories', '#000000', NULL, 8, '2019-01-24 17:10:19', '2019-02-06 14:36:40', 'admin.categories.index', 'null', ''),
(6, 1, 'Страницы', '', '_self', 'admin-file-text', '#000000', NULL, 7, '2019-01-24 17:10:19', '2019-01-28 18:01:02', 'admin.pages.index', 'null', ''),
(7, 1, 'Roles', '', '_self', 'admin-lock', NULL, NULL, 2, '2019-01-24 17:10:19', '2019-01-24 17:10:19', 'admin.roles.index', NULL, ''),
(8, 1, 'Tools', '', '_self', 'admin-tools', NULL, NULL, 9, '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL, NULL, ''),
(9, 1, 'Menu Builder', '', '_self', 'admin-list', NULL, 8, 10, '2019-01-24 17:10:19', '2019-01-24 17:10:19', 'admin.menus.index', NULL, ''),
(10, 1, 'Form Designer', '', '_self', 'admin-wand', NULL, 8, 11, '2019-01-24 17:10:19', '2019-01-24 17:10:19', 'admin.form-designer.index', NULL, ''),
(11, 1, 'Database', '', '_self', 'admin-data', NULL, 8, 11, '2019-01-24 17:10:19', '2019-01-24 17:10:19', 'admin.database.index', NULL, ''),
(12, 1, 'Compass', '/admin/compass', '_self', 'admin-compass', NULL, 8, 12, '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL, NULL, ''),
(13, 1, 'Settings', '', '_self', 'admin-settings', NULL, NULL, 14, '2019-01-24 17:10:19', '2019-01-24 17:10:19', 'admin.settings.index', NULL, ''),
(14, 1, 'Товары', '/admin/products', '_self', 'admin-lighthouse', '#000000', NULL, 15, '2019-02-03 16:04:05', '2019-02-05 10:15:38', NULL, '', ''),
(17, 1, 'Заказы', '/admin/orders', '_self', 'admin-window-list', NULL, NULL, 16, '2019-02-06 21:29:27', '2019-02-06 21:29:27', NULL, NULL, '{\"model\":\"Order\",\"badges\": [{\"field\": \"status_id\",\"operator\": \"=\",\"value\": \"1\",\"class\": \"warning\"}] }');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_admin_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_add_permission_group_id_to_permissions_table', 1),
(17, '2017_01_15_000000_create_permission_groups_table', 1),
(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(20, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(21, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(22, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(23, '2017_08_05_000000_add_group_to_settings_table', 1),
(24, '2018_04_05_200325_add_details_to_data_types_table', 1),
(25, '2018_04_09_052104_create_form_designer_table', 1),
(26, '2018_05_22_114810_add_pagination_to_data_types_table', 1),
(27, '2018_06_06_074941_alter_data_types_table_name_not_unique', 1),
(28, '2018_06_06_164024_alter_permissions_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `fio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `np_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `np_warehouse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sum` varchar(222) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `fio`, `phone`, `email`, `city`, `payment`, `delivery`, `status_id`, `np_city`, `np_warehouse`, `sum`, `created_at`, `updated_at`, `description`) VALUES
(1, 'Надя Дорофеева', '4444412222', 'po', 'Тернополь', '3', '3', 3, NULL, NULL, '7491', '2019-02-06 21:25:00', '2019-03-19 11:08:48', NULL),
(2, 'Надя Дорофеева', '4444412222', 'kurwa.matka@gmail.com', 'Тернополь', '2', '1', 3, NULL, NULL, '7491', '2019-02-06 21:59:58', '2019-03-19 11:09:10', NULL),
(3, 'Арнольд Шварценигр', '12312321', 'test@gmail.com', 'Киев', '2', '1', 3, NULL, NULL, '7491', '2019-02-06 22:02:11', '2019-03-19 11:13:03', NULL),
(4, 'Ольга', '234534534534', 'imotolife@gmail.com', 'Киев', '3', '3', 3, NULL, NULL, '55', '2019-02-07 10:06:04', '2019-03-19 11:13:22', 'Комментарий к заказу'),
(5, 'Ирина Билык uk', '234534534534', 'imotolife@gmail.com', 'Киев', '3', '3', 3, NULL, NULL, '776', '2019-02-07 10:18:55', '2019-03-19 11:13:38', 'ddsfsdafasdfasdf'),
(6, 'Кудрявцева', '0954256800', 'yaroslava.kudryavtseva@gmail.com', 'Киев', '3', '2', 3, NULL, NULL, '6830', '2019-02-20 20:17:08', '2019-03-19 12:59:50', NULL),
(7, 'Кудрявцева', '0954256800', 'yaroslava.kudryavtseva@gmail.com', 'Киев', '2', '3', 3, NULL, NULL, '12487', '2019-02-20 20:18:38', '2019-03-19 12:58:07', NULL),
(8, 'Кудрявцева', '0954256800', 'yaroslava.kudryavtseva@gmail.com', 'Киев', '2', '3', 3, NULL, NULL, '0', '2019-02-20 20:25:12', '2019-03-19 12:58:36', NULL),
(9, 'Кудрявцева 666', '0954256800', 'slava.trudu0@gmail.com', 'Киев', '3', '2', 3, NULL, NULL, '13990', '2019-02-20 20:26:32', '2019-02-20 20:29:23', NULL),
(10, 'Кудрявцева 555', '0954256800', 'slava.trudu0@gmail.com', 'Киев', '2', '1', 3, NULL, NULL, '12487', '2019-02-20 20:27:13', '2019-03-19 12:57:48', NULL),
(11, 'Василий', '+38(095) 425-68-00', 'slava.trudu0@gmail.com', 'Киев', '1', '2', 3, NULL, NULL, '3040', '2019-02-21 12:51:04', '2019-03-19 12:54:38', NULL),
(12, 'Василий', '+380 (95) 425-68-00', 'slava.trudu0@gmail.com', 'Киев', '2', '2', 3, NULL, NULL, '17505', '2019-02-21 12:56:26', '2019-03-19 12:54:50', NULL),
(13, 'Василий', '+38(095) 425-68-00', 'yaroslava.kudryavtseva@gmail.com', 'Киев', '2', '1', 3, NULL, NULL, '9290', '2019-02-21 13:44:21', '2019-03-19 12:55:01', NULL),
(18, 'Василий44', '+38(095) 425-68-00', 'slava.trudu0@gmail.com', 'Киев', '3', NULL, 3, NULL, NULL, '29212', '2019-02-22 10:01:56', '2019-02-22 10:02:16', NULL),
(19, 'Василий555', '+380 (95) 425-68-00', 'yaroslava.kudryavtseva@gmail.com', 'Киев', '1', '1', 3, 'Агрономичное', 'Отделение №1: ул. Мичурина, 6', '25350', '2019-02-22 10:03:15', '2019-03-19 13:00:09', NULL),
(20, 'Василий', '+38(095) 425-68-00', 'slava.trudu0@gmail.com', 'Киев', '3', NULL, 3, NULL, NULL, '550', '2019-02-25 11:18:14', '2019-03-19 12:56:18', NULL),
(23, 'Кудрявцева', '+380 (95) 425-68-00', 'yaroslava.kudryavtseva@gmail.com', 'Киев', '3', NULL, 3, NULL, NULL, '12487', '2019-02-28 15:41:05', '2019-03-19 12:56:50', NULL),
(24, 'Ирина', 'Мельник', 'ira.melnik.work@gmail.com', 'Киев', '1', '1', 3, 'Киев', 'Отделение №35 (до 30 кг): ул. Фучика, 3 (ЦСКА)', '5299', '2019-03-05 14:17:52', '2019-03-19 12:57:14', NULL),
(25, 'Кудрявцева', '0954256800', 'yaroslava.kudryavtseva@gmail.com', 'Киев', '1', '3', 3, NULL, NULL, '15527', '2019-03-06 06:57:59', '2019-03-19 11:08:17', NULL),
(26, 'Бла бла', '324203402023020', 'dasha19931@mail.ru', 'Київ', '1', '3', 3, NULL, NULL, '0', '2019-03-06 07:38:06', '2019-03-19 11:08:00', 'Тестове замовленя'),
(27, 'Стрипа Олег Романович', '0502518506', 'bartkiv1310@gmail.com', 'Киів', '1', '3', 3, NULL, NULL, '6250', '2019-03-22 13:19:20', '2019-03-22 17:10:02', NULL),
(28, 'Богославский А. А.', '0637718475', 'abogoslavsky52@gmail.com', 'Одесса', '1', '1', 3, 'Одесса', 'Отделение №13 (до 30 кг): ул. Семена Палия (ран. Днепропетровская дорога), 82', '3040', '2019-03-24 06:39:34', '2019-03-24 09:10:48', 'Коляска в чёрном цвете полностью необходима'),
(29, 'Шпак Анна Фёдоровна', '0662347248', 'annavasylkiv23@gmail.com', 'Херсон', '1', '1', 3, 'Херсон', 'Отделение №4: ул. Патона, 2а', '6538', '2019-03-26 22:10:37', '2019-03-27 12:05:44', NULL),
(30, 'Анастасия', '0507138777', 'anakorneeva@gmail.com', 'Киев', '2', '3', 3, NULL, NULL, '6250', '2019-03-28 17:35:29', '2019-03-28 18:34:21', NULL),
(31, 'Норець Яна', '0930778191', 'yanmax@ukr.net', 'Киев', '2', '2', 3, NULL, NULL, '9120', '2019-03-29 22:55:52', '2019-03-30 09:51:42', NULL),
(32, 'Остапенко  Ольга', '098 308 76 67', 'ostapenkoolga603615@gmail.com', 'Кривой Рог', '1', '1', 3, 'Кривой Рог', 'Отделение №7 (до 30 кг на одно место): ул. Свято-Николаевская (ран. Ленина), 55', '3760', '2019-04-01 12:17:03', '2019-04-01 17:08:55', 'Перезвоните, хочу уточнить адаптеры подходят между автокреслом Idan 0+ с коляской gb pocit plus 2018'),
(33, 'Янчук Людмила Андреевна', '0502954680', 'ludo4ka250990@gmail.com', 'Херсон', '1', '1', 3, 'Херсон', 'Отделение №15 (до 15 кг), Мини-отделение: ул. 49 Гвардейской Херсонской дивизии, 26-А', '6538', '2019-04-02 06:24:08', '2019-04-02 08:50:43', 'коляска qbit+ цвет черный, и подстаканник к ней'),
(34, 'Горох Наталья', '0505659814', 'nikakvd@gmail.com', 'Киев', '2', '3', 3, NULL, NULL, '10341', '2019-04-03 09:28:29', '2019-04-03 10:10:15', NULL),
(35, 'Лапичак Уляна Ігорівна', '+380631814705', 'Ulyana.mulyava1993@gmail.com', 'Львів', '1', '1', 3, 'Львов', 'Отделение №31 (до 30 кг): ул. Тершаковцев, 1', '288', '2019-04-06 21:40:07', '2019-04-07 08:01:09', NULL),
(36, 'Нагірняк Аліна Вікторівна', '9509698803', 'alina.nagirnyak@gmail.com', 'Івано-Франківськ', '2', '1', 3, 'Ивано-Франковск', 'Отделение №8 (до 30 кг): ул. Миколайчука, 30', '13990', '2019-04-07 14:45:48', '2019-04-08 08:59:39', NULL),
(37, 'Хомутянская Ольга', '0959319377', 'khomutman@gmail.com', 'Днепр', '1', '1', 3, 'Днепр', 'Отделение №166 (до 30 кг): бульв. Славы, 5к', '3040', '2019-04-08 18:01:40', '2019-04-09 07:31:42', NULL),
(38, 'Конон Наталья', '0978172980', 'Natalilarionova7777@gmail.com', 'Киев', '3', '3', 3, NULL, NULL, '6080', '2019-04-09 18:33:35', '2019-04-10 16:53:29', NULL),
(39, 'Павлюк София', '0966766158', 'Moreemothinal@gmail.com', 'Чернигов', '1', '1', 3, 'Чернигов', 'Отделение №4 (до 15 кг): ул. Рокоссовского, 68', '1999', '2019-04-10 11:32:47', '2019-04-10 17:28:49', NULL),
(40, 'Захарова Маргарита Витальевна', '0950761466', 'Margo999zp@gmail.com', 'Запорожье', '2', '1', 3, 'Запорожье', 'Отделение №21 (до 30 кг на одно место): ул. Товарищеская, 56 (ул. Ладожская, 25)', '2900', '2019-04-10 17:15:41', '2019-04-10 17:43:31', 'Цвет чёрный'),
(41, 'Oliver Queen', '18100500', 'Sobakato4ka5@gmail.com', 'Киев', '1', '1', 3, 'Апостолово', 'Отделение №1: ул. Освобождения, 21', '13990', '2019-04-30 08:41:12', '2019-04-30 08:56:23', '111'),
(42, 'Хомутянская Ольга', '0959319377', 'khomutman@gmail.com', 'Днепр', '1', '1', 3, 'Днепр', 'Отделение №166 (до 30 кг): бульв. Славы, 5к', '660', '2019-05-05 17:48:48', '2019-05-06 07:06:03', NULL),
(43, 'Сивак Анастасія', '0969813409', 'Nens312@gmail.com', 'Тернопіль', '2', '1', 1, 'Тернополь', 'Отделение №1: ул. Подольская, 21', '6250', '2019-05-07 15:18:39', '2019-05-07 15:18:39', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `order_products`
--

CREATE TABLE `order_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `sum` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `order_products`
--

INSERT INTO `order_products` (`id`, `product_id`, `count`, `sum`, `order_id`) VALUES
(1, 1, 3, '69', 3),
(2, 2, 2, '444', 3),
(3, 5, 1, '312', 3),
(4, 7, 1, '6666', 3),
(5, 3, 1, '55', 4),
(6, 2, 3, '666', 5),
(7, 3, 2, '110', 5),
(8, 12, 1, '6830', 6),
(9, 1, 1, '12487', 7),
(10, 5, 1, '13990', 9),
(11, 1, 1, '12487', 10),
(12, 2, 1, '3040', 11),
(13, 2, 1, '3040', 12),
(14, 5, 1, '13990', 12),
(15, 8, 1, '475', 12),
(16, 2, 1, '3040', 13),
(17, 3, 1, '6250', 13),
(18, 1, 1, '12487', 14),
(19, 4, 1, '4238', 14),
(20, 2, 1, '3040', 15),
(21, 2, 1, '3040', 16),
(22, 1, 1, '12487', 17),
(23, 1, 2, '24974', 18),
(24, 4, 1, '4238', 18),
(25, 1, 1, '12487', 19),
(26, 4, 1, '4238', 19),
(27, 13, 1, '8625', 19),
(28, 22, 1, '550', 20),
(29, 1, 1, '12487', 21),
(30, 1, 1, '12487', 22),
(31, 1, 1, '12487', 23),
(32, 14, 1, '5299', 24),
(33, 1, 1, '12487', 25),
(34, 2, 1, '3040', 25),
(35, 3, 1, '6250', 27),
(36, 2, 1, '3040', 28),
(37, 3, 1, '6250', 29),
(38, 21, 1, '288', 29),
(39, 3, 1, '6250', 30),
(40, 2, 3, '9120', 31),
(41, 2, 1, '3040', 32),
(42, 19, 1, '720', 32),
(43, 3, 1, '6250', 33),
(44, 21, 1, '288', 33),
(45, 3, 1, '6250', 34),
(46, 17, 1, '288', 34),
(47, 20, 1, '660', 34),
(48, 23, 1, '1152', 34),
(49, 24, 1, '1991', 34),
(50, 21, 1, '288', 35),
(51, 5, 1, '13990', 36),
(52, 2, 1, '3040', 37),
(53, 2, 2, '6080', 38),
(54, 26, 1, '1999', 39),
(55, 25, 1, '2900', 40),
(56, 5, 1, '13990', 41),
(57, 18, 1, '660', 42),
(58, 3, 1, '6250', 43);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`, `meta_title`) VALUES
(1, 'Спасибо за заказ', NULL, NULL, NULL, 'thanks', 'Спасибо за заказ', 'Спасибо за заказ', 2, '2019-01-28 18:06:12', '2019-01-28 18:17:15', 'Спасибо за заказ на сайте'),
(2, 'Сайт GB', NULL, NULL, NULL, 'home', '#GB Qbit - коляски будущего. Легкие, стильные и функциональные коляски для современных родителей.  Коляски GB - Стиль твоего города! Счастливая мама - довольно дитя! GB Pockit коляски для активных мам и их любимых малышей!', 'GB, GOODBABY, коляски GB, автокресла GB, официальный сайт gb, gb ua', NULL, '2019-01-28 18:23:53', '2019-02-27 10:14:44', 'GB эксклюзивный представитель GB в Украине'),
(3, 'Оформление заказа', NULL, NULL, NULL, 'order', 'Заказ', 'Заказ', NULL, '2019-01-28 18:28:48', '2019-01-28 18:29:19', 'Оформление заказа'),
(4, 'О бренде', NULL, '<p style=\"text-align: left;\"><strong><span style=\"color: #000000; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 20px;\">Самое ценное &mdash; это наши дети!</span></span></strong></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\">Поэтому с первых дней рождения малыша безопасность является первостепенной задачей родителей.</span></span></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\">Учитывая общее количество брендов и вариативности, представленных на рынке, для родителей - выбор лучших товаров для малыша, учитывая уровень безопасности и легкости использования, является непростой задачей.&nbsp;</span></span><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\">Мы рады приветствовать Вас в нашем фирменном магазине <a href=\"https://gbaby.com.ua\">https://gbaby.com.ua</a>/. Именно мы являемся официальным представителем бренд Gb в Украине!</span></span></p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\"><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\">Gb является одним из лидирующих брендов в области автомобильной безопасности, технологичности и стиля. Бренд</span></span><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\"> Gb представлена эксклюзивной линейкой детских автокресел, колясок для путешествий и систем 3 в 1.</span></span></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\">Благодаря современным принципам работы, Gb предлагает молодым родителям решение многих задач, путем создания инновационных продуктов в сочетании с превосходными показателями защиты, стиля и функциональности.</span></span><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\">&nbsp;</span></span></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\">Gb приглашает вас в уникальное путешествие в будущее - \"Идеальное будущее\" как четкая приверженность нашему принципу \"не смотри в прошлое\"</span></span></p>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: center;\"><img src=\"https://gbaby.com.ua/storage/pages/February2019/LAR_3414 (1)-min.jpg\" alt=\"Мы рады приветствовать вас в нашем фирменном магазине gbaby.com.ua \" width=\"100%\" height=\"100%\" /></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\">Мы рады приветствовать вас в нашем фирменном магазине gbaby.com.ua</span></span></p>\r\n<h2 style=\"text-align: left;\"><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 15px;\">Именно мы являемся официальным представителем бренд Gb в Украине!</span></span></h2>\r\n<p><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 20px;\">&nbsp;</span></span></p>\r\n<p><span style=\"color: #1d2129; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 20px;\">&nbsp;</span></span></p>\r\n<p>&nbsp;</p>', NULL, 'about', 'Интернет магазин официального представительства GB в Украине.  Эксклюзивный представитель GB в Украине.', 'gb коляски официальный сайт, GOODBABY коляски официальный сайт, сайт гудбеби, сайт gb, автокресла gb официальный сайт,', NULL, '2019-02-03 16:20:19', '2019-03-12 07:47:53', 'Интернет магазин официального представительства GB в Украине.  Эксклюзивный представитель GB в Украине.'),
(5, '404', NULL, '<p>Страница не найдена. Вы можете попробовать еще раз, вернувшись на <strong><a href=\"https://gbaby.com.ua/\">главную страницу</a>&nbsp;</strong>, либо ознакомиться с ассортиментом <a href=\"https://gbaby.com.ua/seats\"><strong>автокресел</strong></a>, <a href=\"https://gbaby.com.ua/strollers\"><strong>колясок</strong></a> и <a href=\"https://gbaby.com.ua/accessories\"><strong>аксессуаров</strong></a> Gb</p>', NULL, '404', '404', '404', NULL, '2019-02-03 16:25:00', '2019-02-22 10:32:54', '404'),
(6, 'Где купить', NULL, '<p><strong>Bаbypark</strong></p>\r\n<p>ул. Черновола, 2</p>\r\n<p>&nbsp;г. Киев&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Bаbypark</strong></p>\r\n<p>Днепровская набережная, 26и</p>\r\n<p>&nbsp;г. Киев</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Bаbypark</strong></p>\r\n<p>ул. Гетьмана, 13</p>\r\n<p>&nbsp;г. Киев&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Bаbypark</strong></p>\r\n<p>ул. Малышка, 3а</p>\r\n<p>&nbsp;г. Киев</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Bаbypark</strong></p>\r\n<p>пр-т. Степана Бандеры, 8</p>\r\n<p>&nbsp;г. Киев</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Bаbypark</strong></p>\r\n<p>пр-т. Дмитрия Яворницкого, 58</p>\r\n<p>&nbsp;г. Днепр</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Kidsline</strong></p>\r\n<p>Столичное шоссе, 103&nbsp;ТЦ Атмосфера, 1-й этаж</p>\r\n<p>г. Киев</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Mini U</strong></p>\r\n<p>ул. Бассейная, 4, ТЦ Mandarin Plaza, 4-й этаж<br />+ 380 (67) 555 94 85</p>\r\n<p>г. Киев</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Тeddy</strong></p>\r\n<p>пр-т. К. Маркса, 24</p>\r\n<p>г. Днепр</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Kidsline</strong></p>\r\n<p>ул. Казимира Малевича, 48</p>\r\n<p>г. Киев&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Vip Baby</strong></p>\r\n<p>ул.Скорини, 44</p>\r\n<p>г. Львов</p>\r\n<p>&nbsp;</p>\r\n<p><strong>VipBaby</strong></p>\r\n<p>ул. Скорини, 44&nbsp;</p>\r\n<p>г. Львов&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>VIPMALUK</strong></p>\r\n<p>Оболонская набережная, 7, корп. 5</p>\r\n<p>&nbsp;г. Киев</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Автокрісла.сom</strong></p>\r\n<p>ТЦ Карван, ул. Луговая, 12</p>\r\n<p>&nbsp;г. Киев&nbsp;<br /><br /></p>\r\n<p><strong>Автокрісла.сom</strong></p>\r\n<p>ул. Андрея Малышка, 3&nbsp;ТЦ Детский Мир, 3-й этаж&nbsp;</p>\r\n<p>г. Киев</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Автокрісла.сom</strong></p>\r\n<p>ТРЦ Караван, ул. Нижнеднепровская, 17</p>\r\n<p>г. Днепр</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Автокрісла.сom</strong></p>\r\n<p>ТЦ Караван, ул. Героев Труда, 7</p>\r\n<p>г. Харьков</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Автокрісла.сom</strong></p>\r\n<p>ул. Пантелеймоновская, 25&nbsp;ТЦ Новый Привоз, 4-й этаж</p>\r\n<p>г. Одесса</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Антошка</strong></p>\r\n<p>пл. Греческая, 2&nbsp;</p>\r\n<p>г. Одесса</p>\r\n<p>&nbsp;</p>\r\n<p><strong>ВізОК</strong></p>\r\n<p>ТЦ Атриум, ул. Проскуровского подполья, 63&nbsp;&nbsp;</p>\r\n<p>г. Хмельницкий</p>\r\n<p>&nbsp;&nbsp;</p>\r\n<p><strong>Веселий Світ</strong></p>\r\n<p>ул. Вайсера, 17/2&nbsp;</p>\r\n<p>г. Хмельницкий</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Дочкам і Сночкам</strong></p>\r\n<p>Проспект Грушевского, 22/1</p>\r\n<p>г. Каменец-Подольский</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Интернет-магазин &laquo;BabyPro&raquo;</strong></p>\r\n<p>г. Днепр&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Країна Мрій</strong></p>\r\n<p>ул. Большая Перспективная 6/3</p>\r\n<p>г. Кропивницкий</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Лелека</strong></p>\r\n<p>Росвигово, ул. Ужгородская, 12&nbsp;</p>\r\n<p>г. Мукачево&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Мажорики</strong></p>\r\n<p>ул. Екатерининская, 27/1&nbsp;ТЦ Кадорр, 5-й этаж&nbsp;</p>\r\n<p>г. Одесса</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Мамин Дом</strong></p>\r\n<p>ул. Академика Вильямса, 5&nbsp;</p>\r\n<p>г. Киев</p>\r\n<p>&nbsp;</p>\r\n<p><strong>МирАвтокресел</strong></p>\r\n<p>пр-т. Голосеевский, 104&nbsp;&laquo;Московский&raquo; универмаг, 3-й этаж&nbsp;&nbsp;</p>\r\n<p>г. Киев</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Наш Малыш</strong></p>\r\n<p>пр. Центральный, 2Б, рынок &laquo;Эвис&raquo;&nbsp;</p>\r\n<p>г. Николаев&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', NULL, 'where-can-buy', 'gde-kupit', 'Интернет магазин официального представительства GB в Украине. Эксклюзивный представитель GB в Украине.', NULL, '2019-02-03 16:26:14', '2019-03-06 09:18:35', 'gde-kupit');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `slug`, `created_at`, `updated_at`, `permission_group_id`) VALUES
(1, 'browse_admin', NULL, '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(2, 'browse_database', NULL, '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(3, 'browse_media', NULL, '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(4, 'browse_compass', NULL, '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(5, 'browse_menus', 'menus', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(6, 'read_menus', 'menus', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(7, 'edit_menus', 'menus', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(8, 'add_menus', 'menus', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(9, 'delete_menus', 'menus', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(10, 'browse_pages', 'pages', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(11, 'read_pages', 'pages', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(12, 'edit_pages', 'pages', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(13, 'add_pages', 'pages', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(14, 'delete_pages', 'pages', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(15, 'browse_roles', 'roles', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(16, 'read_roles', 'roles', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(17, 'edit_roles', 'roles', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(18, 'add_roles', 'roles', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(19, 'delete_roles', 'roles', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(20, 'browse_users', 'users', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(21, 'read_users', 'users', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(22, 'edit_users', 'users', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(23, 'add_users', 'users', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(24, 'delete_users', 'users', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(25, 'browse_posts', 'posts', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(26, 'read_posts', 'posts', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(27, 'edit_posts', 'posts', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(28, 'add_posts', 'posts', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(29, 'delete_posts', 'posts', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(30, 'browse_categories', 'categories', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(31, 'read_categories', 'categories', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(32, 'edit_categories', 'categories', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(33, 'add_categories', 'categories', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(34, 'delete_categories', 'categories', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(35, 'browse_settings', 'settings', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(36, 'read_settings', 'settings', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(37, 'edit_settings', 'settings', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(38, 'add_settings', 'settings', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(39, 'delete_settings', 'settings', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(40, 'browse_form-designer', 'form-designer', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(41, 'read_form-designer', 'form-designer', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(42, 'edit_form-designer', 'form-designer', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(43, 'add_form-designer', 'form-designer', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(44, 'delete_form-designer', 'form-designer', '2019-01-24 17:10:19', '2019-01-24 17:10:19', NULL),
(45, 'browse_products', 'products', '2019-02-03 16:04:04', '2019-02-03 16:04:04', NULL),
(46, 'read_products', 'products', '2019-02-03 16:04:04', '2019-02-03 16:04:04', NULL),
(47, 'edit_products', 'products', '2019-02-03 16:04:04', '2019-02-03 16:04:04', NULL),
(48, 'add_products', 'products', '2019-02-03 16:04:04', '2019-02-03 16:04:04', NULL),
(49, 'delete_products', 'products', '2019-02-03 16:04:04', '2019-02-03 16:04:04', NULL),
(50, 'browse_product-functions', 'product-functions', '2019-02-05 14:37:34', '2019-02-05 14:37:34', NULL),
(51, 'read_product-functions', 'product-functions', '2019-02-05 14:37:34', '2019-02-05 14:37:34', NULL),
(52, 'edit_product-functions', 'product-functions', '2019-02-05 14:37:34', '2019-02-05 14:37:34', NULL),
(53, 'add_product-functions', 'product-functions', '2019-02-05 14:37:34', '2019-02-05 14:37:34', NULL),
(54, 'delete_product-functions', 'product-functions', '2019-02-05 14:37:34', '2019-02-05 14:37:34', NULL),
(55, 'browse_product-images', 'product-images', '2019-02-05 20:46:57', '2019-02-05 20:46:57', NULL),
(56, 'read_product-images', 'product-images', '2019-02-05 20:46:57', '2019-02-05 20:46:57', NULL),
(57, 'edit_product-images', 'product-images', '2019-02-05 20:46:57', '2019-02-05 20:46:57', NULL),
(58, 'add_product-images', 'product-images', '2019-02-05 20:46:57', '2019-02-05 20:46:57', NULL),
(59, 'delete_product-images', 'product-images', '2019-02-05 20:46:57', '2019-02-05 20:46:57', NULL),
(60, 'browse_orders', 'orders', '2019-02-06 21:29:26', '2019-02-06 21:29:26', NULL),
(61, 'read_orders', 'orders', '2019-02-06 21:29:26', '2019-02-06 21:29:26', NULL),
(62, 'edit_orders', 'orders', '2019-02-06 21:29:26', '2019-02-06 21:29:26', NULL),
(63, 'add_orders', 'orders', '2019-02-06 21:29:26', '2019-02-06 21:29:26', NULL),
(64, 'delete_orders', 'orders', '2019-02-06 21:29:26', '2019-02-06 21:29:26', NULL),
(65, 'browse_order-products', 'order-products', '2019-02-06 21:45:51', '2019-02-06 21:45:51', NULL),
(66, 'read_order-products', 'order-products', '2019-02-06 21:45:51', '2019-02-06 21:45:51', NULL),
(67, 'edit_order-products', 'order-products', '2019-02-06 21:45:51', '2019-02-06 21:45:51', NULL),
(68, 'add_order-products', 'order-products', '2019-02-06 21:45:51', '2019-02-06 21:45:51', NULL),
(69, 'delete_order-products', 'order-products', '2019-02-06 21:45:51', '2019-02-06 21:45:51', NULL),
(70, 'browse_product-videos', 'product-videos', '2019-02-20 17:26:59', '2019-02-20 17:26:59', NULL),
(71, 'read_product-videos', 'product-videos', '2019-02-20 17:26:59', '2019-02-20 17:26:59', NULL),
(72, 'edit_product-videos', 'product-videos', '2019-02-20 17:26:59', '2019-02-20 17:26:59', NULL),
(73, 'add_product-videos', 'product-videos', '2019-02-20 17:26:59', '2019-02-20 17:26:59', NULL),
(74, 'delete_product-videos', 'product-videos', '2019-02-20 17:26:59', '2019-02-20 17:26:59', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `permission_groups`
--

CREATE TABLE `permission_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(10, 2),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(45, 2),
(46, 1),
(46, 2),
(47, 1),
(47, 2),
(48, 1),
(48, 2),
(49, 1),
(49, 2),
(50, 1),
(50, 2),
(51, 1),
(51, 2),
(52, 1),
(52, 2),
(53, 1),
(53, 2),
(54, 1),
(54, 2),
(55, 1),
(55, 2),
(56, 1),
(56, 2),
(57, 1),
(57, 2),
(58, 1),
(58, 2),
(59, 1),
(59, 2),
(60, 1),
(60, 2),
(61, 1),
(61, 2),
(62, 1),
(62, 2),
(63, 1),
(63, 2),
(64, 1),
(65, 1),
(65, 2),
(70, 1),
(70, 2),
(71, 1),
(71, 2),
(72, 1),
(72, 2),
(73, 1),
(73, 2),
(74, 1),
(74, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `accessories` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accompanying_goods` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `badge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_badge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `title`, `price`, `image`, `meta_title`, `meta_description`, `meta_keywords`, `created_at`, `updated_at`, `slug`, `active`, `product_id`, `type`, `body`, `accessories`, `accompanying_goods`, `badge`, `text_badge`, `category_id`, `video`, `order_`) VALUES
(1, 'Maris Day Dream', 12487, 'products/February2019/Maris.png', 'Коляска Maris Day Dream', 'Коляска Maris Day Dream', 'Коляска Maris Day Dream, купить коляску Maris Day Dream, коляска для прогулок Коляска Maris Day Dream, прогулочный блок Коляска Maris Day Dream купить', '2019-02-03 16:07:42', '2019-02-21 15:39:07', 'maris-day-dream', '1', NULL, 'strollers', '<p>Коляска от рождения до 17 кг (приблизительно до 4-х лет). Коляска, бренда gb Maris из коллекции Day Dream &ndash; это новое сочетание стиля и функций без&nbsp;ограничений. Оптический дизайн, плавные линии и оттенки черного цвета навеивают&nbsp;спокойствие, но в то же время привлекают внимание. Благодаря своей компактности, коляска&nbsp;задает новый стандарт городской системы путешествий 3 в 1.</p>', '[\"8\",\"13\",\"14\",\"15\",\"16\",\"17\"]', '[\"4\",\"13\"]', NULL, 'НОВИНКА', 5, 'https://www.youtube.com/embed/7H00rVDsleQ', NULL),
(2, 'Pockit+18', 3040, 'products/February2019/Pockit+.png', 'Pockit+', 'Pockit+ Коляска для путешествий от 6 месяцев до 17 кг', 'коляска Pockit, купить коляску покит', '2019-02-03 16:10:08', '2019-03-12 12:09:46', 'pockit+18', '1', NULL, 'strollers', NULL, '[\"7\",\"8\",\"18\",\"19\"]', '[\"4\",\"11\",\"25\",\"26\"]', NULL, 'LIMITED EDITION', 4, '0', 2),
(3, 'Qbit+', 6250, 'products/May2019/GB_18_y045_EU_LABL_QbitPlus_bumperbar_DERV_HQ.png', 'Qbit+', 'Прогулочная коляска Qbit+ от рождения до 17 кг (приблизительно до 4-х лет) Функциональная коляска Qbit+, которая обеспечивает максимальный комфорт и удобство для мамы и ребёнка. GB Qbit+ невероятно стильная и легкая коляска на каждый день.', NULL, '2019-02-03 16:14:53', '2019-05-03 15:27:37', 'qbit', '1', NULL, 'strollers', '<p>Прогулочная коляска Qbit+ от рождения до 17 кг (приблизительно до 4-х лет) Функциональная коляска Qbit+, которая обеспечивает максимальный комфорт и удобство для мамы и ребёнка. GB Qbit+ невероятно стильная и легкая коляска на каждый день.&nbsp;&nbsp;</p>', '[\"7\"]', '[\"11\",\"19\",\"20\"]', NULL, 'ПРЕДЗАКАЗ', 4, 'https://www.youtube.com/embed/Cdn_UL_q438', 4),
(4, 'Автокресло IDAN 0+', 4238, 'products/February2019/GB_171111111.png', 'avtokreslo-idan', 'avtokreslo-idan', 'автокресло идан, автокресло idan купить, купить idan', '2019-02-05 10:17:37', '2019-03-06 09:37:39', 'avtokreslo-idan', '1', NULL, 'seats', '<p>Автокресло гр. 0+ (до 13 кг)&nbsp; Idan из Platinum серии объединяет в себе дизайн hi-tech и инновационные функции безопасности. C таким автокреслом любая поездка пройдет легко и безопасно, а малышу будет удобно и комфортно.</p>', NULL, '[\"1\"]', NULL, NULL, 6, 'https://www.youtube.com/embed/U7zF4HzskvQ', NULL),
(5, 'Автокресло Vaya i-size ГР. 0+/1', 13990, 'products/February2019/Vaya_i_Size.png', 'Детское автокресло GB Vaya i-Size Monument Black', 'Детское автокресло GB Vaya i-Size Monument Black', 'Детское автокресло GB Vaya i-Size Monument Black, купить автокресло Vaya i-size, кресло Vaya i-size', '2019-02-05 10:18:54', '2019-02-27 10:09:06', 'vaya-i-size', '1', NULL, 'seats', '<p>Автокресло гр. 0+/1 (до 18 кг., от рождения до 104 см) Автокресло победило в независимых тестах ADAC и German Design Award и соответствует стандарту безопасности ЕСЕ R-129 (i-size). В автокресле предусмотрена функция поворота на 360&deg;, которая значительно упрощает процесс установки&nbsp;автокресла в положении против и по ходу движения, а также позволяет родителям легко размещать ребенка.</p>', '[\"8\"]', NULL, NULL, NULL, 7, 'https://www.youtube.com/embed/AlafzhHHuXM', NULL),
(7, 'Люлька Cot to Go', 8625, 'products/May2019/Cot to Go Black (1)-min.png', 'Люлька Cot to Go', 'Люлька Cot to Go', NULL, '2019-02-06 11:07:26', '2019-05-03 14:31:33', 'maris-lyul-ka-dlya-novorozhdennogo', '1', NULL, 'accessories', '<div class=\"pram-item__details-left\">Возможно использование от рождения до 1 года. Большой капюшон. Твердое дно. Устанавливается (благодаря адаптерам) на рамы колясок Qbit +, Pockit +&nbsp;Вес: 3,3 кг</div>', NULL, '[\"2\",\"3\"]', 'green', 'НОВИНКА', 3, 'https://www.youtube.com/embed/qAkmMYi13Qc', NULL),
(8, 'Подстаканник Gb для автокресла', 475, 'products/February2019/11181.jpg', 'Подстаканник gb для автокресла', 'Подстаканник gb для автокресла', 'Подстаканник gb для автокресла', '2019-02-18 12:12:18', '2019-02-27 10:20:25', 'podstakannik-gb-dlya-avtokresla', '1', NULL, NULL, '<p>Подходит для использования на автокреслах Vaya i-Size и&nbsp;Elian Fix.</p>', NULL, '[\"5\"]', NULL, NULL, 3, '0', NULL),
(10, 'Автокресло Everna-Fix Гр.1/2/3', 8625, 'products/May2019/GB_19_y045_EU_LABL_Everna-Fix_harness.png', 'Автокресло Everna-Fix', 'Автокресло Everna-Fix', 'Автокресло Everna-Fix', '2019-02-19 09:57:36', '2019-05-03 16:26:25', 'avtokreslo-everna-fix', '1', NULL, NULL, '<p>(до 36 кг., от 9 мес. До 12 лет) Революционное автокресло Everna-Fix соединяет в себе надежные функции безопасности, последние научные разработки и стильный дизайн. Автокресло соответствует европейскому стандарту безопасности ЕСЕ R-44/04. Настоящее автокресло-трансформер - кресло растет вместе с ребенком по высоте и ширине, делая поездку максимально удобной и комфортной.</p>', '[\"8\"]', NULL, 'gray', 'ПРЕДЗАКАЗ', 8, 'https://www.youtube.com/embed/JMwilVrs2wk', NULL),
(11, 'Автокресло Artio Гр.0+', 2678, 'products/February2019/Artio_viertel_Monument_Black.png', 'Автокресло Artio', 'Автокресло Artio с Gold серии объединяет в себе стиль и безопасность. Автокресло соответствует стандарту ECE R-44/04. C таким автокреслом любая поездка пройдет легко и безопасно, а малышу будет удобно и комфортно.', 'Автокресло Artio, автокресла для новорожденных', '2019-02-19 11:54:54', '2019-02-20 17:58:06', 'avtokreslo-artio-gr-0-', '1', NULL, NULL, '<p>Автокресло Artio с Gold серии объединяет в себе стиль и безопасность. Автокресло соответствует стандарту ECE R-44/04. C таким автокреслом любая&nbsp;поездка пройдет легко и безопасно, а малышу будет удобно и комфортно. <br />Вес кресла: 4,7 кг&nbsp;Размеры (д х ш х в): 65 x 43 x 38-56 см</p>', NULL, NULL, NULL, NULL, 6, 'https://www.youtube.com/embed/hwkaZFbxeCg', NULL),
(12, 'Автокресло Elian Fix ГР.2/3', 6830, 'products/February2019/product-Elian-fix-Lux-Black-4228-71_uz2cqh (1).jpg', 'Автокресло Elian Fix', 'Автокресло гр. 2/3, (до 36 кг., от 3 до 12 лет) Автокресло Elian-Fix  соответствует европейскому стандарту безопасности ЕСЕ R-44/04. Автокресло-трансформер - кресло растет вместе с ребенком по высоте и ширине)', 'АвтокреслоАвтокресло Elian Fix', '2019-02-19 12:18:21', '2019-03-19 10:06:35', 'avtokreslo-elian-fix', '1', NULL, NULL, '<p>Автокресло гр. 2/3, (до 36 кг., от 3 до 12 лет) Революционное автокресло Elian-Fix с Platinum коллекции соединяет в себе функции безопасности, последние научные разработки и стильный дизайн. Автокресло соответствует европейскому стандарту безопасности ЕСЕ R-44/04. Настоящее автокресло-трансформер - кресло растет вместе с ребенком по высоте и ширине), делая поездку максимально удобной и комфортной.</p>', NULL, NULL, NULL, NULL, 9, 'https://www.youtube.com/embed/5lP6xRga9Fg', NULL),
(13, 'Люлька Maris Day Dream', 8625, 'products/February2019/GB_17.png', 'Люлька Maris Day Dream', 'Люлька Maris Day Dream', 'Люлька Maris Day Dream', '2019-02-19 12:52:47', '2019-02-19 14:37:38', 'lyul-ka-maris-day-dream', '1', NULL, NULL, '<p>Длина/ширина/высота: 83 х 45 х 52&nbsp;Вес: 4,1 кг. Подходит для использования на коляске gb Maris</p>', NULL, '[\"1\"]', NULL, NULL, 3, NULL, NULL),
(14, 'Сумка для вещей Maris Day Dream', 5299, 'products/February2019/GB_Daydream_Bag_withoutShadow_DERV_LARGE1.png', 'Сумка для вещей Maris Day Dream', 'Стильная сумка из экокожи Maris Day Dream. В комплект входит портативный термочехол для бутылочки, матрасик для пеленания и отделение для влажных вещей.', NULL, '2019-02-19 14:40:32', '2019-03-19 10:08:31', 'sumka-dlya-veshej-maris-day-dream', '1', NULL, NULL, '<p>Стильная сумка из экокожи. В комплект входит портативный термочехол для бутылочки, матрасик для пеленания и отделение для влажных вещей.</p>', NULL, '[\"1\"]', NULL, NULL, 3, '0', NULL),
(15, 'Детское покрывало Day Dream', 2090, 'products/February2019/GB_Daydream_Blanke1.png', 'Детское покрывало Day Dream', 'Мягкое одеяло в черном цвет с декоративными перьями на ободке обеспечит комфорт и тепло. Одеяло двустороннее: с одной стороны оно имеет флисовую подкладку, а с другой стороны - трикотажную. Машинная стирка.', 'Детское покрывало Day Dream, покрывало в детскую коляску', '2019-02-19 14:45:28', '2019-02-19 14:50:13', 'detskoe-pokryvalo-day-dream', '1', NULL, NULL, '<p>Мягкое одеяло в черном цвет с декоративными перьями на ободке обеспечит комфорт и тепло. Одеяло двустороннее: с одной стороны оно имеет флисовую подкладку, а с другой стороны - трикотажную. Машинная стирка.</p>', NULL, '[\"1\"]', NULL, NULL, 3, NULL, NULL),
(16, 'Чехол для ног Maris Day Dream', 4460, 'products/February2019/4444444444444.jpg', 'Чехол для ног Maris Day Dream', 'Элегантный дизайн чехла для ног Maris Day Dream подчеркивает стиль коляски Maris Day Dream . Чехол изготовлен из мягкого материала и комфортной флисовой вставки.Водоотталкивающая ткань чехла защитит малыша от любой непогоды.', NULL, '2019-02-19 14:52:42', '2019-02-19 15:06:39', 'chehol-dlya-nog-maris-day-dream', '1', NULL, NULL, '<p>&nbsp;Элегантный дизайн чехла подчеркивает стиль коляски. Чехол изготовлен из мягкого материала и комфортной флисовой вставки. Водоотталкивающая ткань чехла&nbsp;защитит малыша от любой непогоды.</p>\r\n<p>&nbsp;</p>', NULL, '[\"1\"]', NULL, NULL, 3, NULL, NULL),
(17, 'Москитная сетка  Maris Day Dream', 288, 'products/February2019/maris-mosq - Copy.jpg', 'Москитная сетка Maris Day Dream', 'Москитная сетка  Maris Day Dream защищает от насекомых. Подходит для использования с колясками gb Maris/Qbit+', 'Москитная сетка для коляски Maris Day Dream, москитная сетка на коляску купить', '2019-02-19 15:03:04', '2019-02-20 17:46:47', 'moskitnaya-setka-maris-day-dream', '1', NULL, NULL, '<p>Защищает от насекомых. Подходит для использования с колясками gb Maris/Qbit+</p>', NULL, '[\"1\",\"3\"]', NULL, NULL, 3, '0', NULL),
(18, 'Сумка для коляски Pockit', 660, 'products/May2019/Travel Bag.png', 'Сумка для коляски Pockit', 'Сумка для коляски Pockit удобная и легкая сумка для транспортировки коляски  Pockit.', 'Сумка для коляски Pockit, купить сумку для коляски, сумка для коляски Pockit купить', '2019-02-19 15:14:17', '2019-05-03 14:24:26', 'sumka-dlya-kolyaski-pockit', '1', NULL, NULL, '<p>Удобная и легкая сумка для транспортировки коляски.</p>', NULL, '[\"2\"]', NULL, NULL, 3, '0', NULL),
(19, 'Адаптеры для автокресла gb и CYBEX', 720, 'products/February2019/product-Pockit_-Monument-Black-Adapters-for-infant-car-seats-157-4255-20_rn2y0b1.png', 'Адаптеры для автокресла gb и CYBEX', 'При использовании адаптеров возможна установка автокресел gb и CYBEX, а также люльки gb Cot to Go с коляской Pockit+.', 'Адаптеры для автокресла gb и CYBEX, установка автокресла на шасси,', '2019-02-19 15:18:38', '2019-02-20 17:25:54', 'adaptery-dlya-avtokresla', '1', NULL, NULL, '<p>При использовании адаптеров возможна установка автокресел gb и CYBEX, а также люльки gb Cot to Go с коляской Pockit+.</p>', NULL, '[\"2\",\"4\",\"7\"]', NULL, NULL, 3, NULL, NULL),
(20, 'Дождевик для коляски Qbit+', 660, 'products/February2019/gb_GL_QBit_Raincover_POS.png', 'Дождевик для коляски Qbit+', 'Дождевик для коляски Qbit+.Защита во время прогулок в коляске от ветра, дождя и других неблагоприятных погодных условий.', 'Дождевик для коляски Qbit+', '2019-02-19 16:23:35', '2019-02-19 16:23:35', 'dozhdevik-dlya-kolyaski-qbit-', '1', NULL, NULL, '<p>Защита во время прогулок в коляске от ветра, дождя и других неблагоприятных погодных условий.</p>', NULL, '[\"2\"]', NULL, NULL, 3, NULL, NULL),
(21, 'Подстаканник Gb', 288, 'products/February2019/gb_GL_Cupholder.png', 'Подстаканник gb для коляски', 'Подстаканник gb для коляски подходит для использования ко всем коляскам GB', 'Подстаканник gb для коляски, купить подстаканник для коляски, подстаканник для коляски', '2019-02-20 17:15:48', '2019-03-27 12:01:28', 'podstakannik-gb-dlya-avtokresla', '1', NULL, NULL, '<p>Подстаканник Gb для коляски подходит для использования ко всем коляскам GB, кроме Pockit.</p>', NULL, '[\"2\"]', NULL, NULL, 3, '0', NULL),
(22, 'Адаптеры для автокресла Gb', 550, 'products/May2019/gb_GL_QBit_Adapter11.png', 'Адаптеры для автокресла gb', 'При использовании адаптеров для автокресла gb возможна установка автокресел gb и CYBEX, а также люлька gb Cot to Go с коляской Qbit+.', 'Адаптеры для автокресла gb, купить адаптеры для автокресла,', '2019-02-20 17:23:57', '2019-05-03 16:45:20', 'adaptery-dlya-avtokresla-gb', '1', NULL, NULL, '<p>При использовании адаптеров для автокресла gb возможна установка автокресел gb и CYBEX, а также люлька gb Cot to Go с коляской Qbit+.</p>', NULL, '[\"2\"]', NULL, NULL, 3, '0', NULL),
(23, 'Зонтик для коляски Gb', 1152, 'products/May2019/gb_GL_Parasol.png', 'Зонтик для коляски gb', 'Зонтик gb kегко фиксируется и регулируется на коляске, имеет функцию защиты от солнца UF 50+. Подходит для использования на всех колясках gb, кроме Pockit.', 'Зонтик для коляски gb, купить зонтик для детской коляски, зонтик для коляски', '2019-02-20 17:32:50', '2019-05-03 13:01:28', 'zontik-dlya-kolyaski-gb', '1', NULL, NULL, '<p>Зонтик gb kегко фиксируется и регулируется на коляске, имеет функцию защиты от солнца UF 50+. Подходит для использования на всех колясках gb, кроме Pockit.</p>', NULL, '[\"2\"]', NULL, NULL, 3, '0', NULL),
(24, 'Чехол для ног Gb', 1991, 'products/February2019/gb_GL_Footmuff_black.png', 'Чехол для ног GB', 'Флисовая подкладка чехла для ног gb обеспечивает тепло при температуре -10°.Практичная в уходе ткань.', 'Чехол для ног GB, чехол для ног на коляску GB,', '2019-02-20 17:43:09', '2019-02-27 10:18:21', 'chehol-dlya-nog-gb', '1', NULL, NULL, '<p>Флисовая подкладка чехла для ног gb обеспечивает тепло при температуре -10&deg;.Практичная в уходе ткань.</p>', NULL, '[\"2\"]', NULL, NULL, 3, '0', NULL),
(25, 'Pockit+17', 3070, 'products/February2019/GB_17_prod_y045_stro_EU_CABL_GBPockit+ComponentLine_0002_fi_Derivate_web_large(1).png', 'Pockit+ 2017', 'Коляска для путешествий от 6 месяцев до 17 кг (приблизительно 4 года) Коляска, которая идеально походит как для путешествий, так и для повседневных прогулок с детьми. Допускается на борт самолета.', NULL, '2019-02-20 18:49:43', '2019-04-24 12:59:36', 'Pockit+17', '1', NULL, NULL, '<p>12</p>', '[\"7\",\"18\"]', '[\"2\",\"4\",\"5\",\"11\",\"18\",\"26\"]', NULL, NULL, 4, '0', 3),
(26, 'Pockit', 1999, 'products/February2019/pockit3.png', 'Pockit коляска', 'Pockit коляска которая идеально походит как для путешествий, так и для повседневных прогулок с детьми. Допускается на борт самолета.', 'коляска покит, купить Pockit, коляска для прогулок  Pockit', '2019-02-20 18:50:55', '2019-03-12 07:43:19', 'Pockit', '1', NULL, NULL, '<p>Pockit коляска</p>', '[\"18\",\"23\",\"24\"]', '[\"18\"]', NULL, 'РАСПРОДАЖА', 4, '0', 1),
(27, 'Pockit Air All-Terrain', 4550, 'products/May2019/GB_19_y045_EU_NIBL_PockitAir_All-Terrain_screen_HD.png', 'Детская прогулочная коляска Pockit Air All-Terrain купить онлайн у официального представителя в Украине', 'Детская коляска Pockit Air All-Terrain', 'коляска покит, купить Pockit, коляска для прогулок  Pockit', '2019-04-30 02:13:11', '2019-05-08 11:38:05', 'pockit-air', '0', NULL, NULL, NULL, '[\"18\",\"21\",\"22\",\"23\",\"24\"]', '[\"2\",\"25\",\"28\",\"29\"]', NULL, NULL, 4, '0', 4),
(28, 'Pockit+ All-Terrain', 4990, 'products/May2019/GB_19_y045_EU_LABL_Pockit+_All-Terrain_screen_HD (1)-min.png', 'Детская коляска Pockit+ All-Terrain купить онлайн у официального представителя в Украине', 'Детская коляска Pockit+ All-Terrain', 'коляска покит, купить Pockit, коляска для прогулок  Pockit', '2019-04-30 02:15:03', '2019-05-03 16:16:37', 'pockit-all-terrain', '0', NULL, NULL, NULL, '[\"7\"]', '[\"25\",\"26\",\"29\"]', NULL, NULL, 4, '0', 5),
(29, 'Pockit+ All-City', 5444, 'products/May2019/GB_19_y045_EU_VEBL_Pockit+city_screen_HD.png', 'Детская коляска Pockit+ All-City купить у официального представителя в Украине', 'Детская коляска Pockit+ All-City купить у официального представителя в Украине', 'коляска покит, купить Pockit, коляска для прогулок  Pockit', '2019-04-30 02:16:28', '2019-05-06 15:37:35', 'pockit-all-city', '0', NULL, NULL, NULL, '[\"7\",\"18\",\"19\",\"23\",\"24\"]', '[\"25\",\"26\",\"27\",\"28\"]', NULL, NULL, 4, '0', 6),
(30, 'Qbit+ All-Terrain', 6970, 'products/May2019/GB_19_y045_EU_NIBL_Qbit+_All-Terrain_screen_HD.png', 'Qbit+ All-Terrain', 'детская коляска Qbit+ All-Terrain, купить онлайн у официального представителя в Украине', 'коляска Qbit, коляска кубит, коляска кубит купить', '2019-04-30 02:18:01', '2019-05-06 09:40:48', 'qbit-all-terrain', '0', NULL, NULL, NULL, '[\"7\"]', '[\"19\",\"20\",\"22\"]', NULL, NULL, 4, '0', 8),
(31, 'Qbit+ All-City', 6970, 'products/May2019/GB_19_y045_EU_VEBL_Qbit+city_bumperbar_7296_new_screen_HD-min.png', 'Детская коляска Qbit+ All-City купить онлайн у официального представителя в Украине', 'Детская коляска Qbit+ All-City', 'коляска Qbit, коляска кубит, коляска кубит купить', '2019-04-30 02:18:26', '2019-05-06 15:35:55', 'qbit-all-city', '0', NULL, NULL, NULL, '[\"7\",\"16\",\"19\",\"20\",\"23\",\"24\"]', '[\"3\",\"30\",\"32\"]', NULL, NULL, 4, '0', 9),
(32, 'Qbit+  All-City Fashion', 8960, 'products/May2019/GB_19_y045_EU_RORE_Qbit+city_fe_bumperbar_7382_noshadow_screen_HD.png', 'Детская коляска Qbit+  All-City Fashion купить онлайн у официального представителя в Украине', 'Детская коляска Qbit+  All-City Fashion', 'Детская коляска Qbit, коляска кубит, прогулочная коляска кубит', '2019-04-30 02:18:49', '2019-05-06 15:34:49', 'qbit-all-city-fashion', '0', NULL, NULL, NULL, '[\"7\",\"19\",\"20\",\"21\",\"23\",\"24\"]', '[\"3\",\"30\",\"31\"]', NULL, NULL, 4, '0', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `product_functions`
--

CREATE TABLE `product_functions` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_functions`
--

INSERT INTO `product_functions` (`id`, `image`, `title`, `body`, `product_id`) VALUES
(1, 'product-functions/February2019/Idan_Plus_Headrest1.png', 'Защита, которая растёт вместе с ребёнком', 'Регулируемый по высоте подголовник (11 положений) обеспечит комфорт ребёнку. Направляющие ремней безопасности позволяют легко разместить ребёнка в\r\nавтокресле. Вкладыш для новорожденного обеспечивает максимально горизонтальное положение.', 4),
(2, 'product-functions/February2019/Idan_Travelsystem_Maris1.png', 'Система для путешествий', 'С помощью дополнительных адаптеров автокресло Idan можно установить на шасси. Устанавливается (благодаря адаптерам) на рамы колясок gb, и CYBEX', 4),
(3, 'product-functions/February2019/Idan_Plus_sun_canopy.png', 'Регулируемый капюшон', 'Большой капюшон защитит ребёнка от солнечных лучей, ветра, дождя и других внешних факторов.', 4),
(4, 'product-functions/February2019/product-Vaya-i-Size-Lux-Black-Integrated-Linear-SideImpact-Protection-LSP-System-5334-5340-71_ug6mhn-min.png', 'Система боковой защиты L.S.P.', 'Обеспечит дополнительную защиту во время бокового столкновения.', 5),
(5, 'product-functions/February2019/product-Vaya-i-Size-Lux-Black-Energyabsorbing-shell-4241-5340-71_tih5vu-min.png', 'Энергопоглощающий корпус', 'Поглощает силу удара и направляет ее в противоположную сторону от ребёнка.', 5),
(6, 'product-functions/February2019/product-Vaya-i-Size-Lux-Black-12position-height-adjustable-headrest-5335-5340-71_c1xx2n-min.png', 'Регулируемый подголовник', '12 положений регулировки подголовника по высоте', 5),
(7, 'product-functions/February2019/product-Vaya-i-Size-Lux-Black-Magnetic-Harness-Clips-5338-5340-71_y6qtax-min.png', 'Магнитные застёжки', 'Практичность и лёгкость в использовании ремней безопасности', 5),
(8, 'product-functions/February2019/product-Vaya-i-Size-Lux-Black-OneClick-ISOFIX-Installation-5613-5340-71_nmufvy-min.png', 'Система крепления ISOFIX', 'Правильная и легкая в использовании фиксация с доступной индикацией', 5),
(9, 'product-functions/February2019/product-Vaya-i-Size-Lux-Black-Onehand-recline-and-rotation-function-5336-5340-71_x2pju2-min.png', 'Поворот на 360о, функция регулировки положения автокресла', 'Против хода движения – 5 положений, по ходу движения – 3 положения', 5),
(10, 'product-functions/February2019/product-Vaya-i-Size-Lux-Black-5-point-integrated-harness--5337-5340-71_ylkgf5-min.png', 'Пятиточечные ремни безопасности и мягкие накладки с силиконовыми вставками', 'Лёгкость фиксации ремней безопасности', 5),
(11, 'product-functions/February2019/Idan_Plus_lsp.png', 'Система боковой защиты L.S.P.', 'Обеспечит дополнительную защиту во время бокового столкновения', 4),
(12, 'product-functions/February2019/Idan_with_Base.png', 'Base-fix', 'Практичная в использовании база с функцией безопасной установки. Для фиксации автокресла в машине достаточно закрепить его на базе, в которой редусмотрена дополнительная опора – опорная ножка', 4),
(13, 'product-functions/February2019/1111.jpg', 'Функция регулировки высоты и ширины', 'Jт Группы 1 (от 9 кг) до группы 2/3 (до 36 кг). Автокресло растёт вместе с ребёнком, 12 положений', 10),
(14, 'product-functions/February2019/GB_19_y045_EU_NIBL_Everna-Fix_harness.jpg', 'Запатентованный регулируемый подголовник', 'Адаптируется под положения головы ребёнка, 3 положения', 10),
(15, 'product-functions/February2019/Everna_Details_72732_RGB_screen_HD.jpg', 'Система боковой защиты L.S.P.', 'В случае аварии боковая защита направляет голову ребёнка в безопасное положение', 10),
(16, 'product-functions/February2019/_RGB.jpg', 'Енергопоглощающий корпус', 'Поглощает силу удара и предоставляет дополнительную защиту в случае столкновения', 10),
(17, 'product-functions/February2019/33.jpg', 'Откидная спинка', 'Благодаря откидной спинке автокресло идеально подстраивается под сидение автомобиля, а также гарантирует комфорт вашему ребенку', 10),
(18, 'product-functions/February2019/Everna_Details_72743_RGB (1).jpg', 'Дополнительный ремень безопасности', 'Исключает случаи неправильной фиксации', 10),
(19, 'product-functions/February2019/Everna_Details_72755_RGB.jpg', '5-точечные ремни безопасности', 'Надежная фиксация ребенка', 10),
(20, 'product-functions/February2019/Beli_4_Air_Travelsystem_Artio.png', 'Система для путешествий', 'С помощью дополнительных адаптеров автокресло можно установить на  шасси. Устанавливается (благодаря адаптерам) на рамы колясок gb и CYBEX', 11),
(21, 'product-functions/February2019/Artio_Animation_headrest.png', 'Защита, которая растёт вместе с ребёнком', 'Регулируемый по высоте подголовник (11 положений) обеспечит комфорт ребёнку. Направляющие ремней безопасности позволяют легко разместить ребёнка в автокресле. Вкладыш для новорожденного обеспечивает\r\nмаксимально горизонтальное положение.', 11),
(22, 'product-functions/February2019/Artio_Animation_sun_canopy.png', 'Регулируемый капюшон', 'Большой капюшон защитит ребёнка от солнечных лучей, ветра, дождя и других внешних факторов.', 11),
(23, 'product-functions/February2019/Artio_side_picto.png', 'Эргономическое положение спинки', 'Комфорт и здоровое развитие', 11),
(24, 'product-functions/February2019/Artio_Base.png', 'Base-fix', 'Практичная в использовании база с функцией безопасной установки. Для фиксации автокресла в машине достаточно закрепить его на базе, в которой предусмотрена дополнительная опора – опорная ножка', 11),
(25, 'product-functions/February2019/Artio_LSP_System.png', 'Система боковой защиты L.S.P.', 'Обеспечит дополнительную защиту во время бокового столкновения', 11),
(26, 'product-functions/February2019/936_936_a2f5242ea299d075e84765e776ff056d.png', 'Запатентованный регулируемый подголовник', 'Регулируемый подголовник. Адаптируется под положения головы ребёнка, 3 положения', 12),
(27, 'product-functions/February2019/product-Elian-fix-Lux-Black-AUTOMATIC-HEIGHT-AND-WIDTH-ADJUSTMENT-12-POSITIONS-4220-4228-71_glbpjs.png', 'Функция регулировки высоты и ширины', 'Автокресло растёт вместе с ребёнком, 12 положений', 12),
(28, 'product-functions/February2019/product-Elian-fix-Lux-Black-AIR-VENTILATION-SYSTEM-4221-4228-71_rasc50.png', 'Система вентиляции воздуха', 'Гарантирует оптимальную температуру тела ребёнка во время поездки', 12),
(29, 'product-functions/February2019/product-Elian-fix-Lux-Black-ENERGYABSORBING-SHELL-4226-4228-71_nltomd.png', 'Энергопоглощающий корпус', 'Поглощает силу удара и направляет ее в противоположную сторону от ребёнка', 12),
(30, 'product-functions/February2019/product-Elian-fix-Lux-Black-OPTIMISED-LINEAR-SIDEIMPACT-PROTECTION-LSP-SYSTEM-PLUS-4222-4228-71_mpl1fn.png', 'Система боковой защиты L.S.P.', 'В случае аварии боковая защита направляет голову ребёнка в безопасное положение', 12),
(31, 'product-functions/February2019/product-Elian-fix-Lux-Black-RECLINING-BACKREST-4223-4228-71_c2s8e6 (1).png', 'Откидная спинка', 'Благодаря откидной спинке автокресло идеально подстраивается под сидение автомобиля', 12),
(32, 'product-functions/February2019/gt-617000017-gb-elian-fix-car-seat-monument-black-15280173932.jpg', 'Система крепления ISOFIX', 'Правильная и легкая фиксация в использовании', 12),
(33, 'product-functions/February2019/product-Elian-fix-Lux-Black-GB-SAFETY-PADS-4225-4228-71_f5e8sr.png', 'Подушки безопасности GB', 'Предоставляет дополнительную защиту в случае бокового столкновения', 12),
(34, 'product-functions/February2019/GB_17_prod_ y045_case_EU_BLSW_IdanDaydreamMarisFrame_0003 32_DERV_LARGE.jpg', 'Возможность использования с автокреслом Гр 0+', NULL, 1),
(35, 'product-functions/February2019/universalnye-koliaski_5_1507118965.jpg', 'Компактность и устойчивость в сложенном виде', NULL, 1),
(36, 'product-functions/February2019/GB_17_prod_y090_stro_EU_BLSW_GBMarisLine_Cotonframe_0002_xxx_DERV_LARGE.jpg', 'Возможность использования с люлькой', NULL, 1),
(37, 'product-functions/March2019/a_1.png', 'Компактность в сложенном виде', '20 x 32 x 38\r\nДопускается на борт самолёта', 25),
(38, 'product-functions/March2019/a_2.png', 'Возможность использования с автокреслом', NULL, 25),
(39, 'product-functions/March2019/Cot to Go Black (1).png', 'Возможность использования с люлькой COT TO GO', NULL, 25),
(40, 'product-functions/May2019/GB_18_y045_US_CHRE_QbitPlus_bumperbar_8142_screen_HD111-min.jpg', 'Двойные колеса с амортизацией', NULL, 30),
(41, 'product-functions/May2019/GB_19_y090_EU_RORE_Qbit+_All-Terrain_CHRE_CotToGo_screen_HD (1)-min.jpg', 'Возможность использования с люлькой GB COT', NULL, 30),
(42, 'product-functions/May2019/GB_19_y090_EU_RORE_Qbit+_All-Terrain_folded_stored_screen_HD-min.jpg', 'Компактность в сложенном виде, удобно брать с собой в поездки', NULL, 30),
(43, 'product-functions/May2019/GB_19_y090_EU_RORE_Qbit+_All-Terrain_Canopy_fullreclined_screen_HD-min.jpg', 'Полностью горизонтальное положение спинки', NULL, 30),
(44, 'product-functions/May2019/GB_19_y000_EU_RORE_Pockit+city_compactfold_screen_HD-min.jpg', 'Компактность в сложенном виде', NULL, 29),
(45, 'product-functions/May2019/GB_19_y090_US_RORE_Pockit+city_EU_Adapter_CotToGo_screen_HD-min.jpg', 'Возможность использования с люлькой COT TO GO', NULL, 29),
(46, 'product-functions/May2019/GB_19_y090_Pockit+city_front_wheels_suspension_7266_screen_HD-min.jpg', 'Одинарные колеса', NULL, 29),
(47, 'product-functions/May2019/GB_19_prod_y090_EU_RORE_GBPockit+cityComponentLine_recline_0002_screen_HD-min.jpg', 'Настройка положения спинки', NULL, 29),
(48, 'product-functions/May2019/ GB_18_y045_EU_RORE_Qbit+city_fe_mesh_7401_screen_HD-min.jpg', 'Окошко из сеточки', NULL, 32),
(50, 'product-functions/May2019/GB_19_y090_US_RORE_Qbit+city_fe_CotToGo_7409_screen_HD-min.jpg', 'Возможность использования с люлькой COT TO GO', NULL, 32),
(51, 'product-functions/May2019/GB_19_y000_EU_Qbit+city_all_wheels_suspension_7341_screen_HD-min.jpg', 'Одинарные колеса', NULL, 31),
(52, 'product-functions/May2019/GB_19_y090_US_RORE_Qbit+city_CotToGo_7326_screen_HD-min.jpg', 'Возможность использования с люлькой COT TO GO', NULL, 31),
(53, 'product-functions/May2019/GB_19_y090_EU_RORE_Qbit+city_folded_7340_horizontal_screen_HD-min.jpg', 'Компактность в сложенном виде', NULL, 31),
(54, 'product-functions/May2019/GB_19_y045_EU_RORE_Pockit+_All-Terrain_screen_HD11111-min.jpg', 'Двойные колеса', NULL, 28),
(55, 'product-functions/May2019/GB_19_y000_EU_RORE_Pockit+_All-Terrain_fold_screen_HD-min.jpg', 'Компактность в сложенном виде', NULL, 28),
(56, 'product-functions/May2019/GB_19_y090_EU_RORE_Pockit+_All-Terrain_CHRE_CotToGo_screen_HD-min.jpg', 'Возможность использования с люлькой COT TO GO', NULL, 28),
(57, 'product-functions/May2019/GB_19_y045_EU_VEBL_Pockit+_All-Terrain_screen_HD.jpg', 'Двойные колеса', NULL, 27),
(58, 'product-functions/May2019/GB_19_y000_EU_RORE_PockitAir_All-Terrain_screen_HD (1).jpg', 'Дышащий сетчатый материал', NULL, 27),
(59, 'product-functions/May2019/GB_19_y000_EU_RORE_PockitAir_All-Terrain_compactfold_screen_HD.jpg', 'Компактность в сложенном виде', NULL, 27),
(60, 'product-functions/May2019/ GB_18_y045_EU_RORE_Qbit+city_fe_mesh_7401_screen_HD-min1.jpg', 'Одинарные колеса', NULL, 32),
(61, 'product-functions/May2019/GB_18_y090_EU_RORE_Qbit+city_fe_full recline_7395_screen_HD-min.jpg', 'Полностью горизонтальное положение спинки', NULL, 32),
(62, 'product-functions/May2019/GB_19_y090_EU_RORE_Qbit+city_full recline_7312_new_screen_HD (1)-min.jpg', 'Полностью горизонтальное положение спинки', NULL, 31),
(63, 'product-functions/May2019/GB_19_y090_EU_RORE_Pockit+_All-Terrain_recline3_screen_HD-min.jpg', 'Настройка положения спинки', NULL, 28);

-- --------------------------------------------------------

--
-- Структура таблицы `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_images`
--

INSERT INTO `product_images` (`id`, `color`, `image`, `product_id`) VALUES
(3, 'green', 'product-images/February2019/green.png', 3),
(4, 'blue', 'product-images/February2019/blue.png', 3),
(5, 'gray', 'product-images/February2019/gray.png', 3),
(6, 'black', 'product-images/February2019/black.png', 3),
(7, 'green', 'product-images/February2019/Pockit+.png', 2),
(8, 'red', 'product-images/February2019/Pockit+_red.png', 2),
(9, 'red', 'product-images/February2019/Cot to Go Cherry Red.png', 7),
(11, 'gray', 'product-images/February2019/Cot to Go Solver Fox Grey.png', 7),
(23, 'green', 'product-images/February2019/product-Vaya-i-Size-Laguna-Blue-5340-7327_ltgbdu-min.jpg', 5),
(24, 'gray', 'product-images/February2019/product-Vaya-i-Size-Silverfox-Grey-5340-7330_dtt9nu-min.jpg', 5),
(25, 'red', 'product-images/February2019/product-Vaya-i-Size-Pomergranite-Red-5340-7329_c1o7z2-min.jpg', 5),
(26, 'black', 'product-images/February2019/product-Vaya-i-Size-Lux-Black-5340-71_w96eag-min.jpg', 5),
(27, 'black', 'product-images/February2019/product-Vaya-i-Size-Lux-Black-5340-71_w96eag-min1.jpg', 5),
(28, 'black', 'product-images/February2019/GB_17_prod_y045_case_EU_ BLSW_GBIdanDaydream_without_shadow_DERV_LARGE-min.jpg', 4),
(29, 'black', 'product-images/February2019/Cot to Go Black (1).png', 7),
(30, 'gray', 'product-images/March2019/GB_19_y045_EU_LOGR_Everna-Fix_harness-min.png', 10),
(31, 'green', 'product-images/February2019/GB_19_y045_EU_LABL_Everna-Fix_harness.jpg', 10),
(32, 'red', 'product-images/February2019/GB_19_y045_EU_RORE_Everna-Fix_harness.jpg', 10),
(33, 'blue', 'product-images/February2019/GB_19_y045_EU_NIBL_Everna-Fix_harness.jpg', 10),
(34, 'black', 'product-images/February2019/GB_19_y045_EU_VEBL_Everna-Fix_harness.jpg', 10),
(35, 'pink', 'product-images/February2019/GB_19_y045_EU_SWPI_Everna-Fix_harness.jpg', 10),
(36, 'green', 'product-images/February2019/product-Elian-Fix-Laguna-Blue-4228-7327_mf9a7q.jpg', 12),
(37, 'red', 'product-images/February2019/product-Elian-Fix-Pomergranite-Red-4228-7329_hnhwvw.jpg', 12),
(38, 'black', 'product-images/February2019/product-Elian-fix-Lux-Black-4228-71_uz2cqh (1).jpg', 12),
(39, 'blue', 'product-images/February2019/product-Elian-Fix-Sapphire-Blue-4228-7328_r5welc.jpg', 12),
(40, 'black', 'product-images/February2019/01_GB_18_y045_EU_SABL_Pockit+_14981_DERV_HQ.jpg', 2),
(41, 'blue', 'product-images/February2019/01_GB_18_y045_EU_SABLU_Pockit+_14981_DERV_HQ.jpg', 2),
(42, 'gray', 'product-images/February2019/01_GB_18_y045_EU_SIFOGR_Pockit+_14981_DERV_HQ.jpg', 2),
(44, 'red', 'product-images/February2019/GB_18_y045_EU_CHRE_QbitPlus_bumperbar_DERV_HQ (1).jpg', 3),
(45, 'blue', 'product-images/February2019/Cot to Go Sapphire Blue1.png', 7),
(46, 'green', 'product-images/February2019/Cot to Go Laguna Blue.png', 7),
(47, 'red', 'product-images/February2019/gb_GL_Footmuff_red.png', 24),
(48, 'black', 'product-images/February2019/gb_GL_Footmuff_black.png', 24),
(51, 'blue', 'product-images/February2019/GB_17_prod_y045_stro_EU_SEPB_GBPockit+ComponentLine_0002_fi_Derivate_web_large(1).png', 25),
(52, 'red', 'product-images/February2019/GB_17_prod_y045_stro_EU_POPI_GBPockit+ComponentLine_0002_fi_Derivate_web_large(1).png', 25),
(53, 'green', 'product-images/February2019/Pockit_viertel_Capri_Blue.png', 26),
(54, 'red', 'product-images/February2019/Pockit_viertel_Dragonfire_Red.png', 26),
(55, 'brown', 'product-images/February2019/Pockit_viertel_Lizard_Khaki.png', 26),
(56, 'blue', 'product-images/February2019/Pockit_viertel_Sea_Port_Blue.png', 26),
(57, 'black', 'product-images/February2019/Pockit_viertel_Monument_Black.png', 26),
(58, 'green', 'product-images/May2019/GB_19_y045_EU_LABL_Qbit+_All-Terrain_screen_HD-min.png', 30),
(59, 'red', 'product-images/May2019/GB_18_y045_US_CHRE_QbitPlus_bumperbar_8142_screen_HD-min.png', 30),
(60, 'blue', 'product-images/May2019/GB_19_y045_EU_NIBL_Qbit+_All-Terrain_screen_HD-min.png', 30),
(61, 'black', 'product-images/May2019/GB_19_y045_EU_VEBL_Qbit+_All-Terrain_screen_HD-min.png', 30),
(62, 'brown', 'product-images/May2019/GB_19_y045_EU_VABE_Pockit+city_screen_HD-min.png', 29),
(63, 'black', 'product-images/May2019/GB_19_y045_EU_VEBL_Pockit+city_screen_HD-min.png', 29),
(64, 'red', 'product-images/May2019/GB_19_y045_EU_RORE_Pockit+city_screen_HD-min.png', 29),
(65, 'blue', 'product-images/May2019/GB_19_y045_EU_NIBL_Pockit+city_screen_HD-min.png', 29),
(66, 'brown', 'product-images/May2019/GB_19_y045_EU_VABE_Qbit+city_fe_bumperbar_7382_screen_HD-min.png', 32),
(67, 'red', 'product-images/May2019/GB_19_y045_EU_RORE_Qbit+city_fe_bumperbar_7382_noshadow_screen_HD-min.png', 32),
(68, 'black', 'product-images/May2019/Qbit+city_FASION-min.png', 32),
(69, 'blue', 'product-images/May2019/GB_19_y045_EU_NIBL_Qbit+city_fe_bumperbar_7382_screen_HD-min.png', 32),
(70, 'green', 'product-images/May2019/GB_19_y045_EU_LABL_Qbit+city_bumperbar_7296_new_screen_HD-min1.png', 31),
(71, 'blue', 'product-images/May2019/GB_19_y045_EU_NIBL_Qbit+city_bumperbar_7296_new_screen_HD-min.png', 31),
(72, 'red', 'product-images/May2019/GB_19_y045_EU_RORE_Qbit+city_bumperbar_7296_noshadow_screen_HD-min.png', 31),
(73, 'black', 'product-images/May2019/GB_19_y045_EU_VEBL_Qbit+city_bumperbar_7296_new_screen_HD-min.png', 31),
(74, 'green', 'product-images/May2019/GB_19_y045_EU_LABL_Pockit+_All-Terrain_screen_HD (1)-min.png', 28),
(75, 'red', 'product-images/May2019/GB_19_y045_EU_RORE_Pockit+_All-Terrain_screen_HD-min.png', 28),
(76, 'blue', 'product-images/May2019/GB_19_y045_EU_NIBL_Pockit+_All-Terrain_screen_HD (1)-min.png', 28),
(77, 'black', 'product-images/May2019/GB_19_y045_EU_VEBL_Pockit+_All-Terrain_screen_HD-min.png', 28),
(78, 'blue', 'product-images/May2019/GB_19_y045_EU_NIBL_PockitAir_All-Terrain_screen_HD.png', 27),
(79, 'red', 'product-images/May2019/GB_19_y045_EU_RORE_PockitAir_All-Terrain_screen_HD.png', 27),
(80, 'black', 'product-images/May2019/GB_19_y045_EU_VEBL_PockitAir_All-Terrain_screen_HD.png', 27),
(81, 'black', 'product-images/May2019/ Pockit+.png', 25),
(82, 'green', 'product-images/May2019/ Pockit+1.png', 25);

-- --------------------------------------------------------

--
-- Структура таблицы `product_params`
--

CREATE TABLE `product_params` (
  `product_id` int(11) DEFAULT NULL,
  `accessory_id` int(11) DEFAULT NULL,
  `accompanying_good_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_params`
--

INSERT INTO `product_params` (`product_id`, `accessory_id`, `accompanying_good_id`) VALUES
(7, NULL, 2),
(7, NULL, 3),
(4, NULL, 1),
(5, 8, NULL),
(10, 8, NULL),
(1, NULL, 4),
(1, NULL, 13),
(1, 14, NULL),
(1, 15, NULL),
(1, 16, NULL),
(1, 17, NULL),
(2, 7, NULL),
(2, NULL, 4),
(2, NULL, 11),
(1, 8, NULL),
(1, 13, NULL),
(25, 7, NULL),
(25, 18, NULL),
(25, NULL, 4),
(25, NULL, 5),
(25, NULL, 11),
(26, 18, NULL),
(26, 23, NULL),
(26, 24, NULL),
(8, NULL, 5),
(2, NULL, 25),
(2, NULL, 26),
(26, NULL, 18),
(25, NULL, 18),
(25, NULL, 26),
(3, NULL, 11),
(3, 7, NULL),
(3, NULL, 19),
(3, NULL, 20),
(2, 8, NULL),
(2, 18, NULL),
(2, 19, NULL),
(25, NULL, 2),
(30, 7, NULL),
(30, NULL, 19),
(30, NULL, 20),
(30, NULL, 22),
(29, 7, NULL),
(29, 18, NULL),
(29, 19, NULL),
(29, NULL, 25),
(29, NULL, 26),
(32, 7, NULL),
(32, 19, NULL),
(32, 20, NULL),
(32, 21, NULL),
(32, NULL, 3),
(31, 7, NULL),
(31, 19, NULL),
(31, 20, NULL),
(31, NULL, 3),
(28, 7, NULL),
(28, NULL, 25),
(28, NULL, 26),
(28, NULL, 29),
(27, 18, NULL),
(27, 21, NULL),
(27, 22, NULL),
(27, 23, NULL),
(27, 24, NULL),
(27, NULL, 2),
(27, NULL, 25),
(27, NULL, 28),
(27, NULL, 29),
(32, 23, NULL),
(32, 24, NULL),
(32, NULL, 30),
(32, NULL, 31),
(31, 16, NULL),
(31, 23, NULL),
(31, 24, NULL),
(31, NULL, 30),
(31, NULL, 32),
(29, 23, NULL),
(29, 24, NULL),
(29, NULL, 27),
(29, NULL, 28);

-- --------------------------------------------------------

--
-- Структура таблицы `product_videos`
--

CREATE TABLE `product_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_videos`
--

INSERT INTO `product_videos` (`id`, `name`, `video`, `product_id`) VALUES
(1, 'Видео1', 'https://www.youtube.com/embed/-NmXZtpPN2Y', 1),
(2, 'Видео2', 'https://www.youtube.com/embed/Q7H4f-QoMZQ', 1),
(3, 'Видео3', 'https://www.youtube.com/embed/bgoO5Y9TQEI', 1),
(4, 'Видео1', 'https://www.youtube.com/embed/NMpvAy40gvI', 3),
(5, 'Видео2', 'https://www.youtube.com/embed/8jAzDy3d3qg', 3),
(6, 'Видео1', 'https://www.youtube.com/embed/Wv0auB2vXHA', 2),
(7, 'Видео2', 'https://www.youtube.com/embed/qAkmMYi13Qc', 2),
(8, 'Видео1', 'https://www.youtube.com/embed/Wv0auB2vXHA', 26),
(9, 'Видео1', 'https://www.youtube.com/embed/qAkmMYi13Qc', 25),
(10, NULL, 'https://www.youtube.com/embed/Cdn_UL_q438', 30),
(11, NULL, 'https://www.youtube.com/embed/NMpvAy40gvI', 30),
(12, NULL, 'https://www.youtube.com/embed/8jAzDy3d3qg', 30),
(13, NULL, 'https://www.youtube.com/embed/Wv0auB2vXHA', 29),
(14, NULL, 'https://www.youtube.com/embed/Cdn_UL_q438', 32),
(15, NULL, 'https://www.youtube.com/embed/NMpvAy40gvI', 32),
(16, NULL, 'https://www.youtube.com/embed/8jAzDy3d3qg', 32),
(17, NULL, 'https://www.youtube.com/embed/Cdn_UL_q438', 31),
(18, NULL, 'https://www.youtube.com/embed/NMpvAy40gvI', 31),
(19, NULL, 'https://www.youtube.com/embed/8jAzDy3d3qg', 31),
(20, NULL, 'https://www.youtube.com/embed/qYe7OO0MZKE', 28),
(21, NULL, 'https://www.youtube.com/embed/Wv0auB2vXHA', 27),
(22, NULL, 'https://www.youtube.com/embed/BYvli0ILdKM', 28);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-01-24 17:10:19', '2019-01-24 17:10:19'),
(2, 'user', 'Normal User', '2019-01-24 17:10:19', '2019-01-24 17:10:19');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$EvrWs9tASYCKuDo1OcxKU.vpOFUbyqaW6s12e6X6snVPd8FF6a8R.', 'KWLZWnAxo4ERIXcEulYV8fxHOyq7C8sPconvWYCFmwDfeLR4nnDX78jHPLY9', '2019-01-24 15:12:21', '2019-01-24 15:12:21'),
(2, 2, 'gb', 'gb@admin.com', 'users/default.png', NULL, '$2y$10$EvrWs9tASYCKuDo1OcxKU.vpOFUbyqaW6s12e6X6snVPd8FF6a8R.', 'pBvHoCSUxNgX0FzSvEfMim3eVniZbmArWc7bfCGJWcuv756HaJFmoV9ajCTc', '2019-02-06 16:45:35', '2019-02-06 16:45:35');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Индексы таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `form_designer`
--
ALTER TABLE `form_designer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_designer_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`(191));

--
-- Индексы таблицы `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_functions`
--
ALTER TABLE `product_functions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_videos`
--
ALTER TABLE `product_videos`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT для таблицы `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `form_designer`
--
ALTER TABLE `form_designer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT для таблицы `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT для таблицы `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT для таблицы `product_functions`
--
ALTER TABLE `product_functions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT для таблицы `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT для таблицы `product_videos`
--
ALTER TABLE `product_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `form_designer`
--
ALTER TABLE `form_designer`
  ADD CONSTRAINT `form_designer_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
