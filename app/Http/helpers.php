<?php

function basket_counter()
{
	$countProduct = 0;
	if (session_basket() && count(session_basket())) {
		foreach (session_basket() as $key => $value) {
			$countProduct += $value;
		}
	}

	return $countProduct ?: '';
}

function basket_products()
{
	if (session_basket() && count(session_basket())) {
		$ids = array_keys(session_basket());
		return \App\Product::whereIn('id', $ids)->get();
	}
	return [];
}

function related_products()
{
	return \App\Product::inRandomOrder()->take(4)->get();
}

function session_basket()
{
	if (is_array(session()->get('basket-products'))) {
		return session()->get('basket-products');
	}
	return [];
}

function color_products($productId, $color)
{
	$arrayColors = session()->get('color-products');

	if (isset($arrayColors[$productId])) {
		if ($arrayColors[$productId] == $color) {
			return true;
		}
		return false;
	}
	return false;
}

function product_color($productId)
{
	$arrayColors = session()->get('color-products');

	if (isset($arrayColors[$productId])) {
		return $arrayColors[$productId];
	}
	return false;
}