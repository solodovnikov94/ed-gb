<?php

namespace App\Http\Controllers;

use App\NovaPoshtaApi2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrdersController extends Controller
{
	public function order()
	{
		$page = \App\Page::where('slug', 'order')->first();

		return view('pages.order', compact('page'));
	}

	public function saveOrder(Request $request)
	{
		$rules = [
			'fio' => 'required|min:2|max:190',
			'phone' => 'required',
			'email' => 'required',
			'city' => 'required',
			'payment' => 'required',
		];

		if ($request->payment != 3) {
			$rules['delivery'] = 'required';
		}

		$validator = \Illuminate\Support\Facades\Validator::make($request->all(),
			$rules
		);

		if ($validator->fails()) {
			return response()->json([
				'errors' => $validator->errors()
			]);
		}

		Mail::send(['text' => 'email.invoice'], [], function($message){
			// кому отправаляем info@gbaby.com.ua
			$message->to('goodbaby.ua@gmail.com')->subject('gbaby.com.ua');
			$message->from(env('MAIL_USERNAME'), 'Новый заказ на сайте gbaby.com.ua');
		});

		$card = session_basket();
		$productIds = array_keys($card);
		$products = \App\Product::whereIn('id', $productIds)->get();
		$sum = 0;

		foreach ($products as $product) {
			$sum += $product->price * $card[$product->id];
		}

		$order = new \App\Order;
		$order->fio = $request->fio;
		$order->phone = $request->phone;
		$order->email = $request->email;
		$order->city = $request->city;
		$order->payment = $request->payment;
		$order->delivery = $request->delivery;
		$order->np_city = $request->np_city_hidden;
		$order->np_warehouse = $request->np_warehouse;
		$order->sum = $sum;
		$order->status_id = 1;
		$order->description = $request->description;
		$order->np_city = $request->np_city_hidden;
		$order->np_warehouse = $request->warehouses_np;
		$order->save();

		foreach (basket_products() as $product) {
			$orderProduct = new \App\OrderProduct;
			$orderProduct->product_id = $product->id;
			$orderProduct->count = session_basket()[$product->id];
			$orderProduct->sum = session_basket()[$product->id] * $product->price;
			$orderProduct->order_id = $order->id;
			if(isset(session()->get('color-products')[$product->id])){
				$orderProduct->color = session()->get('color-products')[$product->id];
			} else {
				$orderProduct->color = 'Не указано';
			}

			$orderProduct->save();
		}

		session()->forget('basket-products');

		return response()->json([
			'success' => true,
			'url' => url('thanks')
		]);
	}

	public function novaPoshta(Request $request)
	{
		$novaPoshta = new NovaPoshtaApi2(env('NP_API2'), 'ru', FALSE, 'curl');

		if($request->warehouses) {
			$wh = $novaPoshta->getWarehouses($request->warehouses);
			$returnHTML = view('includes.nova-poshta.warehouses')->with([
				'warehouses' => $wh['data']
			])->render();

			return response()->json([
				'html' => $returnHTML,
			]);
		} else {
			$cities = $novaPoshta->getCities();
			$returnHTML = view('includes.nova-poshta.cities')->with([
				'cities' => $cities['data']
			])->render();

			return response()->json([
				'html' => $returnHTML,
			]);
		}
	}

	public function addToCart($id, $color="")
	{
		$productId = (int) $id;
		if($color) {
			$array = session()->get('color-products');
			$array[$id] = $color;
			session()->put('color-products', $array);
		}
		$card = session()->get('basket-products');
		if ($productId) {
			if (is_array($card)) {
				if (isset($card[$productId])) {
					$card[$productId] += 1;
				} else {
					$card[$productId] = 1;
				}
			} else {
				$card = [
					$productId => 1
				];
			}
			session()->put('basket-products', $card);
		}
		return redirect('order');
	}

	public function crement(Request $request)
	{
		$card = session()->get('basket-products');
		$productId = $request->product;
		$crement = $request->crement;
		$card[$productId] = $card[$productId] + $crement;

		if(!$card[$productId]) {
			unset($card[$productId]);
		}

		session()->put('basket-products', $card);


		return $this->renderCard();
	}

	public function renderCard()
	{
		$html = view('includes.popup-product-content')->render();

		$card = session()->get('basket-products');

		if(!is_array($card)) {
			$card = [];
		}

		$countProducts = 0;
		foreach ($card as $item) {
			$countProducts += $item;
		}

		return response()->json([
			'html' => $html,
			'count' => $countProducts
		]);
	}

	public function addAjaxToCart(Request $request)
	{
		$productId = $request->product;
		$card = session()->get('basket-products');

		if ($productId) {
			if (is_array($card)) {
				if (isset($card[$productId])) {
					$card[$productId] += 1;
				} else {
					$card[$productId] = 1;
				}
			} else {
				$card = [
					$productId => 1
				];
			}
			session()->put('basket-products', $card);
		}

		return $this->renderCard();
	}

	public function productColors(Request $request)
	{
		$array = session()->get('color-products');
		$array[$request->product_id] = $request->product_color;

		return session()->put('color-products', $array);
	}
}
