<?php

namespace App\Http\Controllers;

use App\Page;
use App\Product;
use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function index()
	{
		$page = Page::where('slug', 'home')->first();

		$seats = Product::whereIn('slug', ['avtokreslo-idan', 'vaya-i-size'])->get();

		return view('pages.homepage', compact('page', 'seats'));
	}

	public function thanks()
	{
		$page = Page::where('slug', 'thanks')->first();

		return view('pages.thanks', compact('page'));
	}

	public function about()
	{
		$page = Page::where('slug', 'about')->first();

		return view('pages.about', compact('page'));
	}

	public function whereCanBuy()
	{
		$page = Page::where('slug', 'where-can-buy')->first();

		return view('pages.where-can-buy', compact('page'));
	}

	public function colorProduct(Request $request)
	{
		$productColor = \App\ProductImage::where([
			['product_id', '=', $request->product_id],
			['color', '=', $request->product_color]
		])->first();

		return response()->json([
			'html' => isset($productColor->title) ? $productColor->title : '',
		]);
	}
}
