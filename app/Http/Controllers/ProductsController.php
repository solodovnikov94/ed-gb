<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function stroller($id, $slug)
	{
		$view = '';
		switch ($id) {
			case 1:
				$view = 'maris';
				break;
			case 2:
				$view = 'rockit';
				break;
			case 3:
				$view = 'qbit';
				break;
			case 25:
				$view = 'rockit2';
				break;
			case 26:
				$view = 'rockit3';
				break;
			case 27:
				$view = 'pockit-air-all-terrain';
				break;
			case 28:
				$view = 'pockit-all-terrain';
				break;
			case 29:
				$view = 'pockit-all-city';
				break;
			case 30:
				$view = 'qbit-all-terrain';
				break;
			case 31:
				$view = 'qbit-all-city';
				break;
			case 32:
				$view = 'qbit-all-city-fashion';
				break;
			default:
				$view = 'stroller';
		}

		$product = Product::where([
			['id', '=', $id],
			['slug', '=', $slug]
		])
//			->active()
			->firstOrFail();

		return view('pages.'.$view, compact('product'));
	}
	
	public function strollers($subcategory_slug = '')
	{
		$category = Category::where('slug', 'strollers')->first();

		if($subcategory_slug) {
			$subcategory = Category::where('slug', $subcategory_slug)->first();
			$productQuery = Product::where('category_id', $subcategory->id);
		} else {
			$subcategoriesIds = $category->children->groupBy('id')->keys()->toArray();
			$productQuery = Product::whereIn('category_id', $subcategoriesIds);
		}
		$productList = $productQuery->paginate(10);

		return view('pages.items', compact('productList', 'category', 'subcategory_slug'));
	}

	public function seats($subcategory_slug = '')
	{
		$category = Category::where('slug', 'seats')->first();

		if($subcategory_slug) {
			$subcategory = Category::where('slug', $subcategory_slug)->first();
			$productQuery = Product::where('category_id', $subcategory->id);
		} else {
			$subcategoriesIds = $category->children->groupBy('id')->keys()->toArray();
			$productQuery = Product::whereIn('category_id', $subcategoriesIds);
		}
		$productList = $productQuery->paginate(10);

		return view('pages.items', compact('productList', 'category', 'subcategory_slug'));
	}

	public function seat($id, $slug)
	{
		$product = Product::where([
			['id', '=', $id],
			['slug', '=', $slug]
		])->active()->firstOrFail();

		return view('pages.seat', compact('product'));
	}

	public function accessories()
	{
		$category = Category::where('slug', 'accessories')->first();

		$productList = Product::where('category_id', $category->id)->paginate(10);

		$subcategory_slug = '';

		return view('pages.items', compact('productList', 'category', 'subcategory_slug'));
	}

	public function accessory($id, $slug)
	{
		$product = Product::where([
			['id', '=', $id],
			['slug', '=', $slug]
		])->active()->firstOrFail();

		return view('pages.accessory', compact('product'));
	}

	public function search(Request $request)
	{
		$productName = $request->product_name;

		if (!empty($productName)) {
			$products = Product::where('title', 'LIKE', "%$productName%")->take(10)->get();
			$html = view('includes.search-result')
				->with([
					'products' => $products,
				])->render();

			return response()->json([
				'html' => $html,
			]);
		}
		return response()->json([
			'html' => '',
		]);
	}
}
