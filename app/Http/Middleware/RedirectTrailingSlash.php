<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class RedirectTrailingSlash
{
	public function handle($request, Closure $next)
	{
		if (preg_match('/.+\/$/', $request->getRequestUri()))
		{
			return Redirect::to(rtrim($request->getRequestUri(), '/'), 301);
		}

		$links = [
			'index',
			'home',
			'/ru',
			'public'
		];

		foreach ($links as $item) {
			$pos = strpos($request->getRequestUri(), $item);
			if ($pos !== false) {
				return Redirect::to(env('APP_URL'), 301);
			}
		}

		return $next($request);
	}
}
