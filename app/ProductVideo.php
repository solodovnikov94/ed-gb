<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVideo extends Model
{
	public $timestamps = false;

	public function productId()
	{
		return $this->belongsTo(Product::class, 'product_id', 'id');
	}

	public function productIdList()
	{
		if (isset(request()->crud_id)) {
			return Product::where('id', request()->crud_id)->get();
		}
		return Product::get();
	}

	public function setVideoAttribute($value)
	{
		if (stristr($value, 'youtu.be') === FALSE) {
			$this->attributes['video'] = strtok(str_replace("watch?v=", "embed/", $value), '&');
			// не найдена в строке
		} else {
			$this->attributes['video'] = str_replace("https://youtu.be/", "https://www.youtube.com/embed/", $value);
		}
	}
}
