<?php

namespace App\Console\Commands;

use App\Category;
use App\Product;
use Illuminate\Console\Command;

class SiteMap extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'sitemap:create';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$categories = Category::get();
		$products = Product::where('active', 1)->orderBy('id', 'desc')->get();

		$siteUrl = env('APP_URL');
		$base = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml"></urlset>';
		$xmlBase = new \SimpleXMLElement($base);
		$xmlBaseStandardSiteMap = new \SimpleXMLElement($base);

		// categories
		$dom = new \DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$domStandardSiteMap = $dom;

		foreach ($categories as $category) {
			$row = $xmlBase->addChild("url");
			$row->addChild('loc', $category->url);
			$row->addChild('lastmod', $category->atomDate);
		}

		Header('Content-type: text/xml');

		$dom->loadXML($xmlBase->asXML());
		$dom->save(public_path() . "/sitemap/sitemap_rubrics.xml");

		$data = implode("", file(public_path() . "/sitemap/sitemap_rubrics.xml"));
		$gzData = gzencode($data, 9);
		$fp = fopen(public_path() . "/sitemap/sitemap_rubrics.xml.gz", "w");
		fwrite($fp, $gzData);
		fclose($fp);

		$rowStandardSiteMap = $xmlBaseStandardSiteMap->addChild("sitemap");
		$rowStandardSiteMap->addChild('loc', $siteUrl . '/sitemap/sitemap_rubrics.xml.gz');
		$rowStandardSiteMap->addChild('lastmod', date('c'));

		//products
		$xmlBase = new \SimpleXMLElement($base);

		$dom = new \DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		foreach ($products as $product) {
			if ($product->url) {
				$row = $xmlBase->addChild("url");
				$row->addChild('loc', $product->url);
				$row->addChild('lastmod', $product->atomDate);
			}
		}

		Header('Content-type: text/xml');

		$dom->loadXML($xmlBase->asXML());
		$dom->save(public_path() . "/sitemap/sitemap_products.xml");

		$data = implode("", file(public_path() . "/sitemap/sitemap_products.xml"));
		$gzdata = gzencode($data, 9);
		$fp = fopen(public_path() . "/sitemap/sitemap_products.xml.gz", "w");
		fwrite($fp, $gzdata);
		fclose($fp);

		$rowStandardSiteMap = $xmlBaseStandardSiteMap->addChild("sitemap");
		$rowStandardSiteMap->addChild('loc', $siteUrl . '/sitemap/sitemap_products.xml.gz');
		$rowStandardSiteMap->addChild('lastmod', date('c'));
		// end products

		$domStandardSiteMap->loadXML($xmlBaseStandardSiteMap->asXML());
		$domStandardSiteMap->save(public_path() . "/sitemap.xml");

	}
}
