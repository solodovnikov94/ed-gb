<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFunction extends Model
{
	public $timestamps = false;

	public function productId()
	{
		return $this->belongsTo(Product::class, 'product_id', 'id');
	}

	public function productIdList()
	{
		if(isset(request()->crud_id)) {
			return Product::where('id', request()->crud_id)->get();
		}
		return Product::get();
	}
}
