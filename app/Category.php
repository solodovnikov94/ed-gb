<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	public function parentIdList()
	{
		return $this->whereNull('parent_id')->get();
	}

	public function children()
	{
		return $this->hasMany(Category::class, 'parent_id');
	}

	public function scopeActive($query)
	{
		return $query->where('active', 1);
	}

	public function parent()
	{
		return $this->hasOne(Category::class, 'id', 'parent_id');
	}

	public function parentId()
	{
		return $this->belongsTo(self::class);
	}

	public function getUrlAttribute()
	{
		if ($this->parent){
			return url($this->parent->slug.'/'.$this->slug);
		}
		return url($this->slug);
	}

	public function products()
	{
		return $this->hasMany(Product::class, 'category_id', 'id');
	}

	public function scopeCategories($query)
	{
		return $query->whereNull('parent_id')->active()->orderBy('order', 'asc')->with(['children' => function ($query) {
			$query->where('active', 1);
		}])->get();
	}

	public function scopeOnlyRoot($query)
	{
		return $query->where(function($query){
			$query->whereHas('children')
				->orWhere('slug', 'accessories');
		});
	}

	public function getAtomDateAttribute()
	{
		return \Carbon\Carbon::parse($this->updated_at)->toAtomString();
	}

}
