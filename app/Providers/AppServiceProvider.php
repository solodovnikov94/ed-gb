<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		view()->share('products', $this->strollers());
		view()->share('categories', $this->categories());
	}

	public function strollers()
	{
		return \App\Product::whereIn('id', [1,31])->get();
	}

	public function categories()
	{
		return \App\Category::whereNull('parent_id')->get();
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
