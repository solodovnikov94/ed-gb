<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	public function save(array $options = [])
	{
		$this->accessoriesRelation(request()->accessories);
		$this->accompanyingGoodsRelation(request()->accompanying_goods);
		parent::save($options);
	}

	public function getUrlAttribute()
	{
		if (empty($this->slug)) {
			return '';
		}

		if ($this->subcategory->slug == 'accessories') {
			return url($this->subcategory->slug.'/'.$this->id.'-'.$this->slug);
		}

		if ($this->subcategory->parent) {
			return url($this->subcategory->parent->slug.'/'.$this->id.'-'.$this->slug);
		}
	}

	public function getAccessoryButton()
	{
		if(count($this->accessoriesRelationList) && ($this->category_id == 4 || $this->category_id == 5)) {
			return true;
		}
		return false;
	}

	public function subcategory()
	{
		return $this->belongsTo(Category::class, 'category_id', 'id');
	}

	public function getAddToCartAttribute()
	{
		return url('add-to-cart/' . $this->id);
	}

	public function scopeActive($query)
	{
		return $query->where('active', 1);
	}

	public function productFunctions()
	{
		return $this->hasMany(ProductFunction::class, 'product_id', 'id');
	}

	public function productImages()
	{
		return $this->hasMany(ProductImage::class, 'product_id', 'id');
	}

	public function productVideos()
	{
		return $this->hasMany(ProductVideo::class, 'product_id', 'id');
	}

	public function accompanyingGoods()
	{
		return $this->belongsToMany(Product::class, 'product_params', 'product_id', 'accompanying_good_id');
	}

	public function accessories()
	{
		return $this->belongsToMany(Product::class, 'product_params', 'product_id', 'accessory_id');
	}

	public function accessoriesRelationList()
	{
		return $this->accessories();
	}

	public function accessoriesList()
	{
		return Product::where('category_id', 3)->get();
	}

	public function accessoriesRelation($arrayIds)
	{
		return $this->accessories()->sync($arrayIds);
	}

	public function accompanyingGoodsRelation($arrayIds)
	{
		return $this->accompanyingGoods()->sync($arrayIds);
	}

	public function getAtomDateAttribute()
	{
		return \Carbon\Carbon::parse($this->updated_at)->toAtomString();
	}

	public function getOgImageAttribute()
	{
		return asset('storage/'.$this->image);
	}

	public function setVideoAttribute($value)
	{
		if(stristr($value, 'youtu.be') === FALSE) {
			$this->attributes['video'] = strtok(str_replace("watch?v=", "embed/", $value), '&');
			// не найдена в строке
		} else {
			$this->attributes['video'] = str_replace("https://youtu.be/", "https://www.youtube.com/embed/", $value);
		}
	}
}
