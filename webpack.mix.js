const mix = require('laravel-mix');

mix
	.js('resources/js/common.js', 'public/js');

mix
	.sass('resources/sass/common.scss', 'public/css')
	.browserSync('gbaby.com.ua.loc')
	.options({
		autoprefixer: {
			options: {
				browsers: [
					'last 5 versions'
				]
			}
		}
	});

mix
	.disableNotifications()
	.options({
		processCssUrls: false
	});

if (mix.inProduction()) {
	mix.sourceMaps();
}