$(document).ready(function () {
	$('.children').toggle();
	$('#show_subcategories').click(function () {
		console.log('here');
		$('.children').toggle();
	});
	var isMessage = true;

	function countCall() {
		$.ajax({
			type: "GET",
			async: false,
			url: "/admin/new-order",
			success: function (response) {
				if (response.html) {
					$('ul.navbar-right').append(response.html);
					isMessage = response.isMessage;
				}
			},
			error: function (response) {
				console.log(response);
			}
		});
	}

	countCall();
	setInterval(function () {
		if (isMessage) {
			countCall();
		}
	}, 20000);
});